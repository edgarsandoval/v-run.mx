<?php require_once './header.inc'; ?>
<br/>
<div class="container">
        <?php foreach ($page->content as $content): ?>
            <div class="row align-items-center padding-top-1x padding-bottom-3x">
                <div class="col-md-8 offset-md-2 text-center">
                    <div class="hidden-md-up"></div>
                    <h2><?= $content->title ?></h2>
                    <div class="text-left" style="color: #00b3ff !important;"><?= $content->body; ?></div>
                </div>
            </div>
            <hr>
        <?php endforeach; ?>
        <div class="row padding-top-3x padding-bottom-3x">
            <?php foreach ($page->partners as $partner): ?>
                <div class="col-md-<?= 12 / count($page->partners ); ?> col-sm-6 text-center home-cat">
                    <img class="d-block w-150 mx-auto img-thumbnail rounded-circle mb-2" src="<?= $partner->image->url; ?>" alt="<?= $partner->title;  ?>_logo">
                    <h6><?= $partner->title;  ?></h6>
                    <div class="social-bar">
                        <a class="social-button shape-circle sb-facebook" href="<?= $partner->facebook_url; ?>" data-toggle="tooltip" data-placement="top" title="Facebook" target="_blank"><i class="socicon-facebook"></i></a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <!-- End Our Team -->
    </div>

<?php require_once './footer.inc'; ?>
