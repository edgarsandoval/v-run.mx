<?php require_once './header.inc'; ?>

<?php
$render = 'password_form';
$errors = [];

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function sendEmail($u) {
    $newPassword = generateRandomString(6);
    $m = wireMail();

    var_dump($m->testConnection());

    $body = '';
    $body .= '¡Hola ' . $u->first_name .  "!\n";
    $body .= 'Tu nueva contraseña de acceso es: ' . sprintf('%s', $newPassword);
    $recipients = array($u->email); // vagrant@localhost.localdomain
    foreach ($recipients as $recipient) {
        $m->to($recipient)
            ->from('no-reply@v-run.mx')
            ->fromName('V Run')
            ->subject('Recupera tu contraseña')
            ->body($body)
            ->send();
    }

    $u->pass = $newPassword;
    $u->of(false);
    $u->save();

    return true;
}

function sendSMS($u) {
    $urlToRequest   = 'http://167.71.146.227/whatsapp/ws.php';
    $newPassword    = generateRandomString(6);

    $smsTimestamp = date('Y-m-d H:i:s', time() - 60 * 60);

    $data =[
        'opc'   => 'sendsms',
        'tel'   => $u->phone,
        'sms'   => 'Acceso V-RUN es: ' . $u->email . ' y tu clave: ' . $newPassword,
        'cita'  => $smsTimestamp,
        'accion'=> 'wsapp'
    ];

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $urlToRequest);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($ch);

    curl_close ($ch);

    $u->pass = $newPassword;
    $u->of(false);
    $u->save();

    // if($response == 'recorrio el case - true')
        return true;
    // return false;
}

if($input->post->submit !== null) {
    if ($input->post->type == 'email') {
        $data = array(
            'email' => wire('sanitizer')->email(wire('input')->post('email')),
        );

        $u = users()->get('email=' . $data['email']);

        if(!$u->id)
            $errors[] = 'No se encontró miembro con el correo proporcionado';

        if(count($errors) == 0 && sendEmail($u)) {
            $render = 'email_sent';
        }
    } else {
        $data = array(
            'phone' => wire('sanitizer')->text(wire('input')->post('phone')),
        );

        $u = users()->get('phone=' . $data['phone']);

        if(!$u->id)
            $errors[] = 'No se encontró miembro con el teléfono proporcionado';

        if(count($errors) == 0 && sendSMS($u)) {
            $render = 'sms_sent';
        }
    }
}

if(count($errors) > 0)
    $error = array_shift($errors);

?>

<div class="container padding-top-1x padding-bottom-3x">
    <?php if ($render == 'password_form'): ?>
        <?php if (isset($error)): ?>
            <div class="alert alert-danger alert-dismissible fade show text-center margin-bottom-1x"><span class="alert-close" data-dismiss="alert"></span>
                <p><i class="fa fa-bell"></i> <?= $error ?> </p>
            </div>
        <?php endif; ?>
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-10">
                <h2>¿Olvidaste tu contraseña?</h2>
                <p>Para recuperarla, hay dos maneras de poderlo hacer:</p>
                <h4><b>Por correo electrónico</b></h4>
                <ol class="list-unstyled">
                    <li><span class="text-primary text-medium">1. </span>Ingresa tu correo electrónico debajo.</li>
                    <li><span class="text-primary text-medium">2. </span>Te enviaremos una contraseña temporal para que puedas iniciar sesión. <br/> <small style="color: #a800bb;"> Nota: Revisa tu correo no deseado si no te llega en tu bandeja principal.</small> </li>
                    <li><span class="text-primary text-medium">3. </span>Una vez dentro, te recomendamos cambiar tu contraseña nuevamente, por una fácil de recordar.</li>
                </ol>
                <form class="card mt-4" method="POST" id="password_reset_form">
                    <input type="hidden" name="form" value="reset_password"/>
                    <input type="hidden" name="type" value="email"/>
                    <div class="card-body">
                        <div class="form-group">
                            <input class="form-control" type="email" name="email" placeholder="Ingresa tu dirección de correo electrónico" required>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-primary" type="submit" name="submit">Generar nueva contraseña</button>
                    </div>
                </form>
                <br/>
                <br/>
                <h4><b>Por SMS</b></h4>
                <ol class="list-unstyled">
                    <li><span class="text-primary text-medium">1. </span>Ingresa tu numéro telefónico debajo.</li>
                    <li><span class="text-primary text-medium">2. </span>Te enviaremos una contraseña temporal para que puedas iniciar sesión.</li>
                    <li><span class="text-primary text-medium">3. </span>Una vez dentro, te recomendamos cambiar tu contraseña nuevamente, por una fácil de recordar.</li>
                </ol>
                <form class="card mt-4" method="POST" id="password_reset_form">
                    <input type="hidden" name="form" value="reset_password"/>
                    <input type="hidden" name="type" value="sms"/>
                    <div class="card-body">
                        <div class="form-group">
                            <input class="form-control" type="text" name="phone" placeholder="Ingresa tu numéro de teléfono" required>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-primary" type="submit" name="submit">Generar nueva contraseña</button>
                    </div>
                </form>
            </div>
        </div>
    <?php endif; ?>
    <?php if ($render == 'email_sent'): ?>
        <div class="alert alert-success fade show text-center margin-bottom-1x"></span>
            <p><i class="fa fa-bell"></i> El correo ha sido enviado correctamente, revisa tu bandeja de correo para continuar el proceso </p>
        </div>
    <?php endif; ?>
    <?php if ($render == 'sms_sent'): ?>
        <div class="alert alert-success fade show text-center margin-bottom-1x"></span>
            <p><i class="fa fa-bell"></i> El sms ha sido enviado correctamente, revisa tu aplicación de mensajes para continuar el proceso </p>
        </div>
    <?php endif; ?>
</div>

<?php require_once './footer.inc'; ?>
