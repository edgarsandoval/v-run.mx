<?php require_once './header.inc'; ?>

<div class="container padding-top-3x padding-bottom-3x">
    <img class="d-block m-auto page-404" src="<?php echo $config->urls->assets; ?>images/404_art.jpg" alt="404">
    <div class="text-center">
        <h3>Page Not Found</h3>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a href="/">Go back to Homepage</a><br>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
    </div>
</div>

<?php require_once './footer.inc'; ?>
