<?php require_once './header.inc'; ?>
<?php

if($input->get->generate) {
    $name = $input->get('event');
    $event = $pages->get('template=evento_template, name=' . $name .'');
    if(!$event->id)
        $session->redirect('/');

    $isOrganizator = false;
    if(in_array($user->email, [
        'belen.ofelia@hotmail.com',
        'marco.palencia@gmail.com',
        'edgar.desarrollo@gmail.com',
        'carlos.feria@ec-ideas.com',
    ]))
        $isOrganizator = true;

    if(!$isOrganizator)
        $isOrganizator = $user->isLoggedIn() && $event->event_organizator == $user->email || $user->email == 'edgar@ec-ideas.com';
    $isTrainningEvent = $event->event_type->title == 'Entrenamiento';

    // $isOrganizator = false;
    // Build Query to get users
    $registredUsers = $pages->find('template=user, roles=member, user_events.event.id=' . $event->id . 'sort=user_events.id', 'findAll=true');

    foreach ($registredUsers as $idx => $u) {
        $userEvent = $u->user_events->get('event.id=' . $event->id);
        $padNumber = str_pad("" . $idx + 1, 4, "0", STR_PAD_LEFT);
        $userEvent->event_runner_number = $padNumber;
        $userEvent->of(false);
        $userEvent->save();
    }
}

if($input->get->show == 'evidence') {
    $startDate = strtotime('2020-07-02 02:51:56');
    $endDate = strtotime('2020-07-03 04:00:00');

    $evidences = $pages->find('template=repeater_evidence, created_at>' . $startDate, 'findAll=true');
    echo '<br/><br/>';
    var_dump($evidences->count);

    // $minutes = 50;
    // $seconds = 12;
    //
    // $evidence = $pages->get(11222);
    // $evidence->evidence_duration = floatval($minutes) + ($seconds / 60);
    // $evidence->of(false);
    // $evidence->save();
    // var_dump($evidence);die;
    // die;
    echo '<table class="table">
        <tr>
            <th>ID</th>
            <th>Distancia</th>
            <th>TIempo</th>
            <th>Evidencia</th>
            <th>Evento</th>
            <th>Corredor</th>
            <th>Email</th>
            <th>Timestamp</th>
        <tr>';

    foreach ($evidences as $evidence) {
        $userEvent = $pages->get('template=repeater_user_events,evidence.id=' . $evidence->id);
        $u = users()->get('user_events.id=' . $userEvent->id);

        echo '<tr>';
        echo sprintf('<td>%s</td>', $evidence->id);
        echo sprintf('<td>%s kms</td>', $evidence->evidence_distance);
        echo sprintf('<td>%s mns</td>', $evidence->evidence_duration);
        if(@isset($evidence->evidence_images->url))
            echo sprintf('<td><a href="%s" target="_blank"> ver </a></td>', $evidence->evidence_images->url);
        else
            echo sprintf('<td><a href="#" target="_blank">- </a></td>');
        echo sprintf('<td>%s</td>', $userEvent->event->title);
        echo sprintf('<td>%s %s</td>', $u->first_name, $u->last_name);
        echo sprintf('<td>%s</td>', $u->email);
        echo sprintf('<td>%s</td>', date('Y-m-d h:i:s a', $u->created_at));
        echo '</tr>';

        // $globalTotalDistance = 0.0;
        // $globalTotalDuration = 0.0;
        //
        // foreach ($u->user_events as $userEvent) {
        //     $partialTotalDistance = 0.0;
        //     $partialTotalDuration = 0.0;
        //
        //     foreach ($userEvent->evidence as $evidence) {
        //         $partialTotalDuration += $evidence->evidence_duration;
        //         $partialTotalDistance += $evidence->evidence_distance;
        //     }
        //
        //     $userEvent->event_total_distance = $partialTotalDistance;
        //     $userEvent->event_total_duration = $partialTotalDuration;
        //     $userEvent->of(false);
        //     $userEvent->save();
        //
        //     if($userEvent->event_status->title == 'Terminado') {
        //         $globalTotalDistance += $partialTotalDistance;
        //         $globalTotalDuration += $partialTotalDuration;
        //     }
        // }
        //
        // $u->distance_traveled = $globalTotalDistance;
        // $u->duration_traveled = $globalTotalDuration;
        // $u->of(false);
        // $u->save();
    }

    echo '</table>';

    die;

}

if($input->get->event) {
    if($input->get->dispatch) {
        $showAlert = true;
        if($input->get->dispatch == 'SUCCESS_REGISTER_OLD') {
            $message = 'Usuario ya está registrado y se le asignó el Evento a MisEventos.';
        } else {
            $message = 'Se le envió un correo al usuario para que se de de alta usando este link: <a href="' . $input->get->link . '">' . $input->get->link . '</a>';
        }
    }
    $name = $input->get('event');
    $event = $pages->get('template=evento_template, name=' . $name .'');
    if(!$event->id)
        $session->redirect('/');


    $isOrganizator = false;
    if(in_array($user->email, [
        'belen.ofelia@hotmail.com',
        'marco.palencia@gmail.com',
        'edgar.desarrollo@gmail.com',
        'carlos.feria@ec-ideas.com',
    ]))
        $isOrganizator = true;

    if(!$isOrganizator)
        $isOrganizator = $user->isLoggedIn() && $event->event_organizator == $user->email || $user->email == 'edgar@ec-ideas.com';

    $isTrainningEvent = $event->event_type->title == 'Entrenamiento';
    $isPrivateEvent = $event->event_classification->title == 'Privado';
    // $isOrganizator = false;
    // Build Query to get users
    $registredUsers = $pages->find('template=user, roles=member, user_events.event.id=' . $event->id, 'findAll=true');

    $headers = [];
    $headers['extra_label_1'] = $event->event_extra_label_1 != '' ? $event->event_extra_label_1 : 'Campo Extra 1';
    $headers['extra_label_2'] = $event->event_extra_label_2 != '' ? $event->event_extra_label_2 : 'Campo Extra 2';

    $resultsTable = [];
    $title = 'Resultados - ' . $event->title;

    if($event->event_type->title == 'Distancia' || $isTrainningEvent) {
        $title .= ' ' . $event->event_distance . ' kms';
        $subfix = 'mns';
    }
    else {
        $title .= ' ' . $event->event_duration . ' mns';
        $subfix = 'kms';
    }

    $lastNumber = 0;
    foreach($registredUsers as $rUser) {
        $result = [];
        if ($rUser->profile_picture)
            $result['profile_picture'] = $rUser->profile_picture->url;
        else
            $result['profile_picture'] = $config->urls->assets . "images/no-user.jpg";

        $rUserEvent = $rUser->user_events->get('event.id=' . $event->id);
        // if($rUserEvent->event_runner_number != '') {
        //     $lastNumber = max($lastNumber, intval($rUserEvent->event_runner_number));
        // } else {
        //     var_dump($rUser->email);
        // }

        $result['member'] = '<b>' . $rUserEvent->event_runner_number . '</b> - ' .ucwords(mb_strtolower(trim(sprintf('%s %s', $rUser->first_name, $rUser->last_name))));

        if($rUser->address->count != 0) {
            $result['state'] = $rUser->address[0]->address_state;
            $result['address'] = ucwords(mb_strtolower(trim(sprintf('%s #%s%s, %s, ',
                $rUser->address[0]->address_street, // Calle
                $rUser->address[0]->address_outdoor_number, // No. Exterior
                $rUser->address[0]->address_indoor_number != '' ? '-' . $rUser->address[0]->address_indoor_number : '', // No. Interior
                $rUser->address[0]->address_suburb, // Colonia
            ))));
            $result['address'] .= sprintf(' CP. %s, %s, %s',

                $rUser->address[0]->address_zip_code, // CÃ³digo Postal
                $rUser->address[0]->address_state, // Estado,
                $rUser->address[0]->address_city // Ciudad
            );
            if($rUserEvent->event_tracking_guide != '') {
                $result['tracking_guide'] = $rUserEvent->event_tracking_guide;
                $result['submit_timestamp'] = $rUserEvent->event_submit_timestamp;
            }
            else
                $result['tracking_guide'] = '-';
        } else {
            $result['state'] = 'N/A';
            $result['tracking_guide'] = '';
            $result['address'] = 'SIN INFORMACIÓN';
        }

        $result['number'] = $rUserEvent->event_carrier ?? '-';
        $result['id'] = $rUserEvent->id;

        // if($event->event_type->title == 'Distancia' || $isTrainningEvent)
        // else
        $result['duration'] = $rUserEvent->event_total_duration . 'mns';
        $result['distance'] = $rUserEvent->event_total_distance . 'kms';

            switch ($rUserEvent->event_status->title) {
                case 'Pendiente':
                    $result['status'] = '<div class="d-inline text-warning">' . $rUserEvent->event_status->title . '</div>';
                    break;
                case 'Terminado':
                    $result['status'] = '<div class="d-inline text-success">' . $rUserEvent->event_status->title . '</div>';
                    break;
                case 'Cancelado':
                    $result['status'] = '<div class="d-inline text-danger">' . $rUserEvent->event_status->title . '</div>';
                    break;
            }
        $result['plain_status'] =  $rUserEvent->event_status->title;
        $result['email'] = $rUser->email;

        //
        $result['gender'] = @$rUser->gender->title ?: 'Masculino';
        if($rUser->birthdate == '')
            $result['age'] = '-';
        else
            $result['age'] = date_diff(date_create($rUser->birthdate), date_create(date('Y-m-d')))->format('%Y');

        $result['evidences'] = $rUserEvent->evidence->count;
        $result['extra_field_1'] = $rUserEvent->event_extra_field_1 != '' ? $rUserEvent->event_extra_field_1 : '-';
        $result['extra_field_2'] = $rUserEvent->event_extra_field_2 != '' ? $rUserEvent->event_extra_field_2 : '-';

        $resultsTable[] = $result;
    }

    usort($resultsTable, function($r1, $r2) use ($event, $isTrainningEvent) {
        if($r1['plain_status'] == 'Terminado' && $r2['plain_status'] == 'Pendiente')
            return -1;
        if($r1['plain_status'] == 'Pendiente' && $r2['plain_status'] == 'Terminado')
            return 1;

        if($r1['plain_status'] == $r2['plain_status']) {
            if($event->event_type->title == 'Distancia' || $isTrainningEvent) {
                if($r1['duration'] == 0 && $r2['duration'] != 0) return 1;
                if($r1['duration'] != 0 && $r2['duration'] == 0) return -1;
                if(floatval($r1['duration']) > floatval($r2['duration']))
                    return 1;
                else
                    return -1;
                // return floatval($r1['duration']) - floatval($r2['duration']);
            } else {
                if($r1['distance'] == 0 && $r2['distance'] != 0) return 1;
                if($r1['distance'] != 0 && $r2['distance'] == 0) return -1;
                if(floatval($r2['distance']) > floatval($r1['distance']))
                    return 1;
                else
                    return -1;
                // return floatval($r2['distance']) - floatval($r1['distance']);
            }
        }
    });
} else {
    $session->redirect('/');
}

// echo str_pad("" . $lastNumber + 1, 4, "0", STR_PAD_LEFT);;


function ordinal($number) {
    $ends = array('mo','er','do','er','to','to','to','mo','vo','no');
    if ((($number % 100) >= 11) && (($number%100) <= 13))
        return $number. '<sup>º<sup>';
    else
        return $number. '<sup>' . $ends[$number % 10] . '</sup>';
}

?>

<div class="<?= $isOrganizator ? 'container-fluid' : 'container' ?> padding-top-1x padding-bottom-3x">
    <div class="row justify-content-center">
        <!-- Start Blog Single Content -->
        <div class="<?= $isOrganizator ? 'col-lg-11' : 'col-lg-9' ?>">
            <h2 class="padding-top-2x text-center"><?= $title; ?></h2>
            <?php if ($isOrganizator): ?>
                <h3 class="text-center padding-bottom-2x text-danger">
                    Vista de organizador
                    <button class="btn btn-primary btn-invitation" data-lbl-1="<?= $headers['extra_label_1'] ?>" data-lbl-2="<?= $headers['extra_label_2'] ?>" data-id="<?= $event->id ?>">Inscribir por invitación</button>
                    &nbsp;&nbsp;<a href="/user-event-linker/?dispatch=generateExcel&event_id=<?= $event->id; ?>" style="color: #1D6F42; text-decoration: none;" target="_blank">Excel</a>
                </h3>
            <?php endif; ?>
            <div class="table-filters"  style="margin-bottom: 25px;">
                <div class="row">
                    <div class="col-md-3">
                        <p style="text-align: center; margin-top: 10px; font-weight: bold;">Género</p>
                        <select class="gender-filter form-control">
                            <option value="*" selected>Todos (por defecto)</option>
                            <option value="Masculino">Varonil</option>
                            <option value="Femenino">Femenil</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <p style="text-align: center; margin-top: 10px; font-weight: bold;">Terminación</p>
                        <select class="evidence-filter form-control">
                            <option value="*" selected>Todos (por defecto)</option>
                            <option value="1">Terminado en 1 sola etapa</option>
                            <option value="0">Terminado en varias etapas</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <p style="text-align: center; margin-top: 10px; font-weight: bold;">Edad</p>
                        <select class="age-filter form-control">
                            <option value="*" selected>Todas (por defecto)</option>
                            <option value="less20">MENORES DE 20 AÑOS</option>
                            <option value="more20less30">20-29 AÑOS</option>
                            <option value="more30less40">30-39 AÑOS</option>
                            <option value="more40less50">40-49 AÑOS</option>
                            <option value="more50">50 AÑOS EN ADELANTE</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="table-responsive margin-bottom-none">
                <table class="table" id="result-table">
                    <thead>
                        <tr>
                            <?php if (!$isOrganizator): ?>
                                <th>Posición</th>
                                <th>Foto</th>
                            <?php endif; ?>
                            <th>Corredor</th>
                            <th>Registro</th>
                            <th>Estado</th>
                            <th>Progreso del evento</th>
                            <th>Resultados</th>
                            <th>-</th>
                            <th>-</th>
                            <th>-</th>
                            <?php if ($isOrganizator): ?>
                                <th>Email</th>
                                <th>Carrier</th>
                                <th>Dirección</th>
                                <?php if (!$isPrivateEvent): ?>
                                    <th>Guía de rastreo</th>
                                <?php endif; ?>
                                <th><?php echo $headers['extra_label_1']; ?></th>
                                <th><?php echo $headers['extra_label_2']; ?></th>
                                <th>Acciones</th>
                            <?php endif; ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($resultsTable as $idx => $row): ?>
                            <tr>
                                <?php if (!$isOrganizator): ?>
                                    <td data-position="<?= ($idx + 1) ?>"></td>
                                    <td>
                                        <img src="<?php echo $row['profile_picture'] ?>" class="member-image" alt="">
                                    </td>
                                <?php endif; ?>
                                <td><?php echo $row['member'] ?></td>
                                <td>
                                    <?php if ($event->event_type->title == 'Distancia' || $isTrainningEvent): ?>
                                        <?php echo $row['duration'] ?> / <?php echo $row['distance'] ?>
                                    <?php else: ?>
                                        <?php echo $row['distance'] ?> / <?php echo $row['duration'] ?>
                                    <?php endif; ?>
                                    <?php //echo (isset($row['distance']) ? $row['distance'] : $row['duration']) . ' ' . $subfix; ?>
                                </td>
                                <td><?php echo $row['state']; ?></td>
                                <td><?php echo $row['status'] ?></td>
                                <td style="text-align: center;"><a class="show-evidences-prvt" href="#" data-runner="<?= $row['member'] ?>" data-id="<?= $row['id']; ?>" data-event="<?= $event->title; ?>" data-lock="<?= $isOrganizator ? 'true' : 'false'; ?>" data-training="<?= $isTrainningEvent ? '1' : '0'; ?>"><i class="icon-eye"></i></a></td>
                                <td><?php echo $row['gender']; ?></td>
                                <td><?php echo $row['age']; ?></td>
                                <td><?php echo $row['evidences']; ?></td>
                                <?php if ($isOrganizator): ?>
                                    <td><?php echo $row['email']; ?></td>
                                    <td style="text-align: center; vertical-align: middle;"><?php echo $row['number']; ?></td>
                                    <td><?php echo $row['address']; ?></td>
                                    <?php if (!$isPrivateEvent): ?>
                                        <td><?php echo $row['tracking_guide']; ?></td>
                                    <?php endif; ?>
                                    <td><?php echo $row['extra_field_1']; ?></td>
                                    <td><?php echo $row['extra_field_2']; ?></td>
                                    <td>
                                        <button class="btn btn-info btn-edit-r" data-id="<?= $row['id']; ?>" data-private="<?= $isPrivateEvent  ? '0' : '1'; ?>">Editar <i class="fa fa-pencil"></i></button>
                                        <button class="btn btn-danger btn-remove" data-id="<?= $row['id']; ?>">Eliminar <i class="fa fa-trash"></i></button>
                                        <?php if ($row['plain_status'] == 'Pendiente'): ?>
                                            <button class="btn btn-primary btn-finish" data-id="<?= $row['id']; ?>">Terminar desafío <i class="fa fa-check"></i></button>
                                        <?php endif; ?>
                                    </td>
                                <?php endif; ?>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- End Blog Single Content -->
    </div>
</div>
<script type="text/javascript">
function ordinalSuffix(number) {
    let ends = ['mo','er','do','er','to','to','to','mo','vo','no'];
    if (((number % 100) >= 11) && ((number%100) <= 13))
        return number + '<sup>º<sup>';
    else
        return number + '<sup>' + ends[number % 10] + '</sup>';
}

function onInit() {
    $.fn.dataTable.ext.search.push(
        function(settings, data, dataIndex) {
            let genderFilter = $('.gender-filter').val();
            let ageFilter = $('.age-filter').val();
            let evidenceFilter = $('.evidence-filter').val();

            let genderIdx = 7;
            let ageIdx = 8;
            let evidenceIdx = 9;

            if(genderFilter !== '*') {
                if(data[genderIdx] !== genderFilter)
                    return false;
            }

            if(ageFilter !== '*') {
                // <option value="less20">MENORES DE 20 AÑOS</option>
                // <option value="more20less30">20-29 AÑOS</option>
                // <option value="more30less40">30-39 AÑOS</option>
                // <option value="more40less50">40-49 AÑOS</option>
                // <option value="more50">50 AÑOS EN ADELANTE</option>
                let age = parseInt(data[ageIdx]);

                if(isNaN(age)) return false;

                switch (ageFilter) {
                    case 'less20': if(age > 20) return false; break;
                    case 'more20less30': if(age < 20 || age > 29) return false; break;
                    case 'more30less40': if(age < 30 || age > 39) return false; break;
                    case 'more40less50': if(age < 40 || age > 49) return false; break;
                    case 'more50': if(age < 50) return false; break;
                }
            }

            if(evidenceFilter !== '*') {
                if(evidenceFilter == 1 && data[evidenceIdx] != 1) return false;
                if(evidenceFilter == 0 && data[evidenceIdx] <= 1) return false;
            }

            return true;
        }
    );

    var table = $('#result-table').DataTable({
        language: {
            url: 'https://cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json'
        },
        ordering: false,
        columnDefs: [

            <?php if ($isOrganizator): ?>
                // { targets: [ 0, 1, 3, 4, 5, 6 ], searchable: false },
                // { targets: [ 0, 1, 2, 3, 4, 5, 6 ], sortable: false },
                <?php if($isPrivateEvent): ?>
                    { targets: [ 5, 6, 7, 8, 9, 10, 11, 12, 13 ] , className: 'admin-col' },
                <?php else: ?>
                    { targets: [ 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 ] , className: 'admin-col' },
                <?php endif; ?>
                { targets: [ 5, 6, 7 ], visible: false, searchable: true },
            <?php else: ?>
                { targets: [ 0, 1, 3, 4, 5, 6 ], searchable: false },
                { targets: [ 0, 1, 2, 3, 4, 5, 6 ], sortable: false },
                { targets: [ 7, 8, 9 ], visible: false, searchable: true },
            <?php endif; ?>
        ],
        <?php if (!$isOrganizator): ?>
            initComplete: function(settings, json) {
                $('#result-table thead th')[1].style.width = '1px';
            },
            rowCallback: function( row, data, i, nth ) {
                let firstCol = $(row).find('td').first();
                let position = firstCol.data('position');
                if($('.gender-filter').val() !== '*' || $('.age-filter').val() !== '*' || $('.evidence-filter').val() !== '*') {
                    position = nth + 1;
                }


                if (position == 1) {
                    $(row).removeClass('first-place second-place third-place');
                    $(row).addClass('first-place');
                }
                else if (position == 2) {
                    $(row).removeClass('first-place second-place third-place');
                    $(row).addClass('second-place');
                }
                else if (position == 3) {
                    $(row).removeClass('first-place second-place third-place');
                    $(row).addClass('third-place');
                }
                else
                    $(row).removeClass('first-place second-place third-place');

                firstCol.html(ordinalSuffix(position));
            },
        <?php endif; ?>
        <?php if ($isOrganizator): ?>
        pageLength: 1000
        <?php endif; ?>
    });

    $('.gender-filter, .age-filter, .evidence-filter').change(function() {
        table.draw();
    });
}
</script>

<style media="screen">
.swal2-styled.swal2-confirm {
    background-color: #3085d6 !important;
}
</style>

<?php if (isset($showAlert) && $showAlert): ?>
<script type="text/javascript">
    function removeParam(key, sourceURL) {
        var rtn = sourceURL.split("?")[0],
            param,
            params_arr = [],
            queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
        if (queryString !== "") {
            params_arr = queryString.split("&");
            for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                param = params_arr[i].split("=")[0];
                if (param === key) {
                    params_arr.splice(i, 1);
                }
            }
            rtn = rtn + "?" + params_arr.join("&");
        }
        return rtn;
    }

    function onInit() {
        Swal.fire(
            '¡Éxito!',
            '<?php echo $message ?>',
            'success'
        ).then(result => {
            window.location.href = removeParam('link', removeParam('dispatch', window.location.href));
        });
    }
    // window.history.pushState({}, "Hide", "https://v-run.mx/perfil/");
</script>
<?php endif; ?>

<!-- <form action="/user-event-linker/" method="POST" style="display: none;">
    <input type="hidden" name="form" value="finish_event"/>
    <input type="text" name="id" value="<?= $userEvent->id; ?>"/>
    <button type="submit" name="submit"></button>
</form> -->

<?php require_once './footer.inc'; ?>
