<?php require_once './header.inc'; ?>

<!-- Start Main Slider -->
<div class="hero-slider d-none d-sm-block">
    <div class="owl-carousel large-controls dots-inside" data-owl-carousel='{"nav": true, "dots": true, "loop": true, "autoplay": true, "autoplayTimeou": 7000}'>
        <?php foreach ($page->slider as $slider): ?>
            <div class="item">
                <img src="<?= $slider->httpUrl; ?>" alt="">
            </div>
        <?php endforeach; ?>
    </div>
</div>
<!-- End Main Slider -->

<!-- Start Main Slider Mobile -->
<div class="hero-slider mobile d-block d-sm-none">
    <div class="owl-carousel large-controls dots-inside" data-owl-carousel='{"nav": true, "dots": true, "loop": true, "autoplay": true, "autoplayTimeou": 7000}'>
        <?php foreach ($page->slider_mobile as $slider): ?>
            <div class="item">
                <img src="<?= $slider->httpUrl; ?>" alt="">
            </div>
        <?php endforeach; ?>
    </div>
</div>
<!-- End Main Slider -->
<br/>
<div class="container home-container">
    <?php foreach ($page->section as $i => $section): ?>
        <?php if ($i % 2 == 0): ?>
            <div class="row align-items-center padding-bottom-3x">
                <div class="col-md-5">
                    <img class="d-block w-270 m-auto" src="<?= $section->image->httpUrl; ?>">
                </div>
                <div class="col-md-7 text-md-left text-center">
                    <div class="hidden-md-up"></div>
                    <h2><?= $section->title; ?></h2>
                    <div style="color: #00b3ff; ">
                        <?= $section->body;  ?>
                    </div>
                </div>
            </div>
            <hr>
            <br/>
        <?php else: ?>
            <div class="row align-items-center padding-top-3x padding-bottom-3x">
                <div class="col-md-5 order-md-2">
                    <img class="d-block w-270 m-auto" src="<?= $section->image->httpUrl; ?>">
                </div>
                <div class="col-md-7 order-md-1 text-md-left text-center">
                    <div class="hidden-md-up"></div>
                    <h2><?= $section->title; ?></h2>
                    <div style="color: #00b3ff; ">
                        <?= $section->body;  ?>
                    </div>
                </div>
            </div>
            <hr>
            <br/>
        <?php endif; ?>
    <?php endforeach; ?>
</div>

<?php require_once './footer.inc'; ?>
