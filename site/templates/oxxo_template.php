<?php

use Dompdf\Dompdf;
//
// $dompdf = new Dompdf();
//
// ob_start();
// require_once 'pdf/receipt.php';
// $pdfContent = ob_get_clean();
// // var_dump($pdfContent);die;
// $dompdf->loadHtml($pdfContent);
//
// // Render the HTML as PDF
// $dompdf->render();
//
// // Output the generated PDF to Browser
// echo base64_encode($dompdf->output());

$bodyHTML = file_get_contents('mail/receipt.html');

$vars = [
    '{$full_name}' => $data['full_name'],
    '{$email}' => $data['email'],
    '{$phone}' => $data['phone'],
    '{$message}' => $data['message']
];

$bodyHTML = strtr($bodyHTML, $vars);

echo $bodyHTML;

?>
