<?php
namespace ProcessWire;

require_once './Conekta.inc';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Dompdf\Dompdf;

function encode_arr($data) {
    return base64_encode(serialize($data));
}

function decode_arr($data) {
    return unserialize(base64_decode($data));
}

function response($status, $message, $data = null) {
    return compact('status', 'message', 'data');
}

function calculateProgress($u, $pages) {
    $globalTotalDistance = 0.0;
    $globalTotalDuration = 0.0;

    foreach ($u->user_events as $userEvent) {
        $partialTotalDistance = 0.0;
        $partialTotalDuration = 0.0;

        foreach ($userEvent->evidence as $evidence) {
            $partialTotalDuration += $evidence->evidence_duration;
            $partialTotalDistance += $evidence->evidence_distance;
        }

        $userEvent->event_total_distance = $partialTotalDistance;
        $userEvent->event_total_duration = $partialTotalDuration;
        $userEvent->of(false);
        $userEvent->save();

        if($userEvent->event_status->title == 'Terminado') {
            $globalTotalDistance += $partialTotalDistance;
            $globalTotalDuration += $partialTotalDuration;
        }
    }

    $u->distance_traveled = $globalTotalDistance;
    $u->duration_traveled = $globalTotalDuration;
    $u->of(false);
    $u->save();

    return true;
}

function generateRandomString($length = 10) {
    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

if($input->get->dispatch !== null) {
    // dispatch=event_register&id_boletify=ID_BOLETIFY&email=EMAIL
    switch($input->get->dispatch) {
        case 'event_register':
            $id_boletify = $input->get->id_boletify;// wire('sanitizer')->text(wire('input')->get('id_boletify'));
            $email       = $input->get->email;// wire('sanitizer')->text(wire('input')->get('email'));
            if($id_boletify == 'ID_BOLETIFY' && $email == 'EMAIL') {
                header('Content-Type: application/json');
                echo wireEncodeJSON(response(false, 'Default data: INVALID'), true, true); exit;
            }
            // file_put_contents('./input.txt', PHP_EOL, FILE_APPEND);
            if(file_put_contents('./input.txt', sprintf('date: %s -> ', date('Y-m-d H:i:s')) . http_build_query($_GET), FILE_APPEND)) {
                if($email == '') {
                    file_put_contents('./input.txt', ' >>> ERROR ' . PHP_EOL, FILE_APPEND);
                    return;
                }
                $u = users()->get("email=$email");

                $package = $pages->get("template=paquete_template, id_boletify=$id_boletify");
                if($package->id) {
                    $isPackage = true;
                    $boletify = 1;
                }
                else {
                    $package = $pages->get("template=paquete_template, id_boletify2=$id_boletify");

                    if($package->id) {
                        $isPackage = true;
                        $boletify = 2;
                    }
                    else
                        $isPackage = false;
                }

                if($isPackage) {
                    foreach ($package->events as $event) {
                        $eventPage = $event->event;
                        $userEvent = $u->user_events->getNew();
                        $userEvent->event = $eventPage; //->add($eventPageId);
                        $userEvent->event_total_distance = 0.0;
                        $userEvent->event_total_duration = 0.0;
                        $userEvent->event_status = 'Pendiente';

                        if($boletify == 1)
                            $userEvent->event_carrier  = $package->event_carrier;
                        else
                            $userEvent->event_carrier  = $package->event_carrier2;

                        $eventRecords = $pages->find('template=repeater_user_events, event.id='. $eventPage->id, 'findAll=true');
                        $idx = 0;
                        foreach ($eventRecords as $record) {
                            $idx = max($idx, intval($record->event_runner_number));
                        }

                        $padNumber = str_pad("" . $idx + 1, 4, "0", STR_PAD_LEFT);
                        $userEvent->event_runner_number = $padNumber;

                        $userEvent->save();

                        $u->of(false);
                        $u->user_events->add($userEvent);
                        $u->save();

                        $userEvents = [];

                        foreach($u->user_events as $ue) {
                            if(isset($ue->event))
                                $userEvents[] = $ue->event->id;
                        }

                        if(in_array($eventPage->id, $userEvents))
                            file_put_contents('./input.txt', ' >>> OK ' . $eventPage->id, FILE_APPEND);
                        else
                            file_put_contents('./input.txt', ' >>> ERROR ' . $eventPage->id, FILE_APPEND);
                    }
                    file_put_contents('./input.txt', PHP_EOL, FILE_APPEND);

                    header('Content-Type: application/json');
                    echo wireEncodeJSON(response(true, 'User related to package correctly', [
                        'user'  => $u->email,
                        'package' => $package->title
                    ]), true, true);
                    header("HTTP/1.1 200 OK");
                    exit;
                } else {

                    // Search event by principal id
                    $eventPage  = $pages->get("template=evento_template,id_boletify=$id_boletify");
                    $boletify = 1;

                    // If does not exist search by second id
                    if(!$eventPage->id) {
                        $eventPage = $pages->get("template=evento_template,id_boletify2=$id_boletify");
                        $boletify = 2;
                    }

                    // If does not exist search by third id
                    if(!$eventPage->id) {
                        $eventPage = $pages->get("template=evento_template,id_boletify3=$id_boletify");
                        $boletify = 3;
                    }

                    foreach($u->user_events as $userEvent) {
                        if(isset($userEvent->event)) {
                            if($userEvent->event->id == $eventPage->id) {
                                file_put_contents('./input.txt', ' >>> ERROR ' . PHP_EOL, FILE_APPEND);
                                return;
                            }
                        }
                    }

                    $userEvent = $u->user_events->getNew();
                    $userEvent->event = $eventPage; //->add($eventPageId);
                    $userEvent->event_total_distance = 0.0;
                    $userEvent->event_total_duration = 0.0;
                    $userEvent->event_status = 'Pendiente';

                    if($boletify == 1)
                        $userEvent->event_carrier = $eventPage->event_carrier;
                    if($boletify == 2)
                        $userEvent->event_carrier = $eventPage->event_carrier2;
                    if($boletify == 3)
                        $userEvent->event_carrier = $eventPage->event_carrier3;

                    $eventRecords = $pages->find('template=repeater_user_events, event.id='. $eventPage->id, 'findAll=true');
                    $idx = 0;
                    foreach ($eventRecords as $record) {
                        $idx = max($idx, intval($record->event_runner_number));
                    }

                    $padNumber = str_pad("" . $idx + 1, 4, "0", STR_PAD_LEFT);
                    $userEvent->event_runner_number = $padNumber;

                    if(isset($input->get->extra_field_1) && $input->get->extra_field_1 != '') {
                        $userEvent->event_extra_field_1 = $input->get->extra_field_1;
                    }

                    $userEvent->save();

                    $u->of(false);
                    $u->user_events->add($userEvent);
                    $u->save();

                    $userEvents = [];

                    foreach($u->user_events as $ue) {
                        if(isset($ue->event))
                            $userEvents[] = $ue->event->id;
                    }

                    if(in_array($eventPage->id, $userEvents))
                        file_put_contents('./input.txt', ' >>> OK ' . PHP_EOL, FILE_APPEND);
                    else
                        file_put_contents('./input.txt', ' >>> ERROR ' . PHP_EOL, FILE_APPEND);

                    header('Content-Type: application/json');
                    echo wireEncodeJSON(response(true, 'User related to event correctly', [
                        'user'  => $u->email,
                        'event' => $eventPage->title
                    ]), true, true);
                    header("HTTP/1.1 200 OK");
                }
                exit;
            }
            break;
        case 'remove_event':
            $id_boletify = $input->get->id_boletify;// wire('sanitizer')->text(wire('input')->get('id_boletify'));
            $email       = $input->get->email;// wire('sanitizer')->text(wire('input')->get('email'));
            if($id_boletify == 'ID_BOLETIFY' && $email == 'EMAIL') {
                header('Content-Type: application/json');
                echo wireEncodeJSON(response(false, 'Default data: INVALID'), true, true); exit;
            }
            if(file_put_contents('./input.txt', sprintf('date: %s -> ', date('Y-m-d H:i:s')) . http_build_query($_GET) . PHP_EOL, FILE_APPEND)) {
                $u = users()->get("email=$email");

                $userEvent = null;
                foreach ($u->user_events as $ue) {
                    if($ue->event->id_boletify == $id_boletify || $ue->event->id_boletify2 == $id_boletify) {
                        $userEvent = $ue;
                        break;
                    }
                }

                $eventPage = $userEvent->event;

                $u->user_events->remove($userEvent);
                $u->of(false);
                $u->save();

                header('Content-Type: application/json');
                echo wireEncodeJSON(response(true, 'User removed from correctly', [
                    'user'  => $u->email,
                    'event' => $eventPage->title
                ]), true, true);
                header("HTTP/1.1 200 OK");
                exit;
            }
            break;
        case 'cities_map':
            $users = users()->find('roles.name=member');
            $addressPending = 0;

            foreach($users as $u) {
                if($u->address->count > 0) {
                    $ptAddress = $u->address->eq(0);
                    $zipCode = $ptAddress->address_zip_code;

                    if(strlen($zipCode) < 5) {
                        $zipCode = '0' . $zipCode;
                    }

                    $ptAddress->address_zip_code = $zipCode;
                    $ptAddress->of(false);
                    $ptAddress->save();

                    // $urlToRequest = 'http://138.68.1.140/nominatim/search.php?format=json&postalcode=' . $zipCode;
                    //
                    // $curl = curl_init();
                    //
                    // curl_setopt($curl, CURLOPT_URL, $urlToRequest);
                    // curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    //
                    // $response = curl_exec($curl);
                    // curl_close($curl);
                    //
                    // $response = json_decode($response, true);
                    //
                    // if(count($response) > 0) {
                    //     $displayName = $response[0]['display_name'];
                    //     $data = explode(',', $displayName);
                    //
                    //     $ptAddress->address_city = trim($data[0]);
                    //     $ptAddress->address_state = trim($data[1]);
                    //
                    //     $ptAddress->of(false);
                    //     $ptAddress->save();
                    // } else {
                    //     $addressPending++;
                    //     var_dump($zipCode);
                    // }


                    // if($ptAddress->address_city == ''):
                    //
                    // $curl = curl_init();
                    //
                    // curl_setopt_array($curl, array(
                    //   CURLOPT_URL => "https://forcsec.com/api/codigos-postales",
                    //   CURLOPT_RETURNTRANSFER => true,
                    //   CURLOPT_ENCODING => "",
                    //   CURLOPT_MAXREDIRS => 10,
                    //   CURLOPT_TIMEOUT => 30,
                    //   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    //   CURLOPT_CUSTOMREQUEST => "POST",
                    //   CURLOPT_POSTFIELDS => '{"codigoPostal":"' . $zipCode . '","key":"DzpHAlsaUP9aK8odDWP7ork5cgdbrMEdtSqemgFoUa7BFR5FnR"}',
                    //   CURLOPT_HTTPHEADER => array(
                    //     "accept: application/json",
                    //     "Cache-Control: no-cache",
                    //     "Content-Type: application/json"
                    //   )
                    // ));
                    //
                    // $response = curl_exec($curl);
                    // $err = curl_error($curl);
                    // curl_close($curl);
                    //
                    // $response = json_decode($response, true);
                    // if(!isset($response['estatus'])) {
                    //     $ptAddress->address_state = $response['estado'];
                    //     $ptAddress->address_city = $response['municipio'];
                    //     $ptAddress->address_zip_code = $zipCode;
                    //     $ptAddress->of(false);
                    //     $ptAddress->save();
                    // }
                    //
                    // endif;
                }
            }
            echo PHP_EOL;
            var_dump($addressPending);
            die;
            break;
        case 'zip_search':
            $zipCode = $input->get('zip_code');
            $urlToRequest = 'http://138.68.1.140/nominatim/search.php?format=json&postalcode=' . $zipCode;

            $curl = curl_init();

            curl_setopt($curl, CURLOPT_URL, $urlToRequest);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            $response = curl_exec($curl);
            curl_close($curl);

            $response = json_decode($response, true);

            if(count($response) > 0) {
                $displayName = $response[0]['display_name'];
                $data = explode(',', $displayName);

                foreach ($data as &$item) {
                    $item = trim($item);
                }
            } else {
                $data = [];
            }

            header('Content-Type: application/json');
            echo wireEncodeJSON(response(true, null, $data), true, true);
            header("HTTP/1.1 200 OK");

            exit;
        case 'free_register':
            $event_id = $input->get('event_id');
            // Search event by principal id
            $eventPage  = $pages->get("template=evento_template,id_boletify3=$event_id");
            try {
                $quota = $eventPage->event_quota;
                // Get number of person in this event

                $registredUsers = $pages->find('template=user, roles=member, user_events.event.id=' . $eventPage->id . 'sort=user_events.id', 'findAll=true')->count;
                if($registredUsers >= $quota) {
                    $session->redirect('/eventos/?dispatch=NO_QUOTA');
                    exit;
                }

                $extraData = '';
                if(isset($input->get->extra_field_1) && $input->get->extra_field_1 != '') {
                    $extraData = '&' . http_build_query(['extra_field_1' => $input->get->extra_field_1]);
                }

                // Otherwise we use a traditional call to user_event_link
                $email = user()->email;
                $urlToRequest = 'https://v-run.mx/user-event-linker/?dispatch=event_register&id_boletify=' . $event_id . '&email=' . $email . $extraData;
                $request = file_get_contents($urlToRequest);

                $session->redirect($pages->get('/eventos')->url . '?dispatch=SUCCESS_REGISTER');
            } catch(\Exception $e) {
                $session->redirect('/eventos/?dispatch=ERROR');
            }
            break;
        case 'generateExcel':
            try {
                $id     = $input->get('event_id');
                $event  = $pages->get($id);

                $isTrainningEvent = $event->event_type->title == 'Entrenamiento';
                $isPrivateEvent = $event->event_classification->title == 'Privado';
                $registredUsers = $pages->find('template=user, roles=member, user_events.event.id=' . $event->id, 'findAll=true');

                $headers = [];
                $headers['extra_label_1'] = $event->event_extra_label_1 != '' ? $event->event_extra_label_1 : 'Campo Extra 1';
                $headers['extra_label_2'] = $event->event_extra_label_2 != '' ? $event->event_extra_label_2 : 'Campo Extra 2';

                $resultsTable = [];
                $title = 'Resultados - ' . $event->title;

                if($event->event_type->title == 'Distancia' || $isTrainningEvent) {
                    $title .= ' ' . $event->event_distance . ' kms';
                    $subfix = 'mns';
                }
                else {
                    $title .= ' ' . $event->event_duration . ' mns';
                    $subfix = 'kms';
                }

                foreach($registredUsers as $rUser) {
                    $result = [];
                    if ($rUser->profile_picture)
                        $result['profile_picture'] = $rUser->profile_picture->url;
                    else
                        $result['profile_picture'] = $config->urls->assets . "images/no-user.jpg";

                    $rUserEvent = $rUser->user_events->get('event.id=' . $event->id);
                    $result['member'] = '<b>' . $rUserEvent->event_runner_number . '</b> - ' .ucwords(mb_strtolower(trim(sprintf('%s %s', $rUser->first_name, $rUser->last_name))));

                    if($rUser->address->count != 0) {
                        $result['state'] = $rUser->address[0]->address_state;
                        $result['address'] = ucwords(mb_strtolower(trim(sprintf('%s #%s%s, %s, ',
                            $rUser->address[0]->address_street, // Calle
                            $rUser->address[0]->address_outdoor_number, // No. Exterior
                            $rUser->address[0]->address_indoor_number != '' ? '-' . $rUser->address[0]->address_indoor_number : '', // No. Interior
                            $rUser->address[0]->address_suburb, // Colonia
                        ))));
                        $result['address'] .= sprintf(' CP. %s, %s, %s',

                            $rUser->address[0]->address_zip_code, // CÃ³digo Postal
                            $rUser->address[0]->address_state, // Estado,
                            $rUser->address[0]->address_city // Ciudad
                        );
                        if($rUserEvent->event_tracking_guide != '') {
                            $result['tracking_guide'] = $rUserEvent->event_tracking_guide;
                            $result['submit_timestamp'] = $rUserEvent->event_submit_timestamp;
                        }
                        else
                            $result['tracking_guide'] = '-';//'<input class="form-control" style="min-width: 250px; margin-top: 8px;" type="text"/>';
                    } else {
                        $result['state'] = 'N/A';
                        $result['tracking_guide'] = '';
                        $result['address'] = 'SIN INFORMACIÓN';
                    }

                    $result['phone'] = $rUser->phone;

                    $result['number'] = $rUserEvent->event_carrier ?? '-';
                    $result['id'] = $rUserEvent->id;

                    // if($event->event_type->title == 'Distancia' || $isTrainningEvent)
                    // else
                    $result['duration'] = $rUserEvent->event_total_duration . 'mns';
                    $result['distance'] = $rUserEvent->event_total_distance . 'kms';

                        switch ($rUserEvent->event_status->title) {
                            case 'Pendiente':
                                $result['status'] = '<div class="d-inline text-warning">' . $rUserEvent->event_status->title . '</div>';
                                break;
                            case 'Terminado':
                                $result['status'] = '<div class="d-inline text-success">' . $rUserEvent->event_status->title . '</div>';
                                break;
                            case 'Cancelado':
                                $result['status'] = '<div class="d-inline text-danger">' . $rUserEvent->event_status->title . '</div>';
                                break;
                        }
                    $result['plain_status'] =  $rUserEvent->event_status->title;
                    $result['email'] = $rUser->email;

                    //
                    $result['gender'] = @$rUser->gender->title ?: 'Masculino';
                    if($rUser->birthdate == '')
                        $result['age'] = '-';
                    else
                        $result['age'] = date_diff(date_create($rUser->birthdate), date_create(date('Y-m-d')))->format('%Y');

                    $result['evidences'] = $rUserEvent->evidence->count;
                    $result['extra_field_1'] = $rUserEvent->event_extra_field_1 != '' ? $rUserEvent->event_extra_field_1 : '-';
                    $result['extra_field_2'] = $rUserEvent->event_extra_field_2 != '' ? $rUserEvent->event_extra_field_2 : '-';

                    $resultsTable[] = $result;
                }

                usort($resultsTable, function($r1, $r2) use ($event, $isTrainningEvent) {
                    if($r1['plain_status'] == 'Terminado' && $r2['plain_status'] == 'Pendiente')
                        return -1;
                    if($r1['plain_status'] == 'Pendiente' && $r2['plain_status'] == 'Terminado')
                        return 1;

                    if($r1['plain_status'] == $r2['plain_status']) {
                        if($event->event_type->title == 'Distancia' || $isTrainningEvent) {
                            if($r1['duration'] == 0 && $r2['duration'] != 0) return 1;
                            if($r1['duration'] != 0 && $r2['duration'] == 0) return -1;
                            if(floatval($r1['duration']) > floatval($r2['duration']))
                                return 1;
                            else
                                return -1;
                            // return floatval($r1['duration']) - floatval($r2['duration']);
                        } else {
                            if($r1['distance'] == 0 && $r2['distance'] != 0) return 1;
                            if($r1['distance'] != 0 && $r2['distance'] == 0) return -1;
                            if(floatval($r2['distance']) > floatval($r1['distance']))
                                return 1;
                            else
                                return -1;
                            // return floatval($r2['distance']) - floatval($r1['distance']);
                        }
                    }
                });

                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet();


                $dataTable = [];
                try {
                    foreach ($resultsTable as $row) {
                        $dataTable[] = [
                            strip_tags($row['member']),
                            ($event->event_type->title == 'Distancia' || $isTrainningEvent) ? sprintf('%s / %s', $row['duration'], $row['distance']) : sprintf('%s / %s', $row['distance'], $row['duration']),
                            $row['state'],
                            strip_tags($row['status']),
                            $row['email'],
                            $row['number'],
                            $row['address'],
                            $row['phone'],
                            $row['extra_field_1'],
                            $row['extra_field_2']
                        ];
                    }
                } catch(\Exception $e) {
                    var_dump($row); die;
                }

                // Build sheet
                // Headers First
                $headers = ['Corredor', 'Registro', 'Estado', 'Progreso del evento', 'Email', 'Carrier', 'Dirección', 'Teléfono', 'Título del campo 1', 'Título del campo 2'];

                foreach (range('A', 'J') as $idx => $c) {
                    $sheet->setCellValue($c . '1', $headers[$idx]);
                }

                for($i = 2; $i < count($dataTable) + 2; $i++) {
                    foreach (range('A', 'J') as $j => $c) {
                        $sheet->setCellValue($c . $i, $dataTable[$i - 2][$j]);
                    }
                }

                foreach(range('A', 'J') as $i) {
                    $spreadsheet->getActiveSheet()->getColumnDimension($i)
                        ->setAutoSize(true);
                }

                $writer = new Xlsx($spreadsheet);

                // path where to store
                $tempDir = wire()->files->tempDir('reportFiles')->get();
                $fileName = uniqid() . '.xlsx';
                $filePath = $tempDir . $fileName;

                $writer->save($filePath);

                $content = file_get_contents($filePath);

                if(file_exists($filePath))
                    unlink($filePath);

                header('Content-type: application/ms-excel');
                header("Content-Disposition: attachment; filename=" . $fileName);
                exit($content);
                break;
            } catch(\Exception $e) {
                echo $e->getMessage();
                die;
            }
        case 'email_delivery':

            //

            // $eventId = 55115;
            // $registredUsers = $pages->find('template=user, roles=member, user_events.event.id=' . $eventId . 'sort=user_events.id', 'findAll=true');
            //
            // foreach ($registredUsers as $u) {
            //     $database->prepare('INSERT INTO email_queue(email, template) VALUES(:email, :template);')
            //              ->execute(['email' => $u->email, 'template' => 'message16k.html']);
            // }

            $starttime = microtime(true);

            $query = $database->prepare('SELECT * FROM email_queue WHERE delivered = 0 LIMIT 1');
            $query->execute();
            $data = [];

            while($row = $query->fetch()) {
                $data[] = $row;
            }

            $m = wireMail();

            $total = 0;

            foreach ($data as $row) {
                $bodyHTML = '';
                $bodyHTML = file_get_contents('mail/' . $row['template']);
                $delivered = $m->to($row['email'])
                       ->from('contacto@v-run.mx')
                       ->fromName('V Run')
                       ->subject('Comunicado')
                       ->bodyHTML($bodyHTML)
                       ->send();

               $total += $delivered;

               $database->prepare('UPDATE email_queue SET  delivered=:delivered WHERE id = :id')
                            ->execute(['delivered' => $delivered, 'id' => $row['id']]);
            }


            // $recipients[] = 'edgar.desarrollo@gmail.com';
            // $recipients[] = 'carlos.feria@ec-ideas.com';
            //

            // // $vars = [
            // //     '{$full_name}' => $data['full_name'],
            // //     '{$email}' => $data['email'],
            // //     '{$phone}' => $data['phone'],
            // //     '{$message}' => $data['message']
            // // ];
            // //
            // // $bodyHTML = strtr($bodyHTML, $vars);
            //
            // foreach ($recipients as $recipient) {
            //     $m->to($recipient)
            //         ->from('contacto@v-run.mx')
            //         ->fromName('V Run')
            //         ->subject('Contacto')
            //         ->bodyHTML($bodyHTML)
            //         ->send();
            // }

            // $endtime = microtime(true);
            // $timediff = $endtime - $starttime;
            // echo count($data) . ' ' . $timediff . 's'; die;
            // if(count($data) > 0) header("Refresh:0");
            echo $total;
            exit;
            break;

        case 'get_event':
            $id         = $input->get('id');
            $eventPage  = $pages->get($id);

            $images = [];
            foreach ($eventPage->event_images as $image) {
                $images[] = $image->httpUrl;
            }

            $event = [
                'id'            => $eventPage->id,
                'title'         => $eventPage->title,
                'name'          => $eventPage->name,
                'description'   => $eventPage->event_description,
                'end_date'      => $eventPage->event_end_date,
                'price'         => $eventPage->event_price,
                'image'         => $eventPage->event_images->eq(0)->httpUrl,
                'images'        => $images,
                'id_boletify'   => $eventPage->id_boletify,
                'id_boletify2'  => $eventPage->id_boletify2,
                'id_boletify3'  => $eventPage->id_boletify3,
                'boletify_url'  => $eventPage->boletify_url,
                'boletify_url2' => $eventPage->boletify_url2,
                'event_text_button'     => $eventPage->event_text_button,
                'event_text_button2'    => $eventPage->event_text_button2,
                'event_text_button2'    => $eventPage->event_text_button2,
                'event_request_text'    => $eventPage->event_request_text,
                'event_extra_label_1'   => $eventPage->event_extra_label_1,
                'is_conekta'            => $eventPage->is_conekta,
                'event_price1'          => $eventPage->event_price1,
                'event_price2'          => $eventPage->event_price2,
                'event_shipping1a'          => $eventPage->event_shipping1a,
                'event_shipping1b'          => $eventPage->event_shipping1b,
                'event_shipping2a'          => $eventPage->event_shipping2a,
                'event_shipping2b'          => $eventPage->event_shipping2b,
                'event_carrier'         => $eventPage->event_carrier,
                'event_carrier2'        => $eventPage->event_carrier2,
                'user_id'               => $user->id,
                'type'                  => $eventPage->event_type->title,
                'distance'              => $eventPage->event_distance,
                'duration'              => $eventPage->event_duration
            ];

            header('Content-Type: application/json');
            echo wireEncodeJSON(response(true, null, compact('event')), true, true); exit;
            break;

            break;
        case 'conekta_webhook':
            $body = @file_get_contents('php://input');
            $data = json_decode($body);
            http_response_code(200);

            if ($data->type == 'order.paid'){
                header('Content-Type: application/json');

                $email          = $data->data->object->metadata->email;
                $eventId        = $data->data->object->metadata->eventId;
                $eventCarrier   = $data->data->object->metadata->carrier;
                $shippingAmount = $data->data->object->metadata->shipping_amount;

                $u = users()->get('email=' . $email);
                $eventPage = $pages->get($eventId);

                $isPackage = $eventPage->template->name == 'paquete_template';
                $registerId = [];

                if ($isPackage) {
                    $events = [];

                    foreach($eventPage->events as $event)
                        $events[] = $event->event;
                }
                else
                    $events = [ $eventPage ];
                foreach ($events as $eventPage) {
                    $userEvent = $u->user_events->getNew();
                    $userEvent->event = $eventPage; //->add($eventPageId);
                    $userEvent->event_total_distance = 0.0;
                    $userEvent->event_total_duration = 0.0;
                    $userEvent->event_status = 'Pendiente';

                    $userEvent->event_carrier  = 1;
                    $userEvent->event_shipping = $shippingAmount;
                    // try {
                    //     $userEvent->event_shipping = @trim(implode('-', $eventCarrier)[0]);
                    // } catch(\Exception $e) {
                    //     $userEvent->event_shipping = '';
                    // }

                    $eventRecords = $pages->find('template=repeater_user_events, event.id='. $eventPage->id, 'findAll=true');
                    $idx = 0;
                    foreach ($eventRecords as $record) {
                        $idx = max($idx, intval($record->event_runner_number));
                    }

                    $padNumber = str_pad("" . $idx + 1, 4, "0", STR_PAD_LEFT);
                    $userEvent->event_runner_number = $padNumber;

                    $userEvent->save();

                    $u->of(false);
                    $u->user_events->add($userEvent);
                    $u->save();

                    $registerId[] = $userEvent->id;
                }

                echo wireEncodeJSON(response(true, 'Conekta order found correctly', compact('registerId')), true, true); exit;
            }

            header('Content-Type: application/json');
            echo wireEncodeJSON(response(false, 'Conekta order not found', null), true, true); exit;
            exit;
            break;
        case 'duplicated':
            $users = $users->find("start=0");
            echo '<table border="1">';
            foreach ($users as $u) {
                if($u->hasRole('superuser') || $u->email == '') continue;
                $eventsId = [];
                foreach($u->user_events as $eventPage) {
                    $eventsId[] = $eventPage->event->id;
                }


                // if(count(array_unique($eventsId)) < count($eventsId)) {
                    echo '
                        <tr>
                            <td>
                                ' . trim(sprintf('%s %s', $u->first_name, $u->last_name)) . '
                            </td>
                            <td>
                                ' . $u->email . '
                            </td>
                        </tr>
                    ';

                    $times = array_count_values($eventsId);
                    foreach ($times as $eventId => $time) {
                        // if($time == 1) {
                            $event = $pages->get($eventId);

                            echo '
                                <tr>
                                    <td>
                                        ' . $time . '
                                    </td>
                                    <td>
                                        ' . $event->title . '
                                    </td>
                                </tr>
                            ';
                        // }
                    }
                // }
                // if()
            }

            echo '</table>';
            exit;
            break;
        default:
            header("HTTP/1.1 404 Not Found");
            exit;
    }
}

function getCityName($cities, $id) {
    $selectedCity = @array_shift(array_filter($cities, function($c) use ($id) {
        return $c['id'] == $id;
    }));

    return $selectedCity['name'];
}

if($input->post->submit !== null) {
    // if(!$user->isLoggedIn()) $session->redirect('/login');
    $form = $input->post->form;
    switch($form) {
        case 'event_register':
            $eventPageId = wire('sanitizer')->text(wire('input')->post('event'));
            $eventPage  = $pages->get($eventPageId);
            $userEvent = $user->user_events->getNew();
            // var_dump($userEvent->event);die;
            $userEvent->event = $eventPage; //->add($eventPageId);
            $userEvent->event_total_distance = 0.0;
            $userEvent->event_total_duration = 0.0;
            $userEvent->event_status = 'Pendiente';
            $userEvent->save();
            $user->user_events->add($userEvent);
            $user->save();
            $session->redirect($pages->get('/eventos')->url . '?dispatch=SUCCESS_REGISTER');
            break;
        case 'submit_evidence':
            try {
                $data = array(
        			'id' => wire('sanitizer')->int(wire('input')->post('id')),
        			'evidence_distance' => wire('sanitizer')->float(wire('input')->post('evidence_distance')),
        			// 'evidence_duration' => wire('sanitizer')->float(wire('input')->post('evidence_duration'))
        			'evidence_duration_h' => wire('sanitizer')->int(wire('input')->post('evidence_duration_h')),
        			'evidence_duration_m' => wire('sanitizer')->int(wire('input')->post('evidence_duration_m')),
        			'evidence_duration_s' => wire('sanitizer')->int(wire('input')->post('evidence_duration_s'))
        		);

                $calculatedDuration = 0.0;

                $calculatedDuration += $data['evidence_duration_h'] * 60;
                $calculatedDuration += $data['evidence_duration_s'] * (1 / 60);
                $calculatedDuration += $data['evidence_duration_m'];

                // Principal join
                $userEvent = $pages->get($data['id']);
                $event = $userEvent->event;

                if(is_numeric($event->event_start_date) && is_numeric($event->event_end_date)) {
                    if(time() < $event->event_start_date || time() > $event->event_end_date) {
                        $session->redirect($pages->get('/perfil')->url . '?dispatch=NO_TIME');
                    }
                }

                $evidence  = $userEvent->evidence->getNew();
                $evidence->evidence_distance = 0.0;
                $evidence->evidence_duration = 0.0;

                $evidence->evidence_distance = $data['evidence_distance'];
                $evidence->evidence_duration = $calculatedDuration;

                // var_dump($evidence); die;

                $evidence->created_at = time();
                $evidence->of(false);
                $evidence->save();

                // Try to upload image
                $tempDir = wire()->files->tempDir('userUploads')->get();

                if(!is_dir($tempDir)) {
                    if(!wireMkdir($tempDir)) throw new WireException("No upload path!");
                }

                // setup new wire upload
                $u = (new WireUpload('image'))
                ->setMaxFiles(1)
                ->setMaxFileSize(10 * pow(2, 20))
                ->setDestinationPath($tempDir)
                ->setValidExtensions(["jpeg","jpg","png","gif"]);

                $files  = $u->execute();
                // var_dump($_FILES["image"]['error'],$files); die;

                foreach ($files as $file) {
                    $filePath = $tempDir . $file;
                    $evidence->evidence_images->add($filePath);
                    unlink($filePath);
                }


                $userEvent->evidence->add($evidence);
                $userEvent->of(false);
                $userEvent->save();

                // If everything is okay we update user progress
                $userEvent->event_total_distance += $data['evidence_distance'];
                $userEvent->event_total_duration += $calculatedDuration;

                $userEvent->of(false);
                $userEvent->save();

                $session->redirect($pages->get('/perfil')->url . '?dispatch=SUCCESS_UPLOAD');
            } catch(\Exception $e) {
                $session->redirect($pages->get('/perfil')->url . '?dispatch=ERROR_UPLOAD');
            }
            break;
        case 'profile_picture':
            // Try to upload image
            $tempDir = wire()->files->tempDir('userUploads')->get();

            if(!is_dir($tempDir)) {
                if(!wireMkdir($tempDir)) throw new WireException("No upload path!");
            }

            // set the max dimensions
           $targetWidth = 550;
           $targetHeight = 550;

            // setup new wire upload
            $u = (new WireUpload('image'))
            ->setMaxFiles(1)
            ->setOverwrite(true)
            ->setMaxFileSize(10 * pow(2, 20))
            ->setDestinationPath($tempDir)
            ->setValidExtensions(["jpeg","jpg","png","gif"]);

            $files  = $u->execute();
            $user->profile_picture->deleteAll();

            foreach ($files as $file) {
                $filePath = $tempDir . $file;
                $options = array('upscaling' => false, 'quality' => 100, 'sharpening' => 'soft');
                $imageSizer = new ImageSizer($filePath, $options);
                $imageSizer->resize($targetWidth, $targetHeight);
                $user->profile_picture->add($filePath);
                unlink($filePath);
            }

            $user->of(false);
            $user->save();

            $session->redirect($pages->get('/perfil')->url . '?dispatch=PROFILE_UPLOAD');

            break;
        case 'finish_event':
            $data = array(
                'id' => wire('sanitizer')->int(wire('input')->post('id')),
            );

            $userEvent = $pages->get($data['id']);

            $userEvent->event_status = 'Terminado';
            $userEvent->of(false);
            $userEvent->save();


            $u = users()->get('user_events.id=' . $data['id']);

            $u->distance_traveled    += $userEvent->event_total_distance;
            $u->minutes_traveled     += $userEvent->event_total_duration;
            $u->of(false);
            $u->save();

            $diploma = $userEvent->id;
            $videoUrl = $userEvent->event->event_finish_video->url;

            if(is_null($videoUrl))
                $session->redirect($pages->get('/perfil')->url . '?dispatch=SUCCESS_FINISH&hasDiploma=false');
            else
                $session->redirect($pages->get('/perfil')->url . '?dispatch=SUCCESS_FINISH&hasDiploma=true&diploma=' . $diploma . '&video=' . $videoUrl);

            break;
        case 'event_searcher':
            $data = array(
                'code' => wire('sanitizer')->text(wire('input')->post('code')),
            );
            $event = $pages->get("template=evento_template, event_classification=Privado, event_access_code=" . $data['code']);
            if(!$event->id) {
                header('Content-Type: application/json');
                echo wireEncodeJSON(response(false, 'Evento no encontrado'), true, true); exit;
            }

            header('Content-Type: application/json');
            echo wireEncodeJSON(response(true, null, [
                'id' => $event->id
            ]), true, true); exit;
            break;
        case 'private_evidences':
            $data = array(
                'id' => wire('sanitizer')->int(wire('input')->post('id')),
                'lock' => wire('sanitizer')->text(wire('input')->post('lock')) === 'true',
            );

            $userEvent = $pages->get($data['id']);
            $htmlContent = '';

            if ($userEvent->evidence->count > 0) {
                foreach ($userEvent->evidence as $evidence) {
                    $htmlContent .= '
                        <tr data-id="' . $evidence->id . '" data-ue="' . $userEvent->id . '">
                            <td>' . date('d/m/Y h:i:s a', $evidence->created_at ?: 0) . '</td>';
                            if($userEvent->event->event_type->title == 'Distancia'):
                                $htmlContent .= '<td>';
                                $htmlContent .= '<span class="read-evt">' . number_format($evidence->evidence_duration, 1) . ' mns / ' . number_format($evidence->evidence_distance, 1) . ' kms</span>';
                                $htmlContent .= '<div class="write-evt row" style="display: none; text-align" class="row">
                                                    <div class="col-md-4"><input class="form-control duration-evt" type="text" value="' . number_format($evidence->evidence_duration, 1) . '"/></div> mns /
                                                    <div class="col-md-4"><input class="form-control distance-evt" type="text" value="' . number_format($evidence->evidence_distance, 1) . '"/></div> kms
                                                </div>';
                                $htmlContent .= '</td>';
                            else:
                                $htmlContent .= '<td>';
                                $htmlContent .= '<span class="read-evt">' .number_format($evidence->evidence_distance, 1) . ' kms / ' . number_format($evidence->evidence_duration, 1) . ' mns</span>';
                                $htmlContent .= '<div class="write-evt row" style="display: none; text-align" class="row">
                                                    <div class="col-md-4"><input class="form-control distance-evt" type="text" value="' . number_format($evidence->evidence_distance, 1) . '"/></div> kms /
                                                    <div class="col-md-4"><input class="form-control duration-evt" type="text" value="' . number_format($evidence->evidence_duration, 1) . '"/></div> mns
                                                </div>';
                                $htmlContent .= '</td>';
                            endif;
                    $htmlContent .= '<td>
                                <a href="' . $evidence->evidence_images->url . '" target="_blank" class="read-evt"><i class="icon-camera"></i></a>';
                                if ($data['lock']) {
                                    $htmlContent .= '<a href="#" class="btn-edit read-evt" style="margin-left: 15px;"><i class="fa fa-pencil" style="color: #a800bb; font-size: 16px;"></i></a>';
                                    $htmlContent .= '<a href="#" class="btn-cancel write-evt" title="Cancelar" style="display: none; margin: 0 5px;"><i class="fa fa-times"></i></a>';
                                    $htmlContent .= '<a href="#" class="btn-send write-evt" title="Modificar Registro" style="display: none; margin: 0 5px;"><i class="fa fa-pencil"></i></a>';
                                    $htmlContent .= '<a href="#" class="btn-delete write-evt" title="Eliminar Registro" style="display: none; margin: 0 5px;"><i class="fa fa-trash"></i></a>';
                                }
                    $htmlContent .= '</td>
                        </tr>
                    ';
                }
            } else {
                $htmlContent = '<tr> <td style="text-align: center;" colspan="3"> NO SE HA REGISTRADO INFORMACIÓN</td> </tr>';
            }
            header('Content-Type: application/json');
            echo wireEncodeJSON(response(true, null, compact('htmlContent')), true, true); exit;
            break;
        case 'tracking_submit':
            // $data = array(
            //     'id' => wire('sanitizer')->int(wire('input')->post('id')),
            //     // 'tracking_guide' => wire('sanitizer')->text(wire('input')->post('tracking_guide')),
            // );

            $data = $input->post->data;
            $data['id'] = wire('sanitizer')->int(wire('input')->post('id'));

            $userEvent = $pages->get($data['id']);
            if(isset($data['tracking_guide']))
                $userEvent->event_tracking_guide = $data['tracking_guide'];
            $userEvent->event_extra_field_1 = $data['extra_field_1'];
            $userEvent->event_extra_field_2 = $data['extra_field_2'];
            $userEvent->event_submit_timestamp = time();
            $userEvent->of(false);
            $userEvent->save();

            header('Content-Type: application/json');
            echo wireEncodeJSON(response(true, 'Registro modificado éxitosamente'), true, true); exit;
            break;
        case 'edit_evidence':
            $data = array(
                'id' => wire('sanitizer')->int(wire('input')->post('id')),
                'user_event_id' => wire('sanitizer')->int(wire('input')->post('user_event_id')),
                'distance' => wire('sanitizer')->float(wire('input')->post('distance')),
                'duration' => wire('sanitizer')->float(wire('input')->post('duration')),
            );

            $u = users()->get('user_events.evidence.id=' . $data['id']);

            $evidence = $pages->get($data['id']);
            $evidence->evidence_distance = $data['distance'];
            $evidence->evidence_duration = $data['duration'];
            $evidence->of(false);
            $evidence->save();

            $userEvent = $pages->get($data['user_event_id']);

            if($userEvent->event->event_type->title == 'Distancia'):
                $label = number_format($evidence->evidence_duration, 1) . ' mns / ' . number_format($evidence->evidence_distance, 1) . ' kms';
            else:
                $label = number_format($evidence->evidence_distance, 1) . ' kms / ' . number_format($evidence->evidence_duration, 1) . ' mns';
            endif;

            // if EVERYTHING goes well we must a recalculation of everything.
            calculateProgress($u, $pages);

            header('Content-Type: application/json');
            echo wireEncodeJSON(response(true, 'Evidencia editada correctamente', compact('label')), true, true); exit;
            break;
        case 'remove_evidence':
            $data = array(
                'id' => wire('sanitizer')->int(wire('input')->post('id')),
            );

            $u = users()->get('user_events.evidence.id=' . $data['id']);

            $evidence = $pages->get($data['id']);
            $evidence->delete();

            calculateProgress($u, $pages);

            header('Content-Type: application/json');
            echo wireEncodeJSON(response(true, 'Evidencia eliminada correctamente'), true, true); exit;
            break;
        case 'generate_report':
            // First generate excel report with ids;
            $data = array(
                'ids' => $input->post->ids,
                'report' => $input->post->report
            );

            if($data['report'] !== '') {
                $token = $data['report'];
                $id = explode('-', $token)[0];

                $report = $pages->get($id);
                $url    = $report->report_file->url;
                $name   = $report->name;
                $id     = $report->id;

                header('Content-Type: application/json');
                echo wireEncodeJSON(response(true, null, compact('url', 'name', 'id')), true, true); exit;
                exit;
            };

            // Recreate templates
            foreach ($data['ids'] as $id) {
                $rUserEvent = $pages->get($id);
                $rUser = users()->find('user_events.id=' . $id)[0];
                $event = $rUserEvent->event;

                $result = [];
                $result['event'] = $event->title;

                $event->fields->get('event_color')->outputFormat = 0;

                //
                $color = $event->event_color;
                if($color == '' || is_null($color))
                    $color = '#bababa';
                //
                $result['color'] = $color;
                $result['email'] = $rUser->email;
                $result['number'] = $rUserEvent->event_carrier;
                $result['full_name'] = ucwords(mb_strtolower(trim(sprintf('%s %s', $rUser->first_name, $rUser->last_name))));

                $result['phone'] = $rUser->phone;

                // $result['id'] = $rUserEvent->id;

                if($rUser->address->count != 0) {
                    $result['state'] = $rUser->address[0]->address_state;
                    $result['address'] = ucwords(mb_strtolower(trim(sprintf('%s #%s%s, %s, ',
                        $rUser->address[0]->address_street, // Calle
                        $rUser->address[0]->address_outdoor_number, // No. Exterior
                        $rUser->address[0]->address_indoor_number != '' ? '-' . $rUser->address[0]->address_indoor_number : '', // No. Interior
                        $rUser->address[0]->address_suburb, // Colonia
                    ))));
                    $result['address'] .= sprintf(' CP. %s, %s, %s',

                        $rUser->address[0]->address_zip_code, // CÃ³digo Postal
                        $rUser->address[0]->address_state, // Estado,
                        $rUser->address[0]->address_city // Ciudad
                    );
                    if($rUserEvent->event_tracking_guide != '') {
                        $result['tracking_guide'] = $rUserEvent->event_tracking_guide;
                        $result['submit_timestamp'] = date('d/m/Y h:i:s a', $rUserEvent->event_submit_timestamp);
                    }
                    else {
                        $result['tracking_guide'] = '-';
                        $result['submit_timestamp'] = '-';
                    }
                } else {
                    $result['tracking_guide'] = '';
                    $result['state'] = 'N/A';
                    $result['address'] = 'SIN INFORMACIÓN';
                }

                $resultsTable[] = array_values($result);
            }

            // Now we have the excel format on $resultsTable var;

            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();

            // Build sheet
            // Headers First
            $headers = ['Evento', 'Color', 'Correo Electrónico', 'Carrier', 'Nombre Completo', 'Teléfono', 'Estado', 'Dirección', 'Guía de rastreo', 'Fecha de envío'];

            foreach (range('A', 'J') as $idx => $c) {
                $sheet->setCellValue($c . '1', $headers[$idx]);
            }

            for($i = 2; $i < count($resultsTable) + 2; $i++) {
                foreach (range('A', 'J') as $j => $c) {
                    $sheet->setCellValue($c . $i, $resultsTable[$i - 2][$j]);

                    if($j == 1) {
                        // Set backgorund color
                        $spreadsheet->getActiveSheet()->getStyle($c . $i)->getFill()
                                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                    ->getStartColor()->setRGB(str_replace('#', '', $resultsTable[$i - 2][$j]));

                        $sheet->setCellValue($c . $i, '       ');
                    }
                }
            }

            foreach(range('A', 'J') as $i) {
                $spreadsheet->getActiveSheet()->getColumnDimension($i)
                    ->setAutoSize(true);
            }

            $writer = new Xlsx($spreadsheet);


            // path where to store
            $tempDir = wire()->files->tempDir('reportFiles')->get();
            $fileName = uniqid() . '.xlsx';
            $filePath = $tempDir . $fileName;

            $writer->save($filePath);

            $lastReport = $pages->find('template=reporte_template')->last();

            if(!$lastReport)
                $batch = 1;
            else
                $batch = ($lastReport->report_id) + 1;

            $p = new Page();
            $p->template = 'reporte_template';
            $p->parent = $pages->get('reportes');
            $p->name = date('Ymd') . generateRandomString(3);

            $p->save();

            // Then populate custom fielts;

            $p->title = date('Ymd') . generateRandomString(3);
            $p->report_id = $batch;
            $p->report_date = time();
            $p->report_user = $user->id;

            if(file_exists($filePath)) {
                $p->report_file->add($filePath);
                unlink($filePath);
            }

            $p->of(false);
            $p->save();

            $url    = $p->report_file->url . $fileName;
            $name   = $p->name;
            $id     = $p->id;

            foreach ($data['ids'] as $id) {
                $userEvent = $pages->get($id);
                $userEvent->event_batch = $batch;
                $userEvent->of(false);
                $userEvent->save();
            }

            header('Content-Type: application/json');
            echo wireEncodeJSON(response(true, null, compact('url', 'name', 'id')), true, true); exit;

            break;
        case 'get_evidences':
            $data = array(
                'event' => wire('sanitizer')->int(wire('input')->post('event')),
            );
            $userEvent = $pages->get($data['event']);

            $actualEvidences = $userEvent->event_subevidences;
            $actualSubEvents = $userEvent->event->event_subevent->find('sort=subevent_reference_order');

            $totalSubEvents = count($actualSubEvents);
            $isTrainningFinished = count($actualEvidences) == $totalSubEvents;

            $alreadySubmitted = [];
            foreach ($actualEvidences as $evidence) {
                $alreadySubmitted[] = $evidence->subevidence_order;
            }

            $subEvidences = [];
            $order = null;
            foreach ($actualSubEvents as $subEvidence) {
                $tmp = [];
                $tmp['subevent'] = $subEvidence;
                $tmp['submitted'] = true;
                $order = $subEvidence->subevent_reference_order;
                if(!in_array($subEvidence->subevent_reference_order, $alreadySubmitted)) {
                    $tmp['submitted'] = false;
                    $tmp['evidence'] = null;

                    $subEvidences[] = $tmp;
                    break;
                }
                foreach ($actualEvidences as $evidence) {
                    if($evidence->subevidence_order == $order) {
                        $tmp['evidence'] = $evidence;
                        break;
                    }
                }
                $subEvidences[] = $tmp;
            }

            $htmlResponse = '';

            foreach ($subEvidences as $subEvidence) {
                $isImage = $subEvidence['subevent']->subevent_image_question_checkbox == '1';
                $isVideo = $subEvidence['subevent']->subevent_video_question_checkbox == '1';

                $htmlResponse .= '
                <div class="row lottery-slot">
                    <div class="col-md-4 lottery-card">
                        <b>#' . $subEvidence['subevent']->subevent_reference_order . '/' . $totalSubEvents . '</b>
                        <div class="image-holder">
                            <img src="' . $subEvidence['subevent']->subevent_image->url . '" class="lottery-image"/>
                        </div>
                    </div>
                ';
                if($subEvidence['submitted']) {
                    $htmlResponse .= '<div class="col-md-8  lottery-content">';
                    if($subEvidence['subevent']->subevent_open_question_checkbox == '1') {
                        $htmlResponse .= '<p data-l="answer">' . $subEvidence['subevent']->subevent_open_question . '</p>';

                        $htmlResponse .= '
                        <div class="row lottery-answer">
                            <p class="col-8">' .$subEvidence['evidence']->subevidence_answer . '</p>
                            <div class="col-4">
                                <a href="#" class="edit-subevidence" data-idx="' . $subEvidence['subevent']->subevent_reference_order . '" data-id="' . $subEvidence['evidence']->id . '" data-ans="true"><i class="fa fa-pencil"></i></a>
                            </div>
                        </div>
                        <div class="answer-input" style="display: none;">
                            <input type="text" class="form-control" value="' .$subEvidence['evidence']->subevidence_answer . '"/>
                        </div>
                        ';

                    } else {
                        $htmlResponse .= '<p data-l="media">' . $subEvidence['subevent']->subevent_image_question . '</p>';
                        $htmlResponse .= '
                        <div class="row">
                            <div class="col-6 offset-3 action-container">
                                <a href="' . $subEvidence['evidence']->subevidence_media->url . '" target="_blank"><i class="fa fa-eye"></i></a>
                                <a href="#" class="edit-subevidence" data-idx="' . $subEvidence['subevent']->subevent_reference_order . '" data-id="' . $subEvidence['evidence']->id . '" data-ans="false"><i class="fa fa-pencil"></i></a>
                            </div>
                        </div>
                        <div class="upload-btn-wrapper" style="display: none;">
                            <button class="btn btn-primary fake-input">' . ($isImage ? 'Cargar Imagen' : 'Cargar Video') . '</button>
                            <input class="file-input" type="file" accept="' . ($isImage ? 'image/*' : 'video/*') . '"/>
                            <small style="display: none;" class="file-name"></small>
                        </div>
                        ';
                    }
                    $htmlResponse .= '</div>';
                } else {
                    $htmlResponse .= '<div class="col-md-8 lottery-content">';
                    if($subEvidence['subevent']->subevent_open_question_checkbox == '1') {
                        $htmlResponse .= '<p data-l="answer">' . $subEvidence['subevent']->subevent_open_question . '</p>';
                        $htmlResponse .= '
                            <div>
                                <input type="text" class="form-control" name="answer" required/>
                            </div>
                        ';
                    } else {
                        $htmlResponse .= '<p data-l="media">' . $subEvidence['subevent']->subevent_image_question . '</p>';
                        $htmlResponse .= '
                            <div class="upload-btn-wrapper">
                                <button class="btn btn-primary fake-input">' . ($isImage ? 'Cargar Imagen' : 'Cargar Video') . '</button>
                                <input class="file-input" required type="file" name="file" accept="' . ($isImage ? 'image/*' : 'video/*') . '"/>
                                <small style="display: none;" class="file-name"></small>
                            </div>
                        ';
                    }
                    $htmlResponse .= '</div>';
                }
                $htmlResponse .= '</div>';
            }
            $htmlResponse = '
                <div class="row">
                    <h5>' . $userEvent->event->title . '</h5>
                    <input type="hidden" name="id" value="' . $userEvent->id . '"/>
                    <input type="hidden" name="order" value="' . $order . '"/>
                    <div class="col-12">
                    ' . $htmlResponse  . '
                    </div>
                </div>
            ';
            header('Content-Type: application/json');
            echo wireEncodeJSON(response(true, null, compact('isTrainningFinished', 'htmlResponse')), true, true); exit;

            break;
        case 'submit_subevidence':
            $data = array(
                'id' => wire('sanitizer')->int(wire('input')->post('id')),
                'subevent_id' => wire('sanitizer')->int(wire('input')->post('subevent_id')),
                'order' => wire('sanitizer')->int(wire('input')->post('order')),
                'answer' => wire('sanitizer')->text(wire('input')->post('answer')),
            );

            if($data['subevent_id'] != 0) {
                try {
                    $oldSubEvent = $pages->get($data['subevent_id']);
                    $oldSubEvent->delete();
                } catch(\Exception $e) { }
            }

            $userEvent = $pages->get($data['id']);

            $actualEvidences = $userEvent->event_subevidences;
            $actualSubEvents = $userEvent->event->event_subevent->find('sort=subevent_reference_order');

            $totalSubEvents = count($actualSubEvents);

            $subEvent = $userEvent->event_subevidences->getNew();

            $subEvent->subevidence_order = $data['order'];
            $subEvent->of(false);
            $subEvent->save();

            if(isset($_FILES['file'])) {
                // Then i'm gonna upload a media evidence
                // Try to upload image
                $tempDir = wire()->files->tempDir('userUploads')->get();

                if(!is_dir($tempDir)) {
                    if(!wireMkdir($tempDir)) throw new WireException("No upload path!");
                }

                // setup new wire upload
                $u = (new WireUpload('file'))
                ->setMaxFiles(1)
                ->setMaxFileSize(10 * pow(2, 20))
                ->setDestinationPath($tempDir)
                ->setValidExtensions(["jpeg","jpg","png","gif"]);

                $files  = $u->execute();
                // var_dump($_FILES["image"]['error'],$files); die;

                foreach ($files as $file) {
                    $filePath = $tempDir . $file;
                    $subEvent->subevidence_media->add($filePath);
                    unlink($filePath);
                }
            } else {
                // Just text answer
                $subEvent->subevidence_answer = $data['answer'];
            }

            // new new new patch v-run
            // @date 2020-10-06
            if($subEvent->subevidence_media == null && $subEvent->subevidence_answer == '') {
                // That means the evidence is actually empty, we'll work
                // in a handle for it, here is a list of reason why we think
                // it fails

                /*
                 *  1. The image is toooooo big to upload it we already use a 10MB limit
                 *      then the image is not uploaded that's why is empty the image file
                 */

                 $subEvent->delete();

                 $session->redirect($pages->get('/perfil')->url . '?dispatch=UPLOAD_ERROR');
                 return;
            }

            $userEvent->of(false);
            $userEvent->save();

            $userEvent->event_subevidences->add($subEvent);
            $userEvent->of(false);
            $userEvent->save();

            $actualEvidences = $userEvent->event_subevidences;
            $actualSubEvents = $userEvent->event->event_subevent->find('sort=subevent_reference_order');

            $isTrainningFinished = count($actualEvidences) == $totalSubEvents;

            if($isTrainningFinished)
                $session->redirect($pages->get('/perfil')->url . '?dispatch=FINISH_SET&event=' . $userEvent->event->title . '&id=' . $userEvent->id);
            else
                $session->redirect($pages->get('/perfil')->url . '?dispatch=SUCCESS_UPLOAD');

            break;
        case 'get_evidences_member':
            $data = array(
                'user_event_id' => wire('sanitizer')->int(wire('input')->post('user_event_id')),
                'lock' => wire('sanitizer')->text(wire('input')->post('lock')) === 'true',
            );

            $userEvent = $pages->get($data['user_event_id']);

            $actualEvidences = $userEvent->event_subevidences;
            $actualSubEvents = $userEvent->event->event_subevent->find('sort=subevent_reference_order');

            $totalSubEvents = count($actualSubEvents);
            $isTrainningFinished = count($actualEvidences) == $totalSubEvents;

            $alreadySubmitted = [];
            foreach ($actualEvidences as $evidence) {
                $alreadySubmitted[] = $evidence->subevidence_order;
            }

            $subEvidences = [];
            $order = null;
            foreach ($actualSubEvents as $subEvidence) {
                $tmp = [];
                $tmp['subevent'] = $subEvidence;
                $tmp['submitted'] = true;
                $order = $subEvidence->subevent_reference_order;
                if(!in_array($subEvidence->subevent_reference_order, $alreadySubmitted)) {
                    $tmp['submitted'] = false;
                    $tmp['evidence'] = null;

                    $subEvidences[] = $tmp;
                    break;
                }
                foreach ($actualEvidences as $evidence) {
                    if($evidence->subevidence_order == $order) {
                        $tmp['evidence'] = $evidence;
                        break;
                    }
                }
                $subEvidences[] = $tmp;
            }

            $htmlResponse = '';

            foreach ($subEvidences as $subEvidence) {
                $htmlResponse .= '
                <div class="row lottery-slot">
                    <div class="col-md-4 lottery-card">
                        <b>#' . $subEvidence['subevent']->subevent_reference_order . '/' . $totalSubEvents . '</b>
                        <div class="image-holder">
                            <img src="' . $subEvidence['subevent']->subevent_image->url . '" class="lottery-image"/>
                        </div>
                    </div>
                ';
                if($subEvidence['submitted']) {
                    $htmlResponse .= '<div class="col-md-8  lottery-content">';
                    if($subEvidence['subevent']->subevent_open_question_checkbox == '1') {
                        $htmlResponse .= '<p data-l="answer">' . $subEvidence['subevent']->subevent_open_question . '</p>';

                        $htmlResponse .= '
                        <div class="row lottery-answer">
                            <p class="col-8">' .$subEvidence['evidence']->subevidence_answer . '</p>';
                        if($data['lock']) {
                            $htmlResponse .= '
                                <div class="col-4">
                                    <a href="#" class="edit-subevidence" data-idx="' . $subEvidence['subevent']->subevent_reference_order . '" data-id="' . $subEvidence['evidence']->id . '" data-ans="true"><i class="fa fa-pencil"></i></a>
                                </div>
                            </div>
                            <div class="answer-input" style="display: none;">
                                <input type="text" class="form-control" value="' .$subEvidence['evidence']->subevidence_answer . '"/>
                            </div>
                            ';
                        } else {
                            $htmlResponse .= '</div>';
                        }

                    } else {
                        $htmlResponse .= '<p data-l="media">' . $subEvidence['subevent']->subevent_image_question . '</p>';
                        $htmlResponse .= '
                        <div class="row">
                            <div class="col-6 offset-3 action-container">
                                <a href="' . $subEvidence['evidence']->subevidence_media->url . '" target="_blank"><i class="fa fa-eye"></i></a>';
                                if($data['lock']) {
                                    $htmlResponse .= '
                                    <a href="#" class="edit-subevidence" data-idx="' . $subEvidence['subevent']->subevent_reference_order . '" data-id="' . $subEvidence['evidence']->id . '" data-ans="false"><i class="fa fa-pencil"></i></a>';
                                }
                            $htmlResponse .= '
                            </div>
                        </div>';
                        if($data['lock']) {
                            $htmlResponse .= '
                            <div class="upload-btn-wrapper" style="display: none;">
                                <button class="btn btn-primary fake-input">Cargar archivo</button>
                                <input class="file-input" type="file"/>
                                <small style="display: none;" class="file-name"></small>
                            </div>
                            ';
                        }
                    }
                    $htmlResponse .= '</div>';
                }
                $htmlResponse .= '</div>';
            }
            $htmlResponse = '
                <div class="row">
                    <h5>' . $userEvent->event->title . '</h5>
                    <input type="hidden" name="id" value="' . $userEvent->id . '"/>
                    <input type="hidden" name="order" value="' . $order . '"/>
                    <div class="col-12">
                    ' . $htmlResponse  . '
                    </div>
                </div>
            ';
            echo $htmlResponse; exit;

            break;
        case 'remove_user':
            $id = $input->get('id');

            $data = array(
                'id' => wire('sanitizer')->int(wire('input')->post('id')),
            );

            $userEvent = $pages->get($data['id']);


            $pages->delete($userEvent, true);

            header('Content-Type: application/json');
            echo wireEncodeJSON(response(true, 'Usuario elminado correctamente'), true, true); exit;

            break;
        case 'submit_invitation':
            $data = array(
                'id' => wire('sanitizer')->int(wire('input')->post('id')),
                'email' => wire('sanitizer')->email(wire('input')->post('email')),
                'extra_field_1' => wire('sanitizer')->text(wire('input')->post('extra_field_1')),
                'extra_field_2' => wire('sanitizer')->text(wire('input')->post('extra_field_2')),
            );

            $u = users()->get('email=' . $data['email']);
            $event = $pages->get($data['id']);


            if($u->id) {
                $urlToRedirect = '/resultados/?event=' . $event->name . '&dispatch=SUCCESS_REGISTER_OLD';
                $userEvents = [];

                foreach($u->user_events as $ue) {
                    if(isset($ue->event))
                        $userEvents[] = $ue->event->id;
                }

                if(in_array($event->id, $userEvents)) {
                    $session->redirect($urlToRedirect);
                    exit;
                }

                // user already exist
                $userEvent = $u->user_events->getNew();
                $userEvent->event = $event; //->add($eventPageId);
                $userEvent->event_total_distance = 0.0;
                $userEvent->event_total_duration = 0.0;
                $userEvent->event_status = 'Pendiente';

                $userEvent->event_carrier = 4;//;$event->event_carrier;

                $eventRecords = $pages->find('template=repeater_user_events, event.id='. $event->id, 'findAll=true');
                $idx = 0;
                foreach ($eventRecords as $record) {
                    $idx = max($idx, intval($record->event_runner_number));
                }

                $padNumber = str_pad("" . $idx + 1, 4, "0", STR_PAD_LEFT);
                $userEvent->event_runner_number = $padNumber;

                $userEvent->event_extra_field_1 = $data['extra_field_1'];
                $userEvent->event_extra_field_2 = $data['extra_field_2'];

                $userEvent->save();

                $u->of(false);
                $u->user_events->add($userEvent);
                $u->save();
            } else {
                // user doesnt exist
                $m = wireMail();

                $recipients = array($data['email']);

                $bodyHTML = file_get_contents('mail/invitation.html');
                $customLink = 'https://v-run.mx/invitacion/?token=' . encode_arr($data);
                $urlToRedirect = '/resultados/?event=' . $event->name . '&dispatch=SUCCESS_REGISTER_NEW&link=' . $customLink;

                $vars = [
                    '{$eventName}' => $event->title,
                    '{$eventImage}' => 'https://v-run.mx/' . $event->event_images->eq(0)->url,
                    '{$link}' => $customLink,
                ];

                $bodyHTML = strtr($bodyHTML, $vars);

                foreach ($recipients as $recipient) {
                    $m->to($recipient)
                        ->from('contacto@v-run.mx')
                        ->fromName('V Run')
                        ->subject('Invitación evento: ' . $event->title  .  '( V-RUN )')
                        ->bodyHTML($bodyHTML)
                        ->send();
                }
            }

            $session->redirect($urlToRedirect);
            break;
        default:
            var_dump($input->post);die;
            // $session->redirect('/');
    }
} else {
    if($input->post->form == 'make_payment') {
        $data = array(
            'event_id' => wire('sanitizer')->int(wire('input')->post('event_id')),
            'user_id' => wire('sanitizer')->int(wire('input')->post('user_id')),
            'price' => wire('sanitizer')->float(wire('input')->post('price')),
            'event_carrier' => wire('sanitizer')->text(wire('input')->post('event_carrier')),
            'description' => wire('sanitizer')->text(wire('input')->post('description')),
            'conektaTokenId' => wire('sanitizer')->text(wire('input')->post('conektaTokenId')),
            'shipping' => wire('sanitizer')->float(wire('input')->post('shipping')),
            'carrier' =>  wire('sanitizer')->text(wire('input')->post('shipping_carrier'))
        );

        \Conekta::initConfig();

        $eventPage = $pages->get($data['event_id']);
        $data['event'] = $eventPage;

        $order = \Conekta::cardPayment($data, $user);

        if(gettype($order) == 'string') {
            header('Content-Type: application/json');
            echo wireEncodeJSON(response(false, $order), true, true); exit;
        } else {
            if(get_class($order) == 'Conekta\ProcessingError') {
                header('Content-Type: application/json');
                echo wireEncodeJSON(response(false, $order->getMessage()), true, true); exit;
            }
        }

        if(isset($order)) {
            // $isPackage = $eventPage->template->name == 'paquete_template';
            //
            // if ($isPackage) {
            //     $events = [];
            //
            //     foreach($eventPage->events as $event)
            //         $events[] = $event->event;
            // }
            // else
            //     $events = [ $eventPage ];
            // foreach ($events as $eventPage) {
            //     $userEvent = $user->user_events->getNew();
            //     $userEvent->event = $eventPage; //->add($eventPageId);
            //     $userEvent->event_total_distance = 0.0;
            //     $userEvent->event_total_duration = 0.0;
            //     $userEvent->event_status = 'Pendiente';
            //
            //     $userEvent->event_carrier  = $data['event_carrier'];
            //     try {
            //         $userEvent->event_shipping = @trim(implode('-', $data['carrier'])[0]);
            //     } catch(\Exception $e) {
            //         $userEvent->event_shipping = '';
            //     }
            //
            //     $eventRecords = $pages->find('template=repeater_user_events, event.id='. $eventPage->id, 'findAll=true');
            //     $idx = 0;
            //     foreach ($eventRecords as $record) {
            //         $idx = max($idx, intval($record->event_runner_number));
            //     }
            //
            //     $padNumber = str_pad("" . $idx + 1, 4, "0", STR_PAD_LEFT);
            //     $userEvent->event_runner_number = $padNumber;
            //
            //     $userEvent->save();
            //
            //     $user->of(false);
            //     $user->user_events->add($userEvent);
            //     $user->save();
            // }

            $data = ['status' => true, 'message' => 'Pago realizado correctamente', 'data' => null];
            header('Content-Type: application/json');
            echo wireEncodeJSON($data, true, true);
            exit;
        }

        $data = ['status' => false, 'message' => 'Hubo un error para hacer el pago, por favor intentálo más tarde', 'data' => null];
        header('Content-Type: application/json');
        echo wireEncodeJSON($data, true, true);
        exit;
    }

    if($input->post->form == 'oxxo_payment') {
        $data = array(
            'event_id' => wire('sanitizer')->int(wire('input')->post('event_id')),
            'user_id' => wire('sanitizer')->int(wire('input')->post('user_id')),
            'price' => wire('sanitizer')->float(wire('input')->post('price')),
            'event_carrier' => wire('sanitizer')->text(wire('input')->post('event_carrier')),
            'description' => wire('sanitizer')->text(wire('input')->post('description')),
            'shipping' => wire('sanitizer')->float(wire('input')->post('shipping')),
            'carrier' =>  wire('sanitizer')->text(wire('input')->post('shipping_carrier'))
        );

        \Conekta::initConfig();

        $eventPage = $pages->get($data['event_id']);
        $data['event'] = $eventPage;

        $order = \Conekta::oxxoPayment($data, $user);

        if(gettype($order) == 'string') {
            header('Content-Type: application/json');
            echo wireEncodeJSON(response(false, $order), true, true); exit;
        } else {
            if(get_class($order) == 'Conekta\ProcessingError') {
                header('Content-Type: application/json');
                echo wireEncodeJSON(response(false, $order->getMessage()), true, true); exit;
            }
        }

        if(isset($order)) {
            $dompdf = new Dompdf();

            ob_start();
            require_once 'pdf/receipt.php';
            $pdfContent = ob_get_clean();
            // var_dump($pdfContent);die;
            $dompdf->loadHtml($pdfContent);
            $dompdf->render();

            $output = $dompdf->output();
            $fileName        = date('Ymd') . '_' . time() . '.pdf';
            $fileDestination = $_SERVER["DOCUMENT_ROOT"] . '/public/pdf/' . $fileName;
            file_put_contents($fileDestination, $output);

            $m = wireMail();
            $recipients = array($user->email);
            $bodyHTML = file_get_contents('mail/receipt.html');

            $vars = [
                '{$full_name}' => $data['full_name'],
                '{$email}' => $data['email'],
                '{$phone}' => $data['phone'],
                '{$message}' => $data['message']
            ];

            $bodyHTML = strtr($bodyHTML, $vars);

            foreach ($recipients as $recipient) {
                $m->to($recipient)
                    ->from('contacto@v-run.mx')
                    ->fromName('V Run')
                    ->subject('Recibo de pago en Oxxo')
                    ->bodyHTML($bodyHTML)
                    ->attachment($fileDestination, $fileName)
                    ->send();
            }


            $data = ['status' => true, 'message' => 'El recibo ha sido enviado a tu correo elecrtrónico, recuerda que tienes 7 días para completar tu inscripción', 'data' => null];
            header('Content-Type: application/json');
            echo wireEncodeJSON($data, true, true);
            exit;
        }

        $data = ['status' => false, 'message' => 'Hubo un error para hacer el pago, por favor intentálo más tarde', 'data' => null];
        header('Content-Type: application/json');
        echo wireEncodeJSON($data, true, true);
        exit;
    }

    if($input->post->form == 'spei_payment') {
        $data = array(
            'event_id' => wire('sanitizer')->int(wire('input')->post('event_id')),
            'user_id' => wire('sanitizer')->int(wire('input')->post('user_id')),
            'price' => wire('sanitizer')->float(wire('input')->post('price')),
            'event_carrier' => wire('sanitizer')->text(wire('input')->post('event_carrier')),
            'description' => wire('sanitizer')->text(wire('input')->post('description')),
            'shipping' => wire('sanitizer')->float(wire('input')->post('shipping')),
            'carrier' =>  wire('sanitizer')->text(wire('input')->post('shipping_carrier'))
        );

        \Conekta::initConfig();

        $eventPage = $pages->get($data['event_id']);
        $data['event'] = $eventPage;

        $order = \Conekta::speiPayment($data, $user);

        if(gettype($order) == 'string') {
            header('Content-Type: application/json');
            echo wireEncodeJSON(response(false, $order), true, true); exit;
        } else {
            if(get_class($order) == 'Conekta\ProcessingError') {
                header('Content-Type: application/json');
                echo wireEncodeJSON(response(false, $order->getMessage()), true, true); exit;
            }
        }

        if(isset($order)) {
            $bank   = $order->charges[0]->payment_method->receiving_account_bank;
            $number = $order->charges[0]->payment_method->receiving_account_number;
            $amount = "$ ". number_format($order->amount / 100, 2) . $order->currency;

            $data = ['status' => true, 'message' => null, 'data' => compact('bank', 'number', 'amount')];

            header('Content-Type: application/json');
            echo wireEncodeJSON($data, true, true);
            exit;
        }

        $data = ['status' => false, 'message' => 'Hubo un error para hacer el pago, por favor intentálo más tarde', 'data' => null];
        header('Content-Type: application/json');
        echo wireEncodeJSON($data, true, true);
        exit;
    }
    // $u = users()->get('name=admin');
    // $u->pass = 'admin321_@';
    // $u->of(false);
    // $u->save();
    // var_dump($u);die;
    $data = ['status' => false, 'message' => 'Params not found for correct working of this web service', 'data' => null];
    header('Content-Type: application/json');
    echo wireEncodeJSON($data, true, true);
    exit;
    // $session->redirect('/');
}
