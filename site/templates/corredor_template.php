<?php

header("Content-type: image/jpeg");

$id = intval($_GET['runner']);
$userEvent = $pages->get($id);

$event = $userEvent->event;

$eventImage = $event->event_image;

if($userEvent->event_extra_field_2 != '') {
    $extraText = $userEvent->event_extra_field_2;
}

if($eventImage)
    $imgPath = $eventImage->httpUrl;
else
    $imgPath = 'https://v-run.mx/site/assets/images/numner_tag.png';

$info = getimagesize($imgPath);
$mime = $info['mime'];

if($mime == 'image/jpeg') {
    $imageCreateFunction = 'imagecreatefromjpeg';
    $imageSaveFunction = 'imagejpeg';
    $newImageExtension = 'jpg';
} else if($mime == 'image/png') {
    $imageCreateFunction = 'imagecreatefrompng';
    $imageSaveFunction = 'imagepng';
    $newImageExtension = 'png';
}

$image = $imageCreateFunction($imgPath);
list($width, $height) = getimagesize($imgPath);

// $tmpImage = imagecreatetruecolor(800, 800);
// imagecopyresampled($tmpImage, $image, 0, 0, 0, 0, 800, 800, $width, $height);

$color = imagecolorallocate($image, 0, 0, 0);
$string = $userEvent->event_runner_number;
$fontSize = 150;

$font = '../assets/fonts/MavenPro-Black.ttf';

$x = 270;
$y = 500;

// imagestring($image, $fontSize, $x, $y, $string, $color);
// $bbox = imagettfbbox(10, 45, $font, $string);

// This is our cordinates for X and Y
// $x = $bbox[0] + (imagesx($image) / 2) - ($bbox[4] / 2);
// $y = $bbox[1] + (imagesy($image) / 2) - ($bbox[5] / 2) - 5;


imagettftext($image, $fontSize, 0, $x, $y, $color, $font, $string);

if(isset($extraText)) {
    // determine the size of the text so we can center it
    // $box = imagettfbbox(25, 0, $font, $extraText);
    // $textWidth = abs($box[2]) - abs($box[0]);
    // $imageWidth = imagesx($image);
    // $x = ($imageWidth - $textWidth) / 2;

    imagettftext($image, 35, 0, 410, 700, $color, $font, $extraText);
}

imagejpeg($image);
imagedestroy($image);
