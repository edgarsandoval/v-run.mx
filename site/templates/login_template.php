<?php require_once './header.inc'; ?>
<?php

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function registerUser($data) {
    try {
        $u = new User();
        $id = users()->find('roles.name=member')->count + 1;
        $u->name        = md5($id);
        $u->first_name  = $data['first_name'];
        $u->last_name   = $data['last_name'];
        $u->email       = $data['email'];
        $u->phone       = $data['phone'];
        $u->pass        = $data['pass'];
        $u->gender      = $data['gender'];
        $u->birthdate   = $data['birthdate'];
        $u->distance_traveled = 0.0;
        $u->created_at = time();
        $u->addRole("member");
        $u->save();

        return true;
    } catch(\Exception $e) {
        return false;
    }
}

function login($data, $session) {
    $u = $session->login($data['email'], $data['pass']);
    if($u !== null) {
        $user = $u;
        return true;
    }
    return false;
}

function dump($formData, $errorFields, $idx) {
    if(!in_array($idx, $errorFields) && isset($formData[$idx]))
        return $formData[$idx];
    return '';
}

$errors = [];

$formData = [];
$errorFields = [];

$render = 'login_register';
$template = 'login';

if($input->get('event_id') !== null) {
    $session->event_id = $input->get('event_id');
}

if($input->get('dispatch') !== null) {
    $dispatch = $input->get('dispatch');
    switch ($dispatch) {
        case 'LOGIN_FIRST':
            $errors[] = 'Inicia sesión para continuar interactuando <span style="text-decoration: underline; font-weight: bold; "> si no eres usuario aún da click <a href="/login/?v=RGSTR' . ( $input->get('event_id') !== null ? '&event_id=' . $input->get('event_id') : '')  .'" style="color: #3366BB; ">aquí</a> para registrarte.</span>';
            break;
    }
}

if($input->get('v') !== null && $input->get('v') != 'LGN') {
    $template = 'register';
}

if($input->post->submit !== null) {
    if($input->post->form == 'register') {
        // Register form, register new user

        // Password filter
        if($input->post->pass !== $input->post->get('pass-confirm')) {
            $errorFields[] = 'pass';
            $errorFields[] = 'pass-confirm';
            $errors[] = 'Las contraseñas no coinciden';
        }

        // Phone number filter
        if(!preg_match('/^[0-9]{10}+$/', $input->post->phone)) {
            $errorFields[] = 'phone';
            $errors[] = 'El número celular debe de ser de 10 dígitos y es importante para poder recibir tu contraseña en caso de no recordarla';
        }

        // Email filter
        if(users()->find('email=' . $input->post->email)->count != 0) {
            $errorFields[] = 'email';
            $errors[] = 'El correo electrónico que estás usando ya se encuentra registrado';
        }

        // Phone filter repeated
        if(users()->find('phone=' . $input->post->phone)->count != 0) {
            $errorFields[] = 'phone';
            $errors[] = 'El teléfono que estás usando ya se encuentra registrado';
        }

        $data = array(
			'first_name' => wire('sanitizer')->text(wire('input')->post('first_name')),
			'last_name' => wire('sanitizer')->text(wire('input')->post('last_name')),
			'email' => wire('sanitizer')->email(wire('input')->post('email')),
			'phone' => wire('sanitizer')->text(wire('input')->post('phone')),
            'gender' => wire('sanitizer')->text(wire('input')->post('gender')),
            'birthdate' => wire('sanitizer')->text(wire('input')->post('birthdate')),
			'pass' => wire('sanitizer')->text(wire('input')->post('pass'))
		);

        $formData = $data;

        if(count($errors) == 0 && registerUser($data)) {
            $render = 'register_success';
            $user = $session->login($data['email'], $data['pass']);
        }
    } else if($input->post->form == 'login') {
        $data = array(
			'email' => wire('sanitizer')->email(wire('input')->post('email')),
			'pass' => wire('sanitizer')->text(wire('input')->post('pass'))
		);

        if(login($data, $session)) {
            if($session->event_id != null)
                $session->redirect('/eventos');
            else
                $session->redirect('/perfil');
        } else {
            $errors[] = 'Usuario y/o contraseña incorrectos';
        }
    }
}

// if(count($errors) > 0)
    // $error = array_shift($errors);
// ?>

<div class="container padding-top-1x padding-bottom-3x">
    <?php if ($render == 'login_register'): ?>
        <?php if (count($errors) > 0): ?>
            <div class="alert alert-danger alert-dismissible fade show text-center margin-bottom-1x"><span class="alert-close" data-dismiss="alert"></span>
                <?php foreach ($errors as $error): ?>
                    <p><i class="fa fa-bell"></i> <?= $error ?> </p>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <div class="row">
            <?php if($template == 'login'):  ?>
            <div class="col-md-6 offset-md-3">
                <form class="login-box" method="post">
                    <input type="hidden" name="form" value="login">
                    <!-- <h4 class="margin-bottom-1x">Inicia sesión con una red social</h4> -->
                    <!-- <div class="row margin-bottom-1x"> -->
                        <!-- <div class="col-xl-4 col-md-6 col-sm-4"><a class="btn btn-sm btn-block facebook-btn" href="#"><i class="socicon-facebook"></i>&nbsp;Facebook login</a></div> -->
                        <!-- <div class="col-xl-4 col-md-6 col-sm-4"><a class="btn btn-sm btn-block twitter-btn" href="#"><i class="socicon-twitter"></i>&nbsp;Twitter login</a></div> -->
                        <!-- <div class="col-xl-4 col-md-6 col-sm-4"><a class="btn btn-sm btn-block google-btn" href="#"><i class="socicon-googleplus"></i>&nbsp;Google+ login</a></div> -->
                    <!-- </div> -->
                    <h4 class="margin-bottom-1x"><?= $page->login_title ?></h4>
                    <div class="form-group input-group">
                        <input class="form-control" type="email" name="email" placeholder="Ingresa correo electronico" required><span class="input-group-addon"><i class="icon-mail"></i></span>
                    </div>
                    <div class="form-group input-group">
                        <input class="form-control" type="password" name="pass" placeholder="Ingresa Contraseña" required><span class="input-group-addon"><i class="icon-lock"></i></span>
                    </div>
                    <div class="d-flex flex-wrap justify-content-between padding-bottom-1x text-right">
                        <a class="navi-link" href="/recuperar-contrasena" style="width: 100%;">¿Olvidaste tu contraseña?</a>
                    </div>
                    <div class="text-center text-sm-right">
                        <button class="btn btn-primary margin-bottom-none" type="submit" name="submit">Login</button>
                    </div>
                </form>
            </div>
            <?php else: ?>
            <div class="col-md-6 offset-md-3">
                <div class="padding-top-3x hidden-md-up"></div>
                <h3 class="margin-bottom-1x padding-top-1x">¿No tienes una cuenta aún? Registrate aquí</h3>
                <p><?= $page->register_title ?></p>
                <form class="row" method="post">
                    <input type="hidden" name="form" value="register">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="reg-fn">Nombre</label>
                            <input class="form-control" type="text" name="first_name" value="<?= dump($formData, $errorFields, 'first_name') ?>" id="reg-fn" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="reg-ln">Apellido</label>
                            <input class="form-control" type="text" name="last_name" value="<?= dump($formData, $errorFields, 'last_name') ?>" id="reg-ln" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="reg-email">Correo electrónico</label>
                            <input class="form-control" type="email" name="email" value="<?= dump($formData, $errorFields, 'email') ?>" id="reg-email" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="reg-phone">Numero telefonico (10 dígitos)</label>
                            <input class="form-control" type="text" name="phone" value="<?= dump($formData, $errorFields, 'phone') ?>" id="reg-phone" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="reg-gender">Género</label>
                            <select class="form-control" name="gender" value="<?= dump($formData, $errorFields, 'gender') ?>">
                                <option selected>Masculino</option>
                                <option>Femenino</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="reg-birthdate">Fecha de nacimiento</label>
                                <input class="form-control" type="date" name="birthdate" value="<?= dump($formData, $errorFields, 'birthdate') ?>" id="reg-birthdate" required />
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="reg-pass">Contraseña</label>
                            <input class="form-control" type="password" name="pass" value="<?= dump($formData, $errorFields, 'pass') ?>" id="reg-pass" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="reg-pass-confirm">Confirmar Contraseña</label>
                            <input class="form-control" type="password" name="pass-confirm" value="<?= dump($formData, $errorFields, 'pass-confirm') ?>" id="reg-pass-confirm" required>
                        </div>
                    </div>
                    <div class="col-12 text-center text-sm-right">
                        <button class="btn btn-primary margin-bottom-none" name="submit" type="submit">Registrarse</button>
                    </div>
                </form>
            </div>
            <?php endif; ?>
        </div>
        <script type="text/javascript">
            function onInit() {
                $('.facebook-btn').click(function() { alert('Inicio de sesión aún no disponible :(') })
                $('.twitter-btn').click(function() { alert('Inicio de sesión aún no disponible :(') })
                $('.google-btn').click(function() { alert('Inicio de sesión aún no disponible :(') })
            }
        </script>
    <?php endif; ?>
    <?php if ($render == 'register_success'): ?>
        <?php if ($session->event_id != null): ?>
            <?php  $session->redirect('/eventos'); ?>
        <?php else: ?>
            <script type="text/javascript">
                function onInit() {
                    Swal.fire(
                        'Usuario registrado con éxito',
                        'Serás redirigido a la página de inicio',
                        'success'
                    ).then(result => {
                        window.location.href = '/';
                    });
                }
            </script>
        <?php endif; ?>
    <?php endif; ?>
</div>

<?php require_once './footer.inc'; ?>
