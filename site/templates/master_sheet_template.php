<?php require_once './header.inc'; ?>
<?php if(!$user->isLoggedIn()) $session->redirect('/login'); ?>
<?php

ini_set('max_execution_time', 0);

$userOrganizeEvents = false;

$events = $pages->find("template=evento_template, event_visible=1", 'findAll=true');

if(in_array($user->email, [
    'belen.ofelia@hotmail.com',
    'marco.palencia@gmail.com',
    'edgar.desarrollo@gmail.com',
    'carlos.feria@ec-ideas.com',
]))
    $userOrganizeEvents = true;

foreach($events as $event) {
    if($userOrganizeEvents) break;
    $userOrganizeEvents = $event->event_organizator == $user->email || $user->email == 'edgar@ec-ideas.com';
}

if(!$userOrganizeEvents) $session->redirect('/');

$report = [ 'id' => null ];

$resultsTable = [];

if($input->get->token !== null) {
    try {
        $token = $input->get->token;
        $id = explode('-', $token)[0];

        $report = $pages->get($id);
        $report->token = sprintf('%s-%s', $report->id, $report->name);

        $userEvents = $pages->find('template=repeater_user_events, event_batch=' . $report->report_id, 'findAll=true');
        foreach ($userEvents as $rUserEvent) {
            $rUser = users()->find('user_events.id=' . $rUserEvent->id)[0];
            $event = $rUserEvent->event;

            $result = [];
            $result['event'] = $event->title;

            $event->fields->get('event_color')->outputFormat = 0;

            $color = $event->event_color;
            if($color == '')
                $color = 'rgb(186, 186, 186)';

            $result['color'] = '<div class="color-sample" style="background: ' . $color . '"></div>';

            $result['email'] = $rUser->email;
            $result['full_name'] = ucwords(mb_strtolower(trim(sprintf('%s %s', $rUser->first_name, $rUser->last_name))));

            $result['number'] = $rUserEvent->event_carrier;
            $result['id'] = $rUserEvent->id;

            if($rUser->address->count != 0) {
                $result['state'] = $rUser->address[0]->address_state;
                $result['address'] = ucwords(mb_strtolower(trim(sprintf('%s #%s%s, %s, ',
                    $rUser->address[0]->address_street, // Calle
                    $rUser->address[0]->address_outdoor_number, // No. Exterior
                    $rUser->address[0]->address_indoor_number != '' ? '-' . $rUser->address[0]->address_indoor_number : '', // No. Interior
                    $rUser->address[0]->address_suburb, // Colonia
                ))));
                $result['address'] .= sprintf(' CP. %s, %s, %s',

                    $rUser->address[0]->address_zip_code, // CÃ³digo Postal
                    $rUser->address[0]->address_state, // Estado,
                    $rUser->address[0]->address_city // Ciudad
                );
                if($rUserEvent->event_tracking_guide != '') {
                    $result['tracking_guide'] = $rUserEvent->event_tracking_guide;
                    $result['submit_timestamp'] = $rUserEvent->event_submit_timestamp;
                }
                else
                    $result['tracking_guide'] = '<input class="form-control" style="min-width: 250px; margin-top: 8px;" type="text"/>';
            } else {
                $result['tracking_guide'] = '';
                $result['state'] = 'N/A';
                $result['address'] = 'SIN INFORMACIÓN';
            }

            $resultsTable[] = $result;
        }
    } catch(\Exception $e) {
        $sesion->redirect('/');
    }
} else {
    if($user->email == 'edgar@ec-ideas.com' || $user->email == 'carlos.feria@ec-ideas.com')
        $events = $pages->find("template=evento_template, event_visible=1, event_finished=0", 'findAll=true');
    else
        $events = $pages->find("template=evento_template, event_visible=1, event_finished=0, event_organizator=" . $user->email, 'findAll=true');


    foreach ($events as $event) {
        $registredUsers = $pages->find('template=user, roles=member, user_events.event.id=' . $event->id, 'findAll=true');

        foreach($registredUsers as $rUser) {
            $result = [];
            $result['event'] = $event->title;

            $event->fields->get('event_color')->outputFormat = 2;


            $color = $event->event_color;
            if($color == '')
                $color = 'rgb(186, 186, 186)';

            $result['color'] = '<div class="color-sample" style="background: ' . $color . '"></div>';
            $result['email'] = $rUser->email;
            $result['full_name'] = ucwords(mb_strtolower(trim(sprintf('%s %s', $rUser->first_name, $rUser->last_name))));

            $rUserEvent = $rUser->user_events->get('event.id=' . $event->id);

            if($rUserEvent->event_batch !== '') continue;

            $result['id'] = $rUserEvent->id;
            $result['number'] = $rUserEvent->event_carrier;
            $result['shipping'] = $rUserEvent->event_shipping;

            if($rUser->address->count != 0) {
                $result['state'] = $rUser->address[0]->address_state;
                $result['address'] = ucwords(mb_strtolower(trim(sprintf('%s #%s%s, %s, ',
                    $rUser->address[0]->address_street, // Calle
                    $rUser->address[0]->address_outdoor_number, // No. Exterior
                    $rUser->address[0]->address_indoor_number != '' ? '-' . $rUser->address[0]->address_indoor_number : '', // No. Interior
                    $rUser->address[0]->address_suburb, // Colonia
                ))));
                $result['address'] .= sprintf(' CP. %s, %s, %s',

                    $rUser->address[0]->address_zip_code, // CÃ³digo Postal
                    $rUser->address[0]->address_state, // Estado,
                    $rUser->address[0]->address_city // Ciudad
                );
                if($rUserEvent->event_tracking_guide != '') {
                    $result['tracking_guide'] = $rUserEvent->event_tracking_guide;
                    $result['submit_timestamp'] = $rUserEvent->event_submit_timestamp;
                }
                else
                    $result['tracking_guide'] = '<input class="form-control" style="min-width: 250px; margin-top: 8px;" type="text"/>';
            } else {
                $result['tracking_guide'] = '';
                $result['state'] = 'N/A';
                $result['address'] = 'SIN INFORMACIÓN';
            }

            $resultsTable[] = $result;
        }
    }
}

$reports = $pages->find("template=reporte_template", 'findAll=true');

?>

<div class="filter-container" style="position: relative; ">
    <div class="form-group">
        <label> Reporte:
            <select class="form-control report-select">
                <option hidden selected disabled>Selecciona un reporte</option>
                <option value="*">Todos (por defecto)</option>
                <?php foreach ($reports as $pReport): ?>
                    <option value="<?= $pReport->id ?>-<?= $pReport->name; ?>" <?php echo $pReport->id == ($report->id ?? $report['id']) ? 'selected' : ''; ?>><?= $pReport->name; ?></option>
                <?php endforeach; ?>
            </select>
        </label>
    </div>
    <button class="btn btn-success btn-export" style="display: none;">Generar Bloque Excel <i class="fa fa-file-excel-o"></i></button>
</div>
<div id="master-sheet" class="container-fluid padding-top-1x padding-bottom-3x">
    <div class="row justify-content-center">
        <div class="col-lg-11">
            <h2 class="padding-top-2x text-center">Master Sheet</h2>
            <div class="table-responsive margin-bottom-none">
                <table class="table" id="result-table">
                    <thead>
                        <tr>
                            <th><input name="select_all" value="1" id="select-all" type="checkbox" <?php echo isset($report->id) ? 'checked disabled' : ''; ?> /></th>
                            <th>Evento</th>
                            <th>Color</th>
                            <th>Email</th>
                            <th>Carrier</th>
                            <th>Envío</th>
                            <th>Nombre Completo</th>
                            <th>Estado</th>
                            <th>Dirección</th>
                            <th>Guía de envío</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($resultsTable as $idx => $row): ?>
                            <tr>
                                <td><?= $row['id']; ?></td>
                                <td><?= $row['event']; ?></td>
                                <td><?= $row['color']; ?></td>
                                <td><?= $row['email']; ?></td>
                                <td><?=$row['number']; ?></td>
                                <td><?= @$row['shipping'] ?: '0' ?></td>
                                <td><?= $row['full_name']; ?></td>
                                <td><?= $row['state']; ?></td>
                                <td><?= $row['address']; ?></td>
                                <td><?= $row['tracking_guide']; ?></td>
                                <?php if ($row['tracking_guide'] != '' && !isset($row['submit_timestamp'])): ?>
                                    <td><button class="btn btn-danger btn-send" data-id="<?= $row['id']; ?>">Enviar <i class="fa fa-paper-plane"></i></button></td>
                                <?php elseif(isset($row['submit_timestamp'])): ?>
                                    <td><?php echo date('Y-m-d h:i:s a', $row['submit_timestamp']); ?></td>
                                <?php else: ?>
                                    <td></td>
                                <?php endif; ?>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<style media="screen">
table#result-table tbody tr td {
    text-align: center;
    vertical-align: middle;
}
</style>

<script type="text/javascript">
function onInit() {
    <?php if(isset($report->id)): ?>
        $('.btn-export').show();
    <?php endif; ?>

    Swal.fire('Espera un momento', 'Se está procesando la información');
    Swal.showLoading();

    var table = $('#result-table').DataTable({
        // processing: true,
        // serverSide: true,
        // ajax: 'https://v-run.mx/user-event-linker/?dispatch=master_sheet',
        language: {
            url: 'https://cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json'
        },
        columnDefs: [{
            targets: 0,
            searchable:false,
            orderable:false,
            className: 'dt-body-center',
            render: function (data, type, full, meta) {
                return '<input type="checkbox" name="id[]" value="'+ $('<div/>').text(data).html() + '"  <?php echo isset($report->id) ? 'checked disabled' : ''; ?> />';
            }
        }],
        order: [[3, 'asc']],
        createdRow: function(row, data, dataIndex) {
            $(row).attr("id", "tblRow_" + data[0]);
        },
        fnInitComplete: function() {
            Swal.close();
        },
        pageLength : 10,
        lengthMenu: [[5, 10, 20, 50, 100, -1], [5, 10, 20, 50, 100, 'Todos']]
    });

    // Handle click on "Select all" control
    $('#select-all').on('click', function() {
        // Check/uncheck all checkboxes in the table
        var rows = table.rows({ 'search': 'applied' }).nodes();
        $('input[type="checkbox"]', rows).prop('checked', this.checked);
    });

    // Handle click on checkbox to set state of "Select all" control
    $('#result-table tbody').on('change', 'input[type="checkbox"]', function() {
        if(this.checked)
            $(this).parents('tr').addClass('selected');
        else
            $(this).parents('tr').removeClass('selected');

        let rowsChecked = $('#result-table tbody input[type="checkbox"]:checked').length;
        if(rowsChecked > 0)
            $('.btn-export').show();
        else
            $('.btn-export').hide();

        // If checkbox is not checked
        if(!this.checked) {
            var el = $('#select-all').get(0);
            // If "Select all" control is checked and has 'indeterminate' property
            if(el && el.checked && ('indeterminate' in el)){
                // Set visual state of "Select all" control
                // as 'indeterminate'
                el.indeterminate = true;
            }
        }
    });

    var prevScrollpos = window.pageYOffset + 80;
    $(window).scroll(function(event) {
        var currentScrollPos = window.pageYOffset;

        if (prevScrollpos > currentScrollPos) {
            $('.filter-container').css({
                position: 'relative',
            });
        } else {
            $('.filter-container').css({
                position: 'fixed',
                top: '0'
            });
        }
        prevScrollpos = currentScrollPos;
    });

    $('.btn-export').click(function() {
        let report = $('.report-select').val();
        Swal.fire('Espera un momento', 'Se está procesando la información');
        Swal.showLoading();

        var ids = [];
        var rows = [];
        table.$('input[type="checkbox"]').each(function(idx, $el) {
            // If checkbox doesn't exist in DOM
            // if(!$.contains(document, this)) {
            // If checkbox is checked
            if(this.checked) {
                // Create a hidden element
                ids.push(this.value);
                rows.push($(this).parents('tr')[0]);
                // table.row($(this).parents('tr')[0]).remove().draw();
            }
            // }
        });

        if(<?php echo isset($report->id) ? 'true ||' : ''; ?> confirm('¿Estás seguro que deseas generar (' + ids.length + ') en bloque?')) {
            let data = {
                ids,
                report,
                form: 'generate_report',
                submit: ''
            };

            $.ajax({
                url: '/user-event-linker/',
                method: 'POST',
                data: data,
                success: function (response, status, jqXHR) {
                    if(response.status) {
                        if('url' in response.data) {
                            Swal.close();
                            let a = document.createElement("a");
                                a.download = response.data.name + '.xlsx';
                                a.href = response.data.url;
                                a.click();

                                if(report == null) {
                                    rows.forEach(row => table.row(row).remove().draw());
                                    $('.report-select').append(`<option id="${ response.data.id }-${ response.data.name }">${ response.data.name }</option>`);
                                }
                        }
                    }
                }
            });
        }
    });

    $('.report-select').change(function() {
        let token = $(this).val();

        if(token !== '*') {
            window.location.href = `/master-sheet/?token=${ token }`;
        } else {
            window.location.href = '/master-sheet/';
        }
    });
}
</script>
<?php require_once './footer.inc'; ?>
