<?php require_once './header.inc'; ?>

<div class="container padding-top-1x padding-bottom-3x">
        <div class="row justify-content-center">
            <!-- Start Blog Single Content -->
            <div class="col-lg-9">
                <h2 class="padding-top-2x text-center"><?= $page->title; ?></h2>
                <div>
                    <?= $page->body; ?>
                </div>
            </div>
            <!-- End Blog Single Content -->
        </div>
    </div>

<?php require_once './footer.inc'; ?>
