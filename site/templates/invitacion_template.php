<?php require_once './header.inc'; ?>
<?php

function decode_arr($data) {
    return unserialize(base64_decode($data));
}

function registerUser($data) {
    try {
        $u = new User();
        $id = users()->find('roles.name=member')->count + 1;
        $u->name        = md5($id);
        $u->first_name  = $data['first_name'];
        $u->last_name   = $data['last_name'];
        $u->email       = $data['email'];
        $u->phone       = $data['phone'];
        $u->pass        = $data['pass'];
        $u->gender      = $data['gender'];
        $u->birthdate   = $data['birthdate'];
        $u->distance_traveled = 0.0;
        $u->created_at = time();
        $u->addRole("member");
        $u->save();

        return true;
    } catch(\Exception $e) {
        return false;
    }
}

$formData = [];
$errors = [];
$errorFields = [];

function dump($formData, $errorFields, $idx) {
    if(!in_array($idx, $errorFields) && isset($formData[$idx]))
        return $formData[$idx];
    return '';
}

if(!isset($input->get->token)) $session->redirect('/');

$token = $input->get->token;
$data = decode_arr($token);
if($data == false) $session->redirect('/');


$formData['email'] = $data['email'];

$u = users()->get('email=' . $data['email']);
if($u->id) $session->redirect('/');

if($input->post->submit !== null) {
    // Password filter
    if($input->post->pass !== $input->post->get('pass-confirm')) {
        $errorFields[] = 'pass';
        $errorFields[] = 'pass-confirm';
        $errors[] = 'Las contraseñas no coinciden';
    }

    // Phone number filter
    if(!preg_match('/^[0-9]{10}+$/', $input->post->phone)) {
        $errorFields[] = 'phone';
        $errors[] = 'El número celular debe de ser de 10 dígitos y es importante para poder recibir tu contraseña en caso de no recordarla';
    }

    // Email filter
    // if(users()->find('email=' . $input->post->email)->count != 0) {
    //     $errorFields[] = 'email';
    //     $errors[] = 'El correo electrónico que estás usando ya se encuentra registrado';
    // }

    // Phone filter repeated
    if(users()->find('phone=' . $input->post->phone)->count != 0) {
        $errorFields[] = 'phone';
        $errors[] = 'El teléfono que estás usando ya se encuentra registrado';
    }

    $formData = array(
        'first_name' => wire('sanitizer')->text(wire('input')->post('first_name')),
        'last_name' => wire('sanitizer')->text(wire('input')->post('last_name')),
        'email' => $data['email'],
        'phone' => wire('sanitizer')->text(wire('input')->post('phone')),
        'gender' => wire('sanitizer')->text(wire('input')->post('gender')),
        'birthdate' => wire('sanitizer')->text(wire('input')->post('birthdate')),
        'pass' => wire('sanitizer')->text(wire('input')->post('pass'))
    );

    if(count($errors) == 0 && registerUser($formData)) {
        $user = $session->login($formData['email'], $formData['pass']);

        $u = $user;
        $event = $pages->get($data['id']);

        $userEvent = $u->user_events->getNew();
        $userEvent->event = $event; //->add($eventPageId);
        $userEvent->event_total_distance = 0.0;
        $userEvent->event_total_duration = 0.0;
        $userEvent->event_status = 'Pendiente';

        $userEvent->event_carrier = 4;//&$event->event_carrier;

        $registredUsers = $pages->find('template=user, roles=member, user_events.event.id=' . $event->id . 'sort=id', 'findAll=true');
        $idx = count($registredUsers);

        $padNumber = str_pad("" . $idx + 1, 4, "0", STR_PAD_LEFT);
        $userEvent->event_runner_number = $padNumber;

        $userEvent->event_extra_field_1 = $data['extra_field_1'];
        $userEvent->event_extra_field_2 = $data['extra_field_2'];

        $userEvent->save();

        $u->of(false);
        $u->user_events->add($userEvent);
        $u->save();

        $session->redirect($pages->get('/perfil')->url);
    }
}

?>

<div class="container padding-top-1x padding-bottom-3x">
    <?php if (count($errors) > 0): ?>
        <div class="alert alert-danger alert-dismissible fade show text-center margin-bottom-1x"><span class="alert-close" data-dismiss="alert"></span>
            <?php foreach ($errors as $error): ?>
                <p><i class="fa fa-bell"></i> <?= $error ?> </p>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="padding-top-3x hidden-md-up"></div>
            <h3 class="margin-bottom-1x padding-top-1x">¡Completa tu registro para comenzar a acomular kilometros!</h3>
            <p><?= $page->register_title ?></p>
            <form class="row" method="post">
                <input type="hidden" name="form" value="register">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="reg-fn">Nombre</label>
                        <input class="form-control" type="text" name="first_name" value="<?= dump($formData, $errorFields, 'first_name') ?>" id="reg-fn" required>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="reg-ln">Apellido</label>
                        <input class="form-control" type="text" name="last_name" value="<?= dump($formData, $errorFields, 'last_name') ?>" id="reg-ln" required>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="reg-email">Correo electrónico</label>
                        <input class="form-control" type="email" name="email" value="<?= dump($formData, $errorFields, 'email') ?>" id="reg-email" required>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="reg-phone">Numero telefonico (10 dígitos)</label>
                        <input class="form-control" type="text" name="phone" value="<?= dump($formData, $errorFields, 'phone') ?>" id="reg-phone" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="reg-gender">Género</label>
                        <select class="form-control" name="gender" value="<?= dump($formData, $errorFields, 'gender') ?>">
                            <option selected>Masculino</option>
                            <option>Femenino</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="reg-birthdate">Fecha de nacimiento</label>
                            <input class="form-control" type="date" name="birthdate" value="<?= dump($formData, $errorFields, 'birthdate') ?>" id="reg-birthdate" required />
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="reg-pass">Contraseña</label>
                        <input class="form-control" type="password" name="pass" value="<?= dump($formData, $errorFields, 'pass') ?>" id="reg-pass" required>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="reg-pass-confirm">Confirmar Contraseña</label>
                        <input class="form-control" type="password" name="pass-confirm" value="<?= dump($formData, $errorFields, 'pass-confirm') ?>" id="reg-pass-confirm" required>
                    </div>
                </div>
                <div class="col-12 text-center text-sm-right">
                    <button class="btn btn-primary margin-bottom-none" name="submit" type="submit">Registrarse</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php require_once './footer.inc'; ?>
