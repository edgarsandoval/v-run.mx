<?php

$id = intval($_GET['runner']);
$userEvent = $pages->get($id);
$event =  $userEvent->event;

$gpxFilePath = $event->event_route->httpUrl;
$u = users()->get('user_events.id=' . $id);

$hourTime = 0;
$minutesTime = 0;
$secondsTime = 0;

$hourTime = round($userEvent->event_total_duration / 60, 0);
$hourTime = str_pad($hourTime, 2, "0", STR_PAD_LEFT);

$minutesTime = round($userEvent->event_total_duration % 60, 0);
$minutesTime = str_pad($minutesTime, 2, "0", STR_PAD_LEFT);

$secondsTime = intval(($userEvent->event_total_duration - intval($userEvent->event_total_duration)) * 60);
$secondsTime = str_pad($secondsTime, 2, "0", STR_PAD_LEFT);

// var_dump('template=user, roles=member, user_events.event.id=' . $event->id . ', user_events.event_total_distance <= ' . $userEvent->event_total_distance. ', id <> ' . $u->id . ', sort=user_events.id', 'findAll=true');die;
$peopleBefore   = $pages->find('template=repeater_user_events, (event.id=' . $event->id .', event_total_distance<3),sort=id, limit=10', 'findAll=true');
$peopleAfter    = $pages->find('template=repeater_user_events, (event.id=' . $event->id .', event_total_distance>3),sort=id, limit=10', 'findAll=true');

$peopleBefore   = $pages->find("template=user, user_events=$peopleBefore", 'findAll=true');
$peopleAfter   = $pages->find("template=user, user_events=$peopleAfter", 'findAll=true');

$peopleBeforeDistance = array_map(function($u) use ($event) { return $u->user_events->get('event.id=' . $event->id)->event_total_distance; }, $peopleBefore->getArray());
$peopleAfterDistance = array_map(function($u) use ($event) { return $u->user_events->get('event.id=' . $event->id)->event_total_distance; }, $peopleAfter->getArray());

$defaultImage = $pages->get('template=default_image_template');

$sponsors = $event->sponsor;

if(is_null($u->profile_picture)) {
    $u->profile_picture = 'https://v-run.mx/site/assets/images/no-user.jpg';
}

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title><?= $page->title; ?> | V Run</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <title><?php echo $page->title; ?> | V Run</title>
        <!-- Mobile Specific Meta Tag -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Favicon -->
        <link rel="icon" type="image/x-icon" href="<?php echo $config->urls->assets?>images/favicon.ico?v=3">
    </head>
    <body>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA==" crossorigin="anonymous" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/assets/owl.carousel.css" rel="stylesheet" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
        <style media="screen">
            @import url("https://fonts.googleapis.com/css?family=Maven+Pro:400,500,700,900");
            #map { height: 100%; }
            html, body { height: 100%; margin: 0; padding: 0; overflow: hidden; }
            *:not([class*="fa"]) { font-family: "Maven Pro",Helvetica,Arial,sans-serif; }
            .buttonUp{border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;border:2px solid #c6c6c6;line-height:22px;height:22px;display:inline-block;background-color:#f5f5f5;background:#f5f5f5;background:-moz-linear-gradient(top,#fefefe 0,#eee 100%);background:-webkit-gradient(linear,left top,left bottom,color-stop(0,#fefefe),color-stop(100%,#eee));background:-webkit-linear-gradient(top,#fefefe 0,#eee 100%);background:-o-linear-gradient(top,#fefefe 0,#eee 100%);background:-ms-linear-gradient(top,#fefefe 0,#eee 100%);background:linear-gradient(to bottom,#fefefe 0,#eee 100%);color:#222;font-weight:700;text-decoration:none;text-align:center;width:80px;min-width:80px;font-size:12px;cursor:pointer;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}
            /* img[src*="v-run.mx"]:not(.info-logo) { border-radius: 50% !important; border: 1px solid rgb(168, 0, 187) !important; } */
            #page-content-wrapper,#wrapper{height:100%;width:100%}.nav-tabs>li{float:none}.nav-tabs{border-bottom:0}.nav-tabs>li.active>a,.nav-tabs>li.active>a:focus,.nav-tabs>li.active>a:hover{margin:0;border-radius:0}#wrapper{padding-left:0;padding-right:0;-webkit-transition:all .5s ease;-moz-transition:all .5s ease;-o-transition:all .5s ease;transition:all .5s ease}#wrapper.toggled-left{padding-left:250px}#wrapper.toggled-right{padding-right:250px}#sidebar-wrapper-left{z-index:1000;position:fixed;left:250px;width:0;height:100%;margin-left:-250px;overflow-y:auto;box-shadow:2px 2px 5px #aaa;border-top-right-radius:5px;border-bottom-right-radius:5px;-webkit-transition:all .5s ease;-moz-transition:all .5s ease;-o-transition:all .5s ease;transition:all .5s ease;background:#fff}#sidebar-wrapper-right{z-index:1000;position:fixed;right:250px;width:0;height:100%;margin-right:-250px;overflow-y:auto;box-shadow:2px 2px 5px #aaa;border-top-left-radius:5px;border-bottom-left-radius:5px;-webkit-transition:all .5s ease;-moz-transition:all .5s ease;-o-transition:all .5s ease;transition:all .5s ease;background:#fff}#wrapper.toggled-left #sidebar-wrapper-left{width:250px}#wrapper.toggled-right #sidebar-wrapper-right{width:250px}#page-content-wrapper{width:100%;position:absolute;padding:0}#wrapper.toggled-left #page-content-wrapper{position:absolute;margin-right:-250px}#wrapper.toggled-right #page-content-wrapper{position:absolute;margin-left:-250px}.sidebar-nav{position:absolute;top:0;width:250px;margin:0;padding:0;list-style:none}.sidebar-nav li{text-indent:20px;line-height:40px}.sidebar-nav li a{display:block;text-decoration:none;color:#999}.sidebar-nav li a:hover{text-decoration:none;color:#fff;background:rgba(255,255,255,.2)}.sidebar-nav li a:active,.sidebar-nav li a:focus{text-decoration:none}.sidebar-nav>.sidebar-brand{height:65px;font-size:18px;line-height:60px}.sidebar-nav>.sidebar-brand a{color:#999}.sidebar-nav>.sidebar-brand a:hover{color:#fff;background:0 0}
            .hide-btn{display:none;position:absolute;z-index:9999;top:calc(50% - 45px / 2);background:#fff!important;border:1px solid #aaa;height:45px;transform:translateX(-10px);width:35px;box-shadow:2px 2px 5px #aaa;border-top-right-radius:5px;border-bottom-right-radius:5px}
            .no-gutter{margin-right:0;margin-left:0}.no-gutter>[class*=col-]{padding-right:0;padding-left:0}
            .sidebar-content{text-align: center;}
            .info-content{width:90%;margin:0 auto;border:2px solid #ddd;border-radius:5px}.info-content ul{margin-left:15px;text-align:left;margin-top:15px;list-style:none;padding:0!important;font-size:12px;}.info-content ul li{margin-bottom:10px}
            /* Click the image one by one to see the different layout */

            /* Owl Carousel */

            .owl-prev {
              background: url('https://res.cloudinary.com/milairagny/image/upload/v1487938188/left-arrow_rlxamy.png') left center no-repeat;
              height: 54px;
              position: absolute;
              top: 50%;
              width: 27px;
              z-index: 1000;
              left: 2%;
              cursor: pointer;
              color: transparent;
              margin-top: -27px;
            }

            .owl-next {
              background: url('https://res.cloudinary.com/milairagny/image/upload/v1487938220/right-arrow_zwe9sf.png') right center no-repeat;
              height: 54px;
              position: absolute;
              top: 50%;
              width: 27px;
              z-index: 1000;
              right: 2%;
              cursor: pointer;
              color: transparent;
              margin-top: -27px;
            }

            .owl-prev:hover,
            .owl-next:hover {
              opacity: 0.5;
            }


            /* Owl Carousel */
            .owl-main .item img{
              display: block;
              width: 100%;
              height: auto;
            }

            .owl-stage {
                min-width: 10317px;
            }

            .owl-main {
                width: 80%;
                margin: 0 auto;
                margin-top: 15px;
            }

            #overlay {
                position: fixed; /* Sit on top of the page content */
                display: none; /* Hidden by default */
                width: 100%; /* Full width (cover the whole page) */
                height: 100%; /* Full height (cover the whole page) */
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                background-color: rgba(0,0,0,0.9); /* Black background with opacity */
                z-index: 9999999; /* Specify a stack order in case you're using a different order for other elements */
                cursor: pointer; /* Add a pointer on hover */
                align-items: center;
                justify-content: center;
            }

            .sponsor-list {
                list-style: none;
                padding: 0px;
            }

            .sponsor-list li {
                text-align: left;
                padding-left: 25px;
                cursor: pointer;
                border-bottom: 2px solid #eee;
                padding-top: 12px;
                padding-bottom: 12px;
                transition: all 0.5s;
                font-size: 12px;
            }

            .sponsor-list li:hover {
                background: #eee;
            }

            #sidebar-wrapper-right h4 {
                margin-bottom: 0px;
                padding-bottom: 20px;
                border-bottom: 1px solid #aaa;
                color: #a800bb;
            }

            .toggle-btn {
                position:absolute;
                z-index:9999;
                top:calc(50% - 45px / 2);
                background:#fff!important;
                border:1px solid #aaa;
                height:45px;
                transform:translateX(-10px);
                width:35px;
                box-shadow:2px 2px 5px #aaa;
                border-top-left-radius:5px;
                border-bottom-left-radius:5px;
                right: 240px;
            }

            .checkmark__circle {
                stroke-dasharray: 166;
                stroke-dashoffset: 166;
                stroke-width: 2;
                stroke-miterlimit: 10;
                stroke: #7ac142;
                fill: none;
                animation: stroke 0.6s cubic-bezier(0.65, 0, 0.45, 1) forwards;
            }

            .checkmark {
                width: 20px;
                height: 20px;
                border-radius: 50%;
                display: inline;
                stroke-width: 2;
                stroke: #fff;
                stroke-miterlimit: 10;
                box-shadow: inset 0px 0px 0px #7ac142;
                animation: fill .4s ease-in-out .4s forwards, scale .3s ease-in-out .9s both;
                margin-bottom: -5px;
                animation-iteration-count: infinite;
                animation-duration: 1.5s;
                margin-left: 10px;
            }

            .checkmark__check {
                transform-origin: 50% 50%;
                stroke-dasharray: 48;
                stroke-dashoffset: 48;
                animation: stroke 0.3s cubic-bezier(0.65, 0, 0.45, 1) 0.8s forwards;
            }

            @keyframes stroke {
                100% {
                    stroke-dashoffset: 0;
                }
            }
            @keyframes scale {
                0%, 100% {
                    transform: none;
                }
                50% {
                    transform: scale3d(1.1, 1.1, 1);
                }
            }
            @keyframes fill {
                100% {
                    box-shadow: inset 0px 0px 0px 30px #7ac142;
                }
            }
        </style>

        <div id="wrapper" class="toggled-right">
            <!-- Sidebar -->
            <div id="sidebar-wrapper-left">

            </div>
            <button class="hide-btn"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
            <!-- /#sidebar-wrapper-left -->
            <!-- Page Content -->
            <div id="page-content-wrapper">
                <div id="map"></div>
                <!-- <div id="playBtn" class="buttonUp" style="position: absolute; bottom: 20px; right: 145px; z-index: 99; width: 50px; min-width: 50px; display: block;" onmousedown="javascript:playRoute(this);" data-running="0" data-markers="1">
        				Play
        		</div> -->
            </div>
            <!-- /#page-content-wrapper -->
            <div id="overlay" onclick="this.style.display = 'none';"></div>

            <div id="sidebar-wrapper-right">
                <div style="text-align: center;">
                    <h4>Puntos de interés</h4>
                    <ul class="sponsor-list">
                        <?php foreach ($sponsors as $idx => $sponsor): ?>
                            <?php if ($sponsor->sponsor_distance < $userEvent->event_total_distance): ?>
                                <li onclick="focusMarker(<?= $idx ?>)" style="display: none;"><?= $sponsor->title; ?> - <?= $sponsor->sponsor_distance; ?> kms
                                    <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                        <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/>
                                        <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/>
                                    </svg>
                                </li>
                            <?php else: ?>
                                <li onclick="focusMarker(<?= $idx ?>)" style="display: none;"><?= $sponsor->title; ?> - <?= $sponsor->sponsor_distance; ?> kms</li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>

            </div>
            <button class="toggle-btn"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
        </div>
        <!-- /#wrapper -->


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC_RJ9E15PpNVwDjAEckLG82pb2-xVFcPQ&callback=initMap&libraries=&v=weekly" defer></script>
        <script src="<?php echo $config->urls->assets?>js/GPXParser.min.js"></script>
        <script src="<?php echo $config->urls->assets?>js/TIMER.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.js"></script>
        <script type="text/javascript">

            let gpx = new gpxParser();
            let map;

            let runnerDistance = <?= $userEvent->event_total_distance ?>;
            let runnerPoint = null;

            gpx.parse(`<?= file_get_contents($gpxFilePath); ?>`);

            let runnersBeforeDistance = JSON.parse('<?= json_encode($peopleBeforeDistance); ?>');
            let runnersAfterDistance = JSON.parse('<?= json_encode($peopleAfterDistance); ?>');

            let runnersBeforePoints = [];
            let runnersAfterPoints  = [];

            let runnersBeforeMarkers = [];
            let runnersAfterMarkers = [];

            let sponsors = [];

            let infowindow = null;

            var myWrapper = $("#wrapper");
            $("#menu-toggle").click(function(e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled-left");
                myWrapper.one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(e) {
                    // code to execute after transition ends
                    google.maps.event.trigger(map, 'resize');
                });
            });

            // var app = {
            //     routeTimeline: null,
            //     routeDistance: 0,
            //     drawRouteAnimation: function () {
            //         if (!this.routeTimeline) {
            //             this.routeTimeline = new TimeLine(20000, 33);
            //             this.routeTimeline.addEventListener('onChange', 'updateRouteAnimation', this);
            //         }
            //         this.routeTimeline.duration = Math.min(Math.round(this.routeDistance * 50000), 10000);
            //
            //         if (this.routeTimeline.status != 'PAUSE') {
            //             this.routeTimeline.position = 0;
            //             this.routeTimeline.direction = 1;
            //         }
            //         this.routeTimeline.play();
            //
            //         console.log(map.route.positions);
            //     },
            //     pauseRouteAnimation: function () {
            //         this.routeTimeline.pause();
            //     },
            //     updateRouteAnimation: function () {
            //         var positions = this.route.positions;
            //         var length = 0;
            //         var drawLength = this.routeDistance * this.routeTimeline.getTime(0, this.routeTimeline.duration);
            //         var points = [];
            //         var addToMap = 0;
            //         for (var iRoute = positions.length - 1; iRoute >= 0; iRoute--)
            //         {
            //         	var latLng = new google.maps.LatLng(positions[iRoute].lat, positions[iRoute].long);
            //         	points.push(latLng);
            //
            //
            //         	if (iRoute < positions.length - 1)
            //         	{
            //         		var distance = Math.sqrt(Math.pow(positions[iRoute].lat - positions[iRoute + 1].lat, 2) + Math.pow(positions[iRoute].long - positions[iRoute + 1].long, 2));
            //         		if (length + distance > drawLength)
            //         		{
            //         			points.pop();
            //
            //         			var diff = drawLength - length;
            //         			var angle = Math.atan2(positions[iRoute].lat - positions[iRoute + 1].lat, positions[iRoute].long - positions[iRoute + 1].long);
            //         			var lat  = Number(positions[iRoute + 1].lat ) + diff * Math.sin(angle);
            //         			var long = Number(positions[iRoute + 1].long) + diff * Math.cos(angle);
            //
            //
            //
            //
            //
            //         			var latLng = new google.maps.LatLng(lat, long);
            //         			points.push(latLng);
            //         			/**/
            //         			break;
            //         		}
            //         		length += distance;
            //         	}
            //
            //         	addToMap = iRoute;
            //         }
            //
            //         for (var iRoute = positions.length - 1; iRoute >= 0; iRoute--)
            //         {
            //         	var map = (iRoute >= addToMap) ? this.map : null;
            //         	if (positions[iRoute].marker.getMap() != map)
            //         	{
            //         		positions[iRoute].marker.setMap(map);
            //         	}
            //         }
            //
            //         //routeDetailPanel239796
            //
            //         this.line.setPath(points);
            //
            //         if (this.routeTimeline.duration == this.routeTimeline.position)
            //         {
            //             document.getElementById('playBtn').innerHTML = 'Play';
            //             document.getElementById('playBtn').onmousedown = function()
            //             {
            //                 TRINIUM.exec(TRINIUM.PLAY_ROUTE);
            //             }
            //         }
            //         }
            // };

            function measureDistance(lat1, lon1, lat2, lon2, unit = 'K') {
            	if ((lat1 == lat2) && (lon1 == lon2)) {
            		return 0;
            	}
            	else {
            		var radlat1 = Math.PI * lat1 / 180;
            		var radlat2 = Math.PI * lat2 / 180;
            		var theta = lon1 - lon2;
            		var radtheta = Math.PI * theta / 180;
            		var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
            		if (dist > 1) {
            			dist = 1;
            		}
            		dist = Math.acos(dist);
            		dist = dist * 180 / Math.PI;
            		dist = dist * 60 * 1.1515;

            		if (unit == "K") { dist = dist * 1.609344 }
            		if (unit == "N") { dist = dist * 0.8684 }
            		return dist;
            	}
            }

            function initMap() {
                map = new google.maps.Map(document.getElementById('map'), {
                    center: { lat: 20.659698, lng: -103.349609 },
                    zoom: 8
                });

                var noPoi = [{
                    featureType: "poi",
                        stylers: [
                            { visibility: "off" }
                        ]
                    }
                ];

                map.setOptions({ styles: noPoi });

                let route = gpx.routes[0];
                let routePoints = [];

                let firstPoint  = route.points[0];
                let lastPoint   = route.points[route.points.length - 1];

                let totalDistance = 0.0;
                let tmpPoint = null;

                for(let i = 0; i < runnersBeforeDistance.length; i++)
                    runnersBeforePoints.push(null);

                for(let i = 0; i < runnersAfterDistance.length; i++)
                    runnersAfterPoints.push(null);

                for(let point of route.points) {
                    if(tmpPoint != null) {
                        totalDistance += measureDistance(tmpPoint.lat, tmpPoint.lon, point.lat, point.lon);
                        if(runnerPoint == null && totalDistance >= runnerDistance) {
                            runnerPoint = tmpPoint;
                        }

                        for(let idx in runnersBeforePoints) {
                            if(runnersBeforePoints[idx] == null && totalDistance >= runnersBeforeDistance[idx])
                                runnersBeforePoints[idx] = tmpPoint;
                        }

                        for(let idx in runnersAfterPoints) {
                            if(runnersAfterPoints[idx] == null && totalDistance >= runnersAfterDistance[idx])
                                runnersAfterPoints[idx] = tmpPoint;
                        }
                    }

                    let lat = point.lat;
                    let lng = point.lon;
                    routePoints.unshift({ lat, lng });
                    tmpPoint = point;
                }

                for(let idx in runnersBeforePoints) {
                    if(runnersBeforePoints[idx] == null) {
                        runnersBeforePoints[idx] = firstPoint;
                    }
                }

                for(let idx in runnersAfterPoints) {
                    if(runnersAfterPoints[idx] == null) {
                        runnersAfterPoints[idx] = lastPoint;
                    }
                }

                let routePath = new google.maps.Polyline({
                    path: routePoints,
                    geodesic: true,
                    strokeColor: "#FF0000",
                    strokeOpacity: 1.0,
                    strokeWeight: 2,
                });

                routePath.setMap(map);
                //
                // var bounds = new google.maps.LatLngBounds();
                // routePath.getPath().forEach(function(e) {
                //         bounds.extend(e);
                // });
                //
                // map.fitBounds(bounds);

                // if(runnerPoint.lat !== firstPoint.lat) {
                    var firstMarker = new google.maps.Marker({
                        position: new google.maps.LatLng(firstPoint.lat, firstPoint.lon),
                        icon: 'https://v-run.mx/site/assets/images/start-flag.png?round=0',
                        zIndex: 99999
                    });

                    firstMarker.setMap(map);
                // }

                // if(runnerPoint.lat !== lastPoint.lat) {
                    var lastMarker = new google.maps.Marker({
                        position: new google.maps.LatLng(lastPoint.lat, lastPoint.lon),
                        icon: 'https://v-run.mx/site/assets/images/finish-flag.png?round=0',
                        zIndex: 99999
                    });

                    lastMarker.setMap(map);
                // }

                let runnerMarker = new google.maps.Marker({
                    position: new google.maps.LatLng(runnerPoint.lat, runnerPoint.lon),
                    icon: '<?= $u->profile_picture->size(40, 40, ['cropping' => 'north'])->httpUrl ?>?round=1',
                });

                const windowContent = `
                    <div style="text-align: center;">
                        <div style="display: flex; align-items: center; flex: 1; width: 120px; margin: 0 auto;">
                          <img src="<?= $u->profile_picture->size(50, 50)->httpUrl ?>" alt="" class="info-logo"/>
                          <a href="#" style="color: #606976; text-decoration: none !important; font-size: 20px; font-weight: bold; font-size: calc(45px / 2); pointer-events: none; margin-left: 10px;"><?= $userEvent->event_runner_number; ?></a>
                        </div>
                        <h3 style="color: rgb(168, 0, 187);"><?= trim(sprintf('%s %s', $u->first_name, $u->last_name)); ?></h3>
                        <p style="text-align: left;"><?= ($u->address->count == 0 ? '-' : ($u->address[0]->address_city . ' , ' . $u->address[0]->address_state))  ?></p>
                        <p style="text-align: left;"><b>Kilometros recorridos</b>: <?= $userEvent->event_total_distance ?> kms</p>
                        <p style="text-align: left;"><b>Tiempo recorridos</b>: <?= sprintf('%s : %s : %s', $hourTime, $minutesTime, $secondsTime); ?></p>
                    </div>
                `;

                infowindow = new google.maps.InfoWindow({
                    content: windowContent,
                });

                runnerMarker.addListener("click", () => {
                    closeAllInfoWindows();
                    infowindow.open(map, runnerMarker);
                });

                runnerMarker.setAnimation(google.maps.Animation.BOUNCE);

                runnerMarker.setMap(map);

                map.setCenter(runnerMarker.getPosition());
                map.setZoom(14);
                // NOT BEST IMPLEMENTATION

                <?php

                foreach ($peopleBefore as $idx => $u) {
                    $profileImage = is_null($u->profile_picture) ? $defaultImage->image : $u->profile_picture;
                    $userEvent = $u->user_events->get('event.id=' . $event->id);

                    $hourTime = 0;
                    $minutesTime = 0;
                    $secondsTime = 0;

                    $hourTime = round($userEvent->event_total_duration / 60, 0);
                    $hourTime = str_pad($hourTime, 2, "0", STR_PAD_LEFT);

                    $minutesTime = round($userEvent->event_total_duration % 60, 0);
                    $minutesTime = str_pad($minutesTime, 2, "0", STR_PAD_LEFT);

                    $secondsTime = intval(($userEvent->event_total_duration - intval($userEvent->event_total_duration)) * 60);
                    $secondsTime = str_pad($secondsTime, 2, "0", STR_PAD_LEFT);

                    echo 'runnersBeforeMarkers[' . $idx . '] = {};' . PHP_EOL;
                    echo 'runnersBeforeMarkers[' . $idx . '].marker = new google.maps.Marker({
                        position: new google.maps.LatLng(runnersBeforePoints[' . $idx . '].lat, runnersBeforePoints[' . $idx . '].lon),
                        icon: "' . $profileImage->size(40, 40, ['cropping' => 'north'])->httpUrl . '?round=1"
                    });' . PHP_EOL;

                    echo 'runnersBeforeMarkers[' . $idx . '].marker.addListener("click", () => {
                        if(!("info" in runnersBeforeMarkers[' . $idx . '])) {
                            runnersBeforeMarkers[' . $idx . '].info = new google.maps.InfoWindow({
                                content: `
                                <div style="text-align: center;">
                                    <div style="display: flex; align-items: center; flex: 1; width: 120px; margin: 0 auto;">
                                      <img src="' . $profileImage->size(50, 50)->httpUrl .'" alt="" class="info-logo"/>
                                      <a href="#" style="color: #606976; text-decoration: none !important; font-size: 20px; font-weight: bold; font-size: calc(45px / 2); pointer-events: none; margin-left: 10px;">' . $userEvent->event_runner_number . '</a>
                                    </div>
                                    <h3 style="color: rgb(168, 0, 187);">' . trim(sprintf('%s %s', $u->first_name, $u->last_name)) . '</h3>
                                    <p style="text-align: left;">' . ($u->address->count == 0 ? '-' : ($u->address[0]->address_city . ' , ' . $u->address[0]->address_state)) . '</p>
                                    <p style="text-align: left;"><b>Kilometros recorridos</b>: ' . $peopleBeforeDistance[$idx] . ' kms</p>
                                    <p style="text-align: left;"><b>Tiempo recorridos</b>: ' . sprintf('%s : %s : %s', $hourTime, $minutesTime, $secondsTime) . '</p>
                                </div>
                                `,
                            });
                        }

                        closeAllInfoWindows();

                        runnersBeforeMarkers[' . $idx . '].info.open(map, runnersBeforeMarkers[' . $idx . '].marker);
                    });' . PHP_EOL;

                    echo 'runnersBeforeMarkers[' . $idx . '].marker.addListener("mouseover", () => {
                            runnersBeforeMarkers[' . $idx . '].marker.setAnimation(google.maps.Animation.BOUNCE);
                    });' . PHP_EOL;

                    echo 'runnersBeforeMarkers[' . $idx . '].marker.addListener("mouseout", () => {
                            runnersBeforeMarkers[' . $idx . '].marker.setAnimation(null);
                    });' . PHP_EOL;

                    echo 'runnersBeforeMarkers[' . $idx . '].marker.setMap(map);' . PHP_EOL;
                }

                foreach ($peopleAfter as $idx => $u) {
                    $profileImage = is_null($u->profile_picture) ? $defaultImage->image : $u->profile_picture;
                    $userEvent = $u->user_events->get('event.id=' . $event->id);

                    $hourTime = 0;
                    $minutesTime = 0;
                    $secondsTime = 0;

                    $hourTime = round($userEvent->event_total_duration / 60, 0);
                    $hourTime = str_pad($hourTime, 2, "0", STR_PAD_LEFT);

                    $minutesTime = round($userEvent->event_total_duration % 60, 0);
                    $minutesTime = str_pad($minutesTime, 2, "0", STR_PAD_LEFT);

                    $secondsTime = intval(($userEvent->event_total_duration - intval($userEvent->event_total_duration)) * 60);
                    $secondsTime = str_pad($secondsTime, 2, "0", STR_PAD_LEFT);

                    echo 'runnersAfterMarkers[' . $idx . '] = {};' . PHP_EOL;
                    echo 'runnersAfterMarkers[' . $idx . '].marker = new google.maps.Marker({
                        position: new google.maps.LatLng(runnersAfterPoints[' . $idx . '].lat, runnersAfterPoints[' . $idx . '].lon),
                        icon: "' . $profileImage->size(40, 40, ['cropping' => 'north'])->httpUrl . '?round=1"
                    });' . PHP_EOL;

                    echo 'runnersAfterMarkers[' . $idx . '].marker.addListener("click", () => {
                        if(!("info" in runnersAfterMarkers[' . $idx . '])) {
                            runnersAfterMarkers[' . $idx . '].info = new google.maps.InfoWindow({
                                content: `
                                <div style="text-align: center;">
                                    <div style="display: flex; align-items: center; flex: 1; width: 120px; margin: 0 auto;">
                                      <img src="' . $profileImage->size(50, 50)->httpUrl .'" alt="" class="info-logo"/>
                                      <a href="#" style="color: #606976; text-decoration: none !important; font-size: 20px; font-weight: bold; font-size: calc(45px / 2); pointer-events: none; margin-left: 10px;">' . $userEvent->event_runner_number . '</a>
                                    </div>
                                    <h3 style="color: rgb(168, 0, 187);">' . trim(sprintf('%s %s', $u->first_name, $u->last_name)) . '</h3>
                                    <p style="text-align: left;">' . ($u->address->count == 0 ? '-' : ($u->address[0]->address_city . ' , ' . $u->address[0]->address_state)) . '</p>
                                    <p style="text-align: left;"><b>Kilometros recorridos</b>: ' . $peopleAfterDistance[$idx] . ' kms</p>
                                    <p style="text-align: left;"><b>Tiempo recorridos</b>: ' . sprintf('%s : %s : %s', $hourTime, $minutesTime, $secondsTime) . '</p>
                                </div>
                                `,
                            });
                        }

                        closeAllInfoWindows();

                        runnersAfterMarkers[' . $idx . '].info.open(map, runnersAfterMarkers[' . $idx . '].marker);
                    });' . PHP_EOL;

                    echo 'runnersAfterMarkers[' . $idx . '].marker.addListener("mouseover", () => {
                            runnersAfterMarkers[' . $idx . '].marker.setAnimation(google.maps.Animation.BOUNCE);
                    });' . PHP_EOL;

                    echo 'runnersAfterMarkers[' . $idx . '].marker.addListener("mouseout", () => {
                            runnersAfterMarkers[' . $idx . '].marker.setAnimation(null);
                    });' . PHP_EOL;

                    echo 'runnersAfterMarkers[' . $idx . '].marker.setMap(map);' . PHP_EOL;

                    // if (!is_null($u->gender->title) && $u->gender->title == 'Masculino') {
                    //     echo 'new google.maps.Marker({
                    //         position: new google.maps.LatLng(runnersAfterPoints[' . $idx . '].lat, runnersAfterPoints[' . $idx . '].lon),
                    //         map,
                    //         icon: "https://v-run.mx/site/assets/images/female-shoe.png?round=0"
                    //     });' . PHP_EOL;
                    // } else {
                    //     echo 'new google.maps.Marker({
                    //         position: new google.maps.LatLng(runnersAfterPoints[' . $idx . '].lat, runnersAfterPoints[' . $idx . '].lon),
                    //         map,
                    //         icon: "https://v-run.mx/site/assets/images/male-shoe.png?round=0"
                    //     });' . PHP_EOL;
                    // }

                }


                foreach ($sponsors as $idx => $sponsor) {
                    echo 'sponsors[' . $idx . '] = {};' . PHP_EOL;
                    echo 'sponsors[' . $idx . '].marker = new google.maps.Marker({
                        position: new google.maps.LatLng(' . $sponsor->map->lat . ', ' . $sponsor->map->lng . '),
                        icon: "' . $sponsor->sponsor_icon->httpUrl .'?round=0",
                        zIndex: 99999
                    });' . PHP_EOL;

                    echo 'sponsors[' . $idx . '].marker.addListener("click", () => {
                        let images = [];' . PHP_EOL;
                        foreach ($sponsor->sponsor_gallery as $img) {
                            echo 'images.push("' . $img->httpUrl . '");' . PHP_EOL;
                        }
                    echo '
                        let place = {
                            title: "' . $sponsor->title . '",
                            slogan: "' . $sponsor->slogan . '",
                            images,
                            site_url: "' . $sponsor->site_url . '",
                            social_link_1: "' . $sponsor->social_link_1 . '",
                            social_link_2: "' . $sponsor->social_link_2 . '",
                            sponsor_phone_1: "' . $sponsor->sponsor_phone_1 . '",
                            sponsor_phone_2: "' . $sponsor->sponsor_phone_2 . '",
                        };
                        openSideBar(place);
                    });' . PHP_EOL;


                    echo 'sponsors[' . $idx . '].marker.setMap(map);' . PHP_EOL;
                }

                ?>

                //  Set marker style
                setTimeout(() => {
                    document.querySelectorAll('img[src*="round=1"]').forEach(img => {
                        img.parentNode.style.border = '1.5px solid rgb(168, 0, 187)';
                        img.parentNode.style.borderRadius = '50%';
                    })

                    // document.querySelectorAll('img[src*="round=0"]').forEach(img => {
                    //     img.parentNode.style.border = '0px';
                    //     img.parentNode.style.borderRadius = '0%';
                    // })
                }, 700);
             }

             function openSideBar(place) {
                 $('.hide-btn').show();
                 $('.toggle-btn').hide();
                 $("#wrapper").addClass("toggled-left");
                 // console.log(place);

                 hideSidebar();

                 let list = '<ul>';
                 if(place.site_url != '')
                     list +=  `<li><i class="fab fa-internet-explorer"></i> <a href="${ place.site_url }" target="_blank">${ place.site_url }</a></li>`;

                 if(place.social_link_1 != '')
                     list +=  `<li><i class="fas fa-hashtag"></i> <a href="${ place.social_link_1 }" target="_blank">${ place.site_url }</a></li>`;

                 if(place.social_link_2 != '')
                     list +=  `<li><i class="fas fa-hashtag"></i> <a href="${ place.social_link_2 }" target="_blank">${ place.site_url }</a></li>`;

                 if(place.sponsor_phone_1 != '')
                     list +=  `<li><i class="fas fa-phone"></i> ${ place.sponsor_phone_1 }</li>`;

                 if(place.sponsor_phone_2 != '')
                     list +=  `<li><i class="fas fa-phone"></i> ${ place.sponsor_phone_2 }</li>`;

                 list += '</ul>';

                 let imagesCarrousel = '<div class="owl-main owl-carousel owl-theme">';
                 for(let image of place.images)
                    imagesCarrousel += `<div class="item"><img src="${ image }" /></div>`;
                 imagesCarrousel += '</div>';


                 $('#sidebar-wrapper-left').html(`
                     <div class="row sidebar-content no-gutter">
                         <div class="col-xs-12"><h3>${ place.title }</h3></div>
                         <div class="col-xs-12 ${ place.slogan == '' ? 'hidden' : '' }"><small>"${ place.slogan }"</small></div>
                         <div class="col-xs-12">
                            <hr/>
                            <div class="info-content">
                                ${ list }
                            </div>
                         </div>
                         <div class="col-xs-12">
                            ${ imagesCarrousel }
                         </div>
                     </div>
                 `);

                 setTimeout(function() {
                    $('.owl-carousel').owlCarousel({
                        stagePadding: 0,
                        items: 1,
                        loop:true,
                        margin:0,
                        singleItem:true,
                        nav:true,
                        navText: [
                            "<i class='fa fa-caret-left'></i>",
                            "<i class='fa fa-caret-right'></i>"
                        ],
                        dots:true
                    });

                    $('.owl-main .item').click(function(event) {
                        let img = $(this).find('img');
                        $('#overlay').html(`<img src="${ img.attr('src') }" style="max-width: 75%;"/>`);
                        $('#overlay').css('display', 'flex');
                    });
                }, 500);
             }

             function closeAllInfoWindows() {
                 runnersAfterMarkers.forEach(r => {
                     if(r.info)
                         r.info.close();
                 });

                 runnersBeforeMarkers.forEach(r => {
                     if(r.info)
                         r.info.close();
                 });

                 infowindow.close();
             }

            function playRoute(el) {
                let isRunning = el.getAttribute('data-running') == '1';
                let visibleMarkers = el.getAttribute('data-markers') == '1';

                if(isRunning) {
                    el.setAttribute('data-running', '0');
                    el.innerHTML = 'Play';
                    app.pauseRouteAnimation();
                } else {
                    el.setAttribute('data-running', '1');

                    if(visibleMarkers) {
                        hideAllMarkers();
                    }
                    el.innerHTML = 'Pause';
                    app.drawRouteAnimation();
                }
            }

            function hideAllMarkers() {
                runnersAfterMarkers.forEach(r => r.marker.setMap(null));
                runnersBeforeMarkers.forEach(r => r.marker.setMap(null));
            }

            function showAllMarkers() {
                runnersAfterMarkers.forEach(r => r.marker.setMap(map));
                runnersBeforeMarkers.forEach(r => r.marker.setMap(map));
            }

            function hideSidebar() {
                $('#wrapper').removeClass('toggled-right');
                $('.toggle-btn').css('right', '-10px');
                $('.toggle-btn').html('<i class="fa fa-chevron-left" aria-hidden="true"></i>');

                $('.sponsor-list li').each(function(idx, element) {
                    let $el = $(element);
                    $el.hide();
                });
            }

            function showSidebar() {
                $('#wrapper').addClass('toggled-right');
                $('.toggle-btn').css('right', '240px');
                $('.toggle-btn').html('<i class="fa fa-chevron-right" aria-hidden="true"></i>');
                $("#wrapper").removeClass('toggled-left');
                $('.hide-btn').hide();

                $('.sponsor-list li').each(function(idx, element) {
                    let $el = $(element);
                    setTimeout(() => {
                        $el.fadeIn('slow');
                    }, (idx + 1) * 500);
                });
            }

            function focusMarker(idx) {
                map.panTo(sponsors[idx].marker.getPosition());
                map.setZoom(16);
            }

            $(document).ready(function() {
                $('.hide-btn').click(function(event) {
                    event.preventDefault();
                    $("#wrapper").removeClass('toggled-left');
                    $(this).hide();
                    $('.toggle-btn').show();
                });

                $('.toggle-btn').click(function(event) {
                    let isOpened = $('#wrapper').hasClass('toggled-right');

                    if(isOpened) {
                        hideSidebar();
                    } else {
                        showSidebar();
                    }
                });

                showSidebar();
            });
        </script>
    </body>
</html>
