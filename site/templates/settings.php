<?php

$states = array(
  array('id' => 1,'name' => 'Aguascalientes','acronym' => 'AGS','country_id' => '0'),
  array('id' => 2,'name' => 'Baja California','acronym' => 'BCN','country_id' => '0'),
  array('id' => 3,'name' => 'Baja California Sur','acronym' => 'BCS','country_id' => '0'),
  array('id' => 4,'name' => 'Campeche','acronym' => 'CAM','country_id' => '0'),
  array('id' => 5,'name' => 'Chiapas','acronym' => 'CHP','country_id' => '0'),
  array('id' => 6,'name' => 'Chihuahua','acronym' => 'CHI','country_id' => '0'),
  array('id' => 7,'name' => 'Ciudad de México','acronym' => 'CDMX','country_id' => '0'),
  array('id' => 8,'name' => 'Coahuila','acronym' => 'COH','country_id' => '0'),
  array('id' => 9,'name' => 'Colima','acronym' => 'COL','country_id' => '0'),
  array('id' => 10,'name' => 'Durango','acronym' => 'DUR','country_id' => '0'),
  array('id' => 11,'name' => 'Guanajuato','acronym' => 'GUA','country_id' => '0'),
  array('id' => 12,'name' => 'Guerrero','acronym' => 'GUE','country_id' => '0'),
  array('id' => 13,'name' => 'Hidalgo','acronym' => 'HID','country_id' => '0'),
  array('id' => 14,'name' => 'Jalisco','acronym' => 'JAL','country_id' => '0'),
  array('id' => 15,'name' => 'Estado de México','acronym' => 'MEX','country_id' => '0'),
  array('id' => 16,'name' => 'Michoacán','acronym' => 'MIC','country_id' => '0'),
  array('id' => 17,'name' => 'Morelos','acronym' => 'MOR','country_id' => '0'),
  array('id' => 18,'name' => 'Nayarit','acronym' => 'NAY','country_id' => '0'),
  array('id' => 19,'name' => 'Nuevo León','acronym' => 'NVL','country_id' => '0'),
  array('id' => 20,'name' => 'Oaxaca','acronym' => 'OAX','country_id' => '0'),
  array('id' => 21,'name' => 'Puebla','acronym' => 'PUE','country_id' => '0'),
  array('id' => 22,'name' => 'Querétaro','acronym' => 'QRO','country_id' => '0'),
  array('id' => 23,'name' => 'Quintana Roo','acronym' => 'QR','country_id' => '0'),
  array('id' => 24,'name' => 'San Luis Potosí','acronym' => 'SLP','country_id' => '0'),
  array('id' => 25,'name' => 'Sinaloa','acronym' => 'SIN','country_id' => '0'),
  array('id' => 26,'name' => 'Sonora','acronym' => 'SON','country_id' => '0'),
  array('id' => 27,'name' => 'Tabasco','acronym' => 'TAB','country_id' => '0'),
  array('id' => 28,'name' => 'Tamaulipas','acronym' => 'TAM','country_id' => '0'),
  array('id' => 29,'name' => 'Tlaxcala','acronym' => 'TLA','country_id' => '0'),
  array('id' => 30,'name' => 'Veracruz','acronym' => 'VER','country_id' => '0'),
  array('id' => 31,'name' => 'Yucatán','acronym' => 'YUC','country_id' => '0'),
  array('id' => 32,'name' => 'Zacatecas','acronym' => 'ZAC','country_id' => '0')
);

// usort($states, function($a, $b) {
//     return strnatcmp($a['name'], $b['name']);
// });
//
// usort($cities, function($a, $b) {
//     return strnatcmp($a['name'], $b['name']);
// });

$cities = array_map('str_getcsv', file('./cities2020.csv'));
array_walk($cities, function(&$a) use ($cities) {
    $a = array_combine($cities[0], $a);
});
array_shift($cities);

// usort($states, function($a, $b) {
//     return strnatcmp($a['name'], $b['name']);
// });
usort($cities, function($a, $b) {
    return strnatcmp($a['name'], $b['name']);
});

?>
