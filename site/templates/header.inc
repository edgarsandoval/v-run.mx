<?php

// If is logged as a super-user then logout
// if($user->isLoggedin() && $user->hasRole('superuser')) {
//     $session->logout();
//     $session->redirect($page->path);
// }

$isConektaCheckout = $page->name == 'conekta-checkout';
$websitePage = $pages->get('template=website_template');

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title><?php echo $page->title; ?> | V Run</title>
    <!-- Mobile Specific Meta Tag -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="<?php echo $config->urls->assets?>images/favicon.ico?v=3">
    <!-- Bootsrap CSS -->
    <link rel="stylesheet" media="screen" href="<?php echo $config->urls->assets?>css/bootstrap.min.css">
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" media="screen" href="<?php echo $config->urls->assets?>css/font-awesome.min.css">
    <!-- Feather Icons CSS -->
    <link rel="stylesheet" media="screen" href="<?php echo $config->urls->assets?>css/feather-icons.css">
    <!-- Pixeden Icons CSS -->
    <link rel="stylesheet" media="screen" href="<?php echo $config->urls->assets?>css/pixeden.css">
    <!-- Social Icons CSS -->
    <link rel="stylesheet" media="screen" href="<?php echo $config->urls->assets?>css/socicon.css">
    <!-- PhotoSwipe CSS -->
    <link rel="stylesheet" media="screen" href="<?php echo $config->urls->assets?>css/photoswipe.css">
    <!-- Izitoast Notification CSS -->
    <link rel="stylesheet" media="screen" href="<?php echo $config->urls->assets?>css/izitoast.css">
    <!-- Main Style CSS -->
    <!-- <link rel="stylesheet" media="screen" href="<?php echo $config->urls->assets?>css/style.css"> -->
    <!-- Patch Custom Style CSS -->
    <link rel="stylesheet" media="screen" href="<?php echo $config->urls->assets?>css/main.css?v=<?= time(); ?>">

    <!-- CDN Custom libraries -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>
<body>
    <?php if (!$isConektaCheckout): ?>
        <!-- Start Mobile Menu -->
        <div class="offcanvas-container" id="mobile-menu">
            <?php if ($user->isLoggedin()): ?>
                <a class="account-link" href="/perfil/">
                    <div class="user-ava">
                        <?php if ($user->profile_picture->count): ?>
                            <img src="<?php echo $user->profile_picture->eq(0)->url; ?>"/>
                        <?php else: ?>
                            <img src="<?php echo $config->urls->assets?>images/no-user.jpg"/>
                        <?php endif; ?>
                    </div>
                    <div class="user-info">
                        <h6 class="user-name"><?= trim(sprintf('%s %s', $user->first_name, $user->last_name)); ?></h6>
                        <span class="text-sm text-white opacity-60"><?= $user->distance_traveled ?> km recorridos</span>
                    </div>
                </a>
            <?php endif; ?>
            <nav class="offcanvas-menu">
                <ul class="menu">
                    <?php if (!$user->isLoggedin()): ?>
                        <li class="<?php echo $page->name == 'login' ? 'active' : '';?> alt">
                            <span>
                                <a href="/login/?v=LGN"><span>Iniciar Sesión</span></a>
                            </span>
                        </li>
                        <li class="<?php echo $page->name == 'login' ? 'active' : '';?> alt">
                            <span>
                                <a href="/login/?v=RGSTR"><span>Registrarse</span></a>
                            </span>
                        </li>
                    <?php else: ?>
                        <li class="<?php echo $page->name == 'perfil' ? 'active' : '';?> alt">
                            <span>
                                <a href="/perfil/#eventos" class="btn-menu"><span>Mis eventos</span></a>
                            </span>
                        </li>
                    <?php endif; ?>
                    <li class="<?php echo $page->name == 'home' ? 'active' : '';?>">
                        <span>
                            <a href="/"><span>Home</span></a>
                        </span>
                    </li>
                    <li class="<?php echo $page->name == 'eventos' ? 'active' : '';?>">
                        <span>
                            <a href="/eventos"><span>Eventos</span></a>
                        </span>
                    </li>
                    <?php if ($websitePage->event_visible): ?>
                        <li class="<?php echo $page->name == $websitePage->name ? 'active' : '';?>">
                            <span>
                                <a href="<?= $websitePage->httpUrl ?>"><span><?= $websitePage->title ?></span></a>
                            </span>
                        </li>
                    <?php endif; ?>
                    <li class="<?php echo $page->name == 'como-funciona' ? 'active' : '';?>">
                        <span>
                            <a href="/como-funciona"><span>Cómo Funciona</span></a>
                        </span>
                    </li>
                    <!-- <li class="<?php echo $page->name == 'resultados' ? 'active' : '';?>">
                        <span>
                            <a href="/"><span>Resultados</span></a>
                        </span>
                    </li> -->
                    <!-- <li class="<?php echo $page->name == 'quienes-somos' ? 'active' : '';?>">
                        <span>
                            <a href="/quienes-somos"><span>Quiénes Somos</span></a>
                        </span>
                    </li> -->
                    <li class="<?php echo $page->name == 'labor-comunitaria' ? 'active' : '';?>">
                        <span>
                            <a href="/labor-comunitaria"><span>Apoyos AC</span></a>
                        </span>
                    </li>
                    <li class="<?php echo $page->name == 'contacto' ? 'active' : '';?>">
                        <span>
                            <a href="/contacto"><span>Contacto</span></a>
                        </span>
                    </li>
                    <?php if ($user->isLoggedin()): ?>
                        <li class="alt">
                            <span>
                                <a href="/logout"><span>Cerrar sesión</span></a>
                            </span>
                        </li>
                    <?php endif; ?>
                </ul>
            </nav>
        </div>
        <!-- End Mobile Menu -->
    <?php endif; ?>

    <!-- Start NavBar -->
    <header class="navbar navbar-sticky">
        <!-- Start Search -->
        <form class="site-search" method="POST">
            <input type="text" name="site_search" placeholder="Type to search...">
            <div class="search-tools">
                <span class="clear-search">Clear</span>
                <span class="close-search"><i class="icon-cross"></i></span>
            </div>
        </form>
        <!-- End Search -->
        <!-- Start Logo -->
        <div class="site-branding">
            <div class="inner">
                <a class="offcanvas-toggle cats-toggle" href="#shop-categories" data-toggle="offcanvas" style="visibility: hidden;"></a>
                <?php if (!$isConektaCheckout): ?>
                    <a class="offcanvas-toggle menu-toggle" href="#mobile-menu" data-toggle="offcanvas"></a>
                <?php endif; ?>
                <a class="site-logo" href="/"><img src="<?php echo $config->urls->assets?>images/home1-01.jpg" alt="V Run"></a>
            </div>
        </div>
        <!-- End Logo -->
        <!-- Start Nav Menu -->
        <nav class="site-menu">
            <?php if (!$isConektaCheckout): ?>
                <ul>
                    <li class="<?php echo $page->name == 'home' ? 'active' : '';?>">
                        <a href="/"><span>Home</span></a>
                    </li>
                    <li class="<?php echo $page->name == 'eventos' ? 'active' : '';?>">
                        <a href="/eventos"><span>Eventos</span></a>
                    </li>
                    <?php if ($websitePage->event_visible): ?>
                        <li class="<?php echo $page->name == $websitePage->name ? 'active' : '';?>">
                            <a href="<?= $websitePage->httpUrl ?>"><span><?= $websitePage->title; ?></span></a>
                        </li>
                    <?php endif; ?>
                    <li class="<?php echo $page->name == 'como-funciona' ? 'active' : '';?>">
                        <a href="/como-funciona"><span>Cómo Funciona</span></a>
                    </li>
                    <!-- <li class="<?php echo $page->name == 'resultados' ? 'active' : '';?>">
                        <a href="/resultados"><span>Resultados</span></a>
                    </li> -->
                    <!-- <li class="<?php echo $page->name == 'quienes-somos' ? 'active' : '';?>">
                        <a href="/quienes-somos"><span>Quiénes Somos</span></a>
                    </li> -->
                    <li class="<?php echo $page->name == 'labor-comunitaria' ? 'active' : '';?>">
                        <a href="/labor-comunitaria"><span>Apoyos AC</span></a>
                    </li>
                    <li class="<?php echo $page->name == 'contacto' ? 'active' : '';?>">
                        <a href="/contacto"><span>Contacto</span></a>
                    </li>
                </ul>
            <?php endif;  ?>
        </nav>
        <?php if (!$isConektaCheckout): ?>
            <!-- End Nav Menu -->
            <?php if ($user->isLoggedin()): ?>
            <!-- Start Toolbar -->
            <div class="toolbar">
                <div class="inner">
                    <div class="tools">
                        <div class="search" style="visibility: hidden;"><i class="icon-search"></i></div>
                        <!-- Start Account -->
                        <div class="account">
                            <a href="<?php echo (!$user->isLoggedin() ? '/login' : '#') ?>"></a><i class="icon-head"></i>
                            <ul class="toolbar-dropdown">
                                <?php if ($user->isLoggedin()): ?>
                                    <li class="sub-menu-user">
                                        <div class="user-ava">
                                            <?php if ($user->profile_picture->count): ?>
                                                <img src="<?php echo $user->profile_picture->eq(0)->url; ?>"/>
                                            <?php else: ?>
                                                <img src="<?php echo $config->urls->assets?>images/no-user.jpg"/>
                                            <?php endif; ?>
                                        </div>
                                        <div class="user-info">
                                            <h6 class="user-name"><?= trim(sprintf('%s %s', $user->first_name, $user->last_name)); ?></h6>
                                            <span class="text-xs text-muted"><?= $user->distance_traveled ?> km recorridos</span>
                                        </div>
                                    </li>
                                    <li><a href="/perfil/#perfil-informacion" class="btn-menu">Mi Perfil</a></li>
                                    <li><a href="/perfil/#direccion" class="btn-menu">Mi Dirección</a></li>
                                    <li><a href="/perfil/#eventos" class="btn-menu">Mis Eventos</a></li>
                                    <li class="sub-menu-separator"></li>
                                    <li><a href="/logout"><i class="fa fa-lock"></i> Cerrar Sesión</a></li>
                                <?php else: ?>
                                    <li><a href="/login"><i class="fa fa-sign-in"></i> Iniciar sesión</a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <!-- End Account -->
                        <!-- Start Cart -->
                        <div class="cart">
                            <?php if ($user->isLoggedin()): ?>
                                <span class="count" style="margin-top: 9.5px;"><?= $user->distance_traveled ?> kms</span>
                            <?php else: ?>
                                <span class="count" style="margin-top: 9.5px;">0 kms</span>
                            <?php endif; ?>
                        </div>
                        <!-- End Cart -->
                    </div>
                </div>
            </div>
            <!-- End Toolbar -->
            <?php else: ?>
                <!-- Start Toolbar -->
                <div class="toolbar d-none d-md-block">
                    <div class="inner">
                        <div class="tools">
                            <a class="btn btn-default" style="color: #fff; background-color: #a800bb;" href="/login/?v=LGN">Inicia sesión </a>
                            <a class="btn btn-primary" style="color: #fff;" href="/login/?v=RGSTR">Registrate </a>
                        </div>
                    </div>
                </div>
                <!-- End Toolbar -->
            <?php endif; ?>
        <?php endif; ?>
    </header>
    <!-- End NavBar -->
    <div class="offcanvas-wrapper">
    <!-- Start Page Content -->
