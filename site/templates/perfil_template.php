<?php require_once './header.inc'; ?>
<?php if(!$user->isLoggedIn()) $session->redirect('/login'); ?>
<?php

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

if($user->address->count != 0)
    $address = $user->address->eq(0);
else
    $address = [];

$response = null;

$userOrganizeEvents = false;

$events = $pages->find("template=evento_template, event_visible=1", 'findAll=true');
foreach($events as $event) {
    if($userOrganizeEvents) break;
    // $userOrganizeEvents = $event->event_organizator == $user->email || $user->email == 'edgar@ec-ideas.com';
    $userOrganizeEvents = $user->email == 'marco.palencia@gmail.com' || $user->email == 'edgar@ec-ideas.com';
}

$hasDiploma = false;

if($input->get('dispatch') !== null) {
    $dispatch = $input->get('dispatch');
    switch ($dispatch) {
        case 'SUCCESS_UPLOAD':
            $response = ['status' => true, 'message' => 'Progreso registrado correctamente'];
            break;
        case 'PROFILE_UPLOAD':
            $response = ['status' => true, 'message' => 'Imagen de perfil cargada correctamente'];
            break;
        case 'SUCCESS_UPDATE':
            $response = ['status' => true, 'message' => 'Perfil modificado correctamente'];
            break;
        case 'ADDRESS_UPDATE':
            $response = ['status' => true, 'message' => 'Dirección actualizada correctamente'];
            break;
        case 'SUCCESS_FINISH':
            $response = ['status' => true, 'message' => 'Tu distancia y tiempo han sido actualizados'];
            if($input->get->hasDiploma == 'true')
                $hasDiploma = true;
            break;
        case 'FINISH_SET':
            $response = ['status' => true, 'title' => '¡¡¡ Lotería !!!', 'message' => '
                <p>¡Felicidades!  Has completado todas las casillas de tu tabla de lotería.
Ahora sigue que realices una última carrera de 5 km.</p>
                <p>Deberás subir tus registros (tiempo y distancia) y la foto de tu app (como de costumbre) y con ello aparecerás en la tabla de resultados.</p>
                <p>Aprovecha el entrenamiento que realizaste al completar las casillas, y con ello intenta mejorar tu rendimiento</p>
            '];
            break;
        case 'UPLOAD_ERROR':
            $response = ['status' => false, 'message' => 'Ocurrió un error al subir tu evidencia, intenta con otra fotografía o más tarde'];
            break;
        case 'NO_TIME':
            $response = ['status' => false, 'message' => 'No es posible subir la evidencia, ya que este evento finalizó o aún no comienza'];
            break;
    }
}


function changeUserData($data) {
    $u = users()->get('email=' . $data['email']);
    $u->first_name  = $data['first_name'];
    $u->last_name   = $data['last_name'];
    $u->email       = $data['email'];
    $u->phone       = $data['phone'];
    $u->gender       = $data['gender'];
    $u->birthdate       = $data['birthdate'];

    if($data['pass'] != '')
        $u->pass        = $data['pass'];

    $u->of(false);
    $u->save();

    return true;
}

function changeAddress($data, $user) {
    foreach($user->address as $a){
    	$a->delete();
    }

    $address = $user->address->getNew();

    $address->address_state = $data['address_state'];
    $address->address_city = $data['address_city'];
    $address->address_street = $data['address_street'];
    $address->address_suburb = $data['address_suburb'];
    $address->address_outdoor_number = $data['address_outdoor_number'];
    $address->address_indoor_number = $data['address_indoor_number'];
    $address->address_zip_code = $data['address_zip_code'];

    $address->of(false);
    $address->save();

    $user->address->add($address);
    $user->of(false);
    $user->save();

    return true;
}

$errors = [];

if($input->post->submit !== null) {
    $form = $input->post->form;
    switch($form) {
        case 'profile':
            // Password filter
            if($input->post->pass != '' && $input->post->pass !== $input->post->get('pass-confirm'))
                $errors[] = 'Las contraseñas no coinciden';

            // Phone number filter
            if(!preg_match('/^[0-9]{10}+$/', $input->post->phone))
                $errors[] = 'El número de telefónico debe tener 10 dígitos';

            $data = array(
    			'first_name' => wire('sanitizer')->text(wire('input')->post('first_name')),
    			'last_name' => wire('sanitizer')->text(wire('input')->post('last_name')),
    			'email' => wire('sanitizer')->email(wire('input')->post('email')),
    			'phone' => wire('sanitizer')->text(wire('input')->post('phone')),
    			'gender' => wire('sanitizer')->text(wire('input')->post('gender')),
    			'birthdate' => wire('sanitizer')->text(wire('input')->post('birthdate')),
    			'pass' => wire('sanitizer')->text(wire('input')->post('pass'))
    		);

            if(count($errors) == 0 && changeUserData($data))
                $session->redirect($pages->get('/perfil')->url . '?dispatch=SUCCESS_UPDATE');
            break;
        case 'address':
            $data = array(
                'address_state' => wire('sanitizer')->text(wire('input')->post('address_state')),
                'address_city' => wire('sanitizer')->text(wire('input')->post('address_city')),
                'address_street' => wire('sanitizer')->text(wire('input')->post('address_street')),
                'address_suburb' => wire('sanitizer')->text(wire('input')->post('address_suburb')),
                'address_outdoor_number' => wire('sanitizer')->text(wire('input')->post('address_outdoor_number')),
                'address_indoor_number' => wire('sanitizer')->text(wire('input')->post('address_indoor_number')),
                'address_zip_code' => wire('sanitizer')->text(wire('input')->post('address_zip_code'))
            );

            if(count($errors) == 0 && changeAddress($data, $user))
                $session->redirect($pages->get('/perfil')->url . '?dispatch=ADDRESS_UPDATE');
            break;
        default:
            var_dump($input->post);die;
    }
}


?>

<div class="container padding-top-2x padding-bottom-3x">
        <div class="row">
            <div class="col-lg-4">
                <aside class="user-info-wrapper">
                    <div class="user-cover account-details">
                        <div class="info-label" data-toggle="tooltip" title="Hasta el momento llevas <?= $user->distance_traveled ?> km recorridos"><i class="icon-medal"></i><?= $user->distance_traveled ?> km</div>
                    </div>
                    <div class="user-info">
                        <div class="user-avatar">
                            <a class="edit-avatar" href="#"></a>
                            <form id="profile-form" action="/user-event-linker/" method="POST" enctype="multipart/form-data" style="display: none;">
                                <input type="hidden" name="form" value="profile_picture">
                                <input type="file" name="image"/>
                                <button type="submit" name="submit"></button>
                            </form>
                            <?php if (isset($user->profile_picture) && $user->profile_picture->count): ?>
                                <img src="<?php echo $user->profile_picture->eq(0)->url ?>"/>
                            <?php else: ?>
                                <img src="<?php echo $config->urls->assets?>images/no-user.jpg"/>
                            <?php endif; ?>
                        </div>
                        <div class="user-data">
                            <h4><?= trim(sprintf('%s %s', $user->first_name, $user->last_name)); ?></h4><span>Miembro desde <?= date('d/m/Y', $user->created_at)?></span>
                        </div>
                    </div>
                </aside>
                <nav class="list-group">
                    <a class="list-group-item active" data-toggle="tab" href="#perfil-informacion"><i class="icon-head"></i>Mi Perfil</a>
                    <a class="list-group-item" data-toggle="tab" href="#direccion"><i class="icon-map"></i>Mi Dirección</a>
                    <a class="list-group-item" data-toggle="tab" href="#eventos"><i class="icon-tag"></i>Mis eventos</a>
                    <?php if ($userOrganizeEvents): ?>
                        <a class="list-group-item" href="/master-sheet" target="_blank"><i class="icon-clipboard"></i>Master sheet</a>
                    <?php endif; ?>
                </nav>
            </div>
            <div class="col-lg-8">
                <div class="padding-top-2x mt-2 hidden-lg-up"></div>
                <div class="tab-content profile-content">
                    <div id="perfil-informacion" class="tab-pane fade active show">
                        <h3>Mi Perfil</h3>
                        <hr class="padding-bottom-1x">
                        <form class="row" method="POST">
                            <input type="hidden" name="form" value="profile">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="account-fn">Nombre</label>
                                    <input class="form-control" type="text" name="first_name" id="account-fn" value="<?= $user->first_name; ?>" required />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="account-ln">Apellidos</label>
                                    <input class="form-control" type="text" name="last_name" id="account-ln" value="<?= $user->last_name; ?>" required />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="account-email">Correo electrónico</label>
                                    <input class="form-control" type="email" name="email" id="account-email" value="<?= $user->email; ?>" readonly />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="account-phone">Número telefónico</label>
                                    <input class="form-control" type="text" name="phone" id="account-phone" value="<?= $user->phone; ?>" required />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="account-gender">Género</label>
                                    <select class="form-control" name="gender">
                                        <?php if (is_null($user->gender->title)): ?>
                                            <option selected>Masculino</option>
                                            <option>Femenino</option>
                                        <?php else: ?>
                                            <option <?php echo $user->gender->title == 'Masculino' ? 'selected' : '' ?>>Masculino</option>
                                            <option <?php echo $user->gender->title == 'Femenino' ? 'selected' : '' ?>>Femenino</option>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="account-birthdate">Fecha de nacimiento</label>
                                    <?php if ($user->birthdate !== ''): ?>
                                        <input class="form-control" type="date" name="birthdate" id="account-birthdate" value="<?php echo date('Y-m-d', $user->birthdate); ?>" required />
                                    <?php else: ?>
                                        <input class="form-control" type="date" name="birthdate" id="account-birthdate" required />
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="account-pass">Nueva Contraseña</label>
                                    <input class="form-control" type="password" name="pass" id="account-pass" value="" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="account-pass-confirm">Confirmar Contraseña</label>
                                    <input class="form-control" type="password" name="pass-confirm" id="account-confirm-pass" value="" />
                                </div>
                            </div>
                            <div class="col-12">
                                <hr class="mt-2 mb-3" />
                                <div class="d-flex flex-wrap justify-content-between align-items-center">
                                    <button class="btn btn-primary margin-right-none" name="submit" type="submit">
                                        Guardar cambios
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                    <div id="direccion" class="tab-pane fade">
                        <h3>Mi dirección</h3>
                        <hr class="padding-bottom-1x"/>
                        <form class="row" method="POST">
                            <input type="hidden" name="form" value="address">

                            <div class="col-md-6" style="display: none;">
                                <div class="form-group">
                                    <a href="#" class="btn btn-primary btn-block btn-search-zip">Buscar</a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Calle *</label>
                                    <input class="form-control" type="text" name="address_street" value="<?= @$address->address_street; ?>" required pattern="[a-zA-Z ]+">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Colonia *</label>
                                    <input class="form-control" type="text" name="address_suburb" value="<?= @$address->address_suburb; ?>" required pattern="[a-zA-Z ]+">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Número exterior *</label>
                                    <input class="form-control" type="number" name="address_outdoor_number" value="<?= @$address->address_outdoor_number; ?>" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Número interior</label>
                                    <input class="form-control" type="text" name="address_indoor_number" value="<?= @$address->address_indoor_number; ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Estado *</label>
                                    <input class="form-control" type="text" id="state" name="address_state" value="<?= @$address->address_state; ?>" required pattern="[a-zA-Z ]+">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Ciudad *</label>
                                    <input class="form-control" type="text" id="city" name="address_city" value="<?= @$address->address_city; ?>" required pattern="[a-zA-Z ]+">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Código postal *</label>
                                    <input class="form-control" id="zip_code" type="text" name="address_zip_code" value="<?= @$address->address_zip_code; ?>" pattern="[0-9]{5}">
                                </div>
                            </div>
                            <div class="col-12">
                                <hr class="mt-2 mb-3" />
                                <div class="d-flex flex-wrap justify-content-between align-items-center">
                                    <button class="btn btn-primary margin-right-none" name="submit" type="submit">
                                        Guardar cambios
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div id="eventos" class="tab-pane fade">
                        <h3>Mis Eventos</h3>
                        <!-- Wishlist Table-->
                        <div class="table-responsive wishlist-table margin-bottom-none">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Evento</th>
                                        <th>Número</th>
                                        <th>Registro</th>
                                        <th>Evidencias</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php if ($user->user_events->count > 0): ?>
                                    <?php foreach ($user->user_events as $userEvent): ?>
                                        <?php if(!$userEvent->event) { continue; } ?>
                                        <tr>
                                            <td>
                                                <div class="product-item">
                                                    <a class="product-thumb" href="/resultados/?event=<?php echo $userEvent->event->name; ?>" title="Ver Resultados" target="_blank"><img src="<?php echo $userEvent->event->event_images->eq(0)->url; ?>"></a>
                                                </div>
                                            </td>
                                            <td style="font-size: 20px; font-weight: bold; text-align: center;"><a href="/corredor/?runner=<?= $userEvent->id; ?>&token=<?= md5($userEvent->id) ?>" target="_blank" style="color: #606976; text-decoration: none !important; "><?= $userEvent->event_runner_number; ?></a>
                                                 <?php if ($userEvent->event->event_diploma != null && $userEvent->event_status->title == 'Terminado'): ?>
                                                     <br/>
                                                     <a href="/diploma/?runner=<?= $userEvent->id; ?>&salt=<?php echo generateRandomString(18) ?>" target="_blank">Diploma</a>
                                                 <?php endif; ?>
                                                 <?php if ($userEvent->event->event_type->title == 'Recorrido'): ?>
                                                     <br/>
                                                     <!-- <i class="fas fa-map-marked-alt"></i> -->
                                                     <a href="#" class="open-map" data-id="<?= $userEvent->id ?>" data-salt="<?= generateRandomString(18) ?>">Mapa </a>
                                                 <?php endif; ?>
                                            </td>
                                            <td>
                                                <a href="/corredor/?runner=<?= $userEvent->id; ?>&token=<?= md5($userEvent->id) ?>" target="_blank" style="color: #606976; text-decoration: none !important; font-size: 20px; font-weight: bold; font-size: calc(45px / 2);" class="hidden-md-up"><?= $userEvent->event_runner_number; ?> </a>
                                                <?php if ($userEvent->event->event_diploma != null && $userEvent->event_status->title == 'Terminado'): ?>
                                                    &nbsp; &nbsp;<a href="/diploma/?runner=<?= $userEvent->id; ?>&salt=<?php echo generateRandomString(18) ?>" target="_blank"  style="display: block; text-decoration: none !important; font-size: 20px; font-weight: bold; font-size: calc(45px / 2);" class="hidden-md-up">Diploma</a>
                                                <?php endif; ?>
                                                <?php if ($userEvent->event->event_type->title == 'Recorrido'): ?>
                                                    <!-- <i class="fas fa-map-marked-alt"></i> -->
                                                    &nbsp; &nbsp;<a href="#" class="open-map hidden-md-up" data-id="<?= $userEvent->id ?>" data-salt="<?= generateRandomString(18) ?>"  style="text-decoration: none !important; font-size: 20px; font-weight: bold; font-size: calc(45px / 2);">Mapa </a>
                                                <?php endif; ?>
                                                <div class="product-item">
                                                    <div class="product-info">
                                                        <h4 class="product-title"><a href="/resultados/?event=<?php echo $userEvent->event->name; ?>" target="_blank" title="Ver Resultados"><?php echo  $userEvent->event->title; ?></a>
                                                            <?php if ($userEvent->event->event_classification->title == 'Privado'): ?>
                                                                <img class="private-icon" src="<?= $config->urls->assets?>images/team.png" />
                                                            <?php endif; ?>
                                                            <?php if ($userEvent->event->event_type->title == 'Entrenamiento'): ?>
                                                                <i class="fa fa-book" aria-hidden="true" style="color: #00e3ff; font-size: 22px;"></i>
                                                            <?php endif; ?>
                                                        </h4>
                                                        <div class="text-lg text-medium text-muted"><?php
                                                            if($userEvent->event->event_type->title == 'Distancia')
                                                                echo number_format($userEvent->event_total_duration, 1) . ' MINS/' . number_format($userEvent->event_total_distance, 1) . ' KMS';
                                                            else
                                                                echo number_format($userEvent->event_total_distance, 1) . ' KMS/' . number_format($userEvent->event_total_duration, 1) .' MINS';
                                                        ?>
                                                        </div>
                                                        <div>Estatus:
                                                            <?php
                                                                switch ($userEvent->event_status->title) {
                                                                    case 'Pendiente':
                                                                        echo '<div class="d-inline text-warning">';
                                                                        break;
                                                                    case 'Terminado':
                                                                        echo '<div class="d-inline text-success">';
                                                                        break;
                                                                    case 'Cancelado':
                                                                        echo '<div class="d-inline text-danger">';
                                                                        break;
                                                                }
                                                            ?>
                                                                <?php echo  $userEvent->event_status->title; ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="action-details-mobile" style="display: none;">
                                                    <div class="action-content">
                                                    <?php if($userEvent->event_status->title != 'Cancelado'): ?>
                                                        <a class="show-evidences" href="#"  data-finished="<?= count($userEvent->event_subevidences) == count($userEvent->event->event_subevent) ? 'true' : 'false'; ?>" data-id="<?= $userEvent->id; ?>" data-event="<?= $userEvent->event->title; ?>" data-training="<?= $userEvent->event->event_type->title == 'Entrenamiento' ? '1' : '0'; ?>" ><i class="icon-eye"></i></a>
                                                        <a class="add-evidence" href="#" data-video-url="<?= isset($userEvent->event->event_explain_video) ? $userEvent->event->event_explain_video->url : '0';  ?>" data-id="<?= $userEvent->id; ?>" data-type="<?= $userEvent->event->event_type->id ?>" data-training="<?= $userEvent->event->event_type->title == 'Entrenamiento' ? '1' : '0'; ?>" title="Agregar evidencia"><i class="icon-plus"></i></a>
                                                        <a class="finish_event" href="#" title="Terminar desafío" style="<?php echo $userEvent->evidence->count == 0 || $userEvent->event_status->title == 'Terminado' ? 'visibility: hidden;' : ''; ?>"><i class="icon-check"></i></a>
                                                        <form action="/user-event-linker/" method="POST" style="display: none;">
                                                            <input type="hidden" name="form" value="finish_event"/>
                                                            <input type="text" name="id" value="<?= $userEvent->id; ?>"/>
                                                            <button type="submit" name="submit"></button>
                                                        </form>
                                                        <a class="show-guide" href="#" data-id="<?= $userEvent->id; ?>" data-tracking="<?= $userEvent->event_tracking_guide; ?>" data-date="<?= date('Y-m-d h:i:s a', $userEvent->event_submit_timestamp); ?>" title="Mostrar guía de rastreo" style="<?php echo $userEvent->event_submit_timestamp == '' ? 'visibility: hidden;' : '';  ?>"><i class="fa fa-envelope-o"></i></a>
                                                    <?php endif; ?>
                                                    </div>
                                                    <div class="evidence-table" style="display: none;">
                                                        <?php if ($userEvent->evidence->count > 0): ?>
                                                            <table>
                                                                <?php foreach ($userEvent->evidence as $evidence): ?>
                                                                    <tr>
                                                                        <td><?= date('d/m/Y h:i:s a', $evidence->created_at ?: 0)?></td>
                                                                        <?php if($userEvent->event->event_type->title == 'Distancia'): ?>
                                                                            <td><?php echo number_format($evidence->evidence_duration, 1) ?> mns / <?php echo number_format($evidence->evidence_distance, 1) ?> kms</td>
                                                                        <?php else: ?>
                                                                            <td><?php echo number_format($evidence->evidence_distance, 1) ?> kms / <?php echo number_format($evidence->evidence_duration, 1) ?> mns</td>
                                                                        <?php endif; ?>
                                                                        <td>
                                                                            <a href="<?php echo $evidence->evidence_images->url; ?>" target="_blank"><i class="icon-camera"></i></a>
                                                                        </td>
                                                                    </tr>
                                                                <?php endforeach; ?>
                                                            </table>
                                                        <?php else: ?>
                                                            <table>
                                                                <tr>
                                                                    <td colspan="3" style="text-align: center;">No se ha cargado evidencia</td>
                                                                </tr>
                                                            </table>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div class="action-content">
                                                <?php if($userEvent->event_status->title != 'Cancelado'): ?>
                                                    <a class="show-evidences" href="#"  data-finished="<?= count($userEvent->event_subevidences) == count($userEvent->event->event_subevent) ? 'true' : 'false'; ?>" data-id="<?= $userEvent->id; ?>" data-event="<?= $userEvent->event->title; ?>" data-training="<?= $userEvent->event->event_type->title == 'Entrenamiento' ? '1' : '0'; ?>" ><i class="icon-eye"></i></a>
                                                    <a class="add-evidence" href="#" data-video-url="<?= isset($userEvent->event->event_explain_video) ? $userEvent->event->event_explain_video->url : '0';  ?>" data-id="<?= $userEvent->id; ?>" data-type="<?= $userEvent->event->event_type->id ?>" data-training="<?= $userEvent->event->event_type->title == 'Entrenamiento' ? '1' : '0'; ?>" title="Agregar evidencia"><i class="icon-plus"></i></a>
                                                    <?php if ($userEvent->evidence->count > 0 && $userEvent->event_status->title != 'Terminado'): ?>
                                                        <a class="finish_event" href="#" title="Terminar desafío"><i class="icon-check"></i></a>
                                                        <form action="/user-event-linker/" method="POST" style="display: none;">
                                                            <input type="hidden" name="form" value="finish_event"/>
                                                            <input type="text" name="id" value="<?= $userEvent->id; ?>"/>
                                                            <button type="submit" name="submit"></button>
                                                        </form>
                                                    <?php endif; ?>
                                                    <a class="show-guide" href="#" data-id="<?= $userEvent->id; ?>" data-tracking="<?= $userEvent->event_tracking_guide; ?>" data-date="<?= date('Y-m-d h:i:s a', $userEvent->event_submit_timestamp); ?>" title="Mostrar guía de rastreo" style="<?php echo $userEvent->event_submit_timestamp == '' ? 'visibility: hidden;' : '';  ?>"><i class="fa fa-envelope-o"></i></a>
                                                <?php endif; ?>
                                                </div>
                                                <div class="evidence-table" style="display: none;">
                                                    <?php if ($userEvent->evidence->count > 0): ?>
                                                        <table>
                                                            <?php foreach ($userEvent->evidence as $evidence): ?>
                                                                <tr>
                                                                    <td><?= date('d/m/Y h:i:s a', $evidence->created_at ?: 0)?></td>
                                                                    <?php if($userEvent->event->event_type->title == 'Distancia'): ?>
                                                                        <td><?php echo number_format($evidence->evidence_duration, 1) ?> mns / <?php echo number_format($evidence->evidence_distance, 1) ?> kms</td>
                                                                    <?php else: ?>
                                                                        <td><?php echo number_format($evidence->evidence_distance, 1) ?> kms / <?php echo number_format($evidence->evidence_duration, 1) ?> mns</td>
                                                                    <?php endif; ?>
                                                                    <td>
                                                                        <a href="<?php echo $evidence->evidence_images->url; ?>" target="_blank"><i class="icon-camera"></i></a>
                                                                    </td>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                        </table>
                                                    <?php else: ?>
                                                        <table>
                                                            <tr>
                                                                <td colspan="3" style="text-align: center;">No se ha cargado evidencia</td>
                                                            </tr>
                                                        </table>
                                                    <?php endif; ?>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="3" style="text-align: center">AÚN NO TIENES EVENTOS REGISTRADOS DA CLICK <a href="<?php echo $pages->get('/eventos')->url ?>">AQUÍ</a> PARA REVISAR LOS EVENTOS DISPONIBLES</td>
                                    </tr>
                                <?php endif; ?>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
                <!-- <hr class="mb-4"> -->
            </div>
        </div>
    </div>


<script>

function makeid(length) {
   var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}

function onInit() {
    let id = window.location.hash;
    $('a[href="' + id + '"]').click();
    <?php if(isset($response)): ?>
        <?php if($input->get('dispatch') == 'FINISH_SET'): ?>
            confetti.start();
            Swal.fire({
                title: '<?= $response['title'] ?>',
                html: `<?= $response['message'] ?>`,
                type: 'success',
                confirmButtonText: 'Siguiente'
            }).then(result => {
                if(result.value) {
                    let id = <?= $input->get('id') ?>;
                    $('#eventos').find('.action-content').find('a.add-evidence[data-id="' + id + '"]').first().click();
                    confetti.stop();
                }
            });
        <?php else: ?>
            <?php if($response['status']): ?>
                <?php if($hasDiploma): ?>
                    Swal.fire(
                        '¡Éxito!',
                        '<?= $response['message'] ?>',
                        'success'
                    ).then(result => {
                        let videoUrl = '<?php echo $input->get->video; ?>';
                        let id = '<?php echo $input->get->diploma; ?>';

                        $('#finish-modal .modal-content').html('');
                        $('#finish-modal .modal-content').append(`
                            <div class="row">
                                <div class="col-md-12">
                                    <video width="320" height="240" controls autoplay>
                                        <source src="${ videoUrl }" type="video/mp4">
                                        Your browser does not support the video tag.
                                    </video>
                                </div>
                            </div>
                        `);

                        $('#finish-modal .modal-content').append(`
                            <div class="row">
                                <div class="col-md-12" style="text-align: center;">
                                    <a href="/diploma/?runner=${ id }&salt=${ makeid(18) }" target="_blank" class="btn btn-info" style="margin: 15px auto;"><i class="fa fa-download" aria-hidden="true"></i> Descarga tu diploma </a>
                                </div>
                            </div>
                        `);

                        $('#finish-modal').modal('show');
                        // $(".overlay-video").find('iframe').attr('src', videoUrl);
                        // $(".overlay-video").show();
                        // $('.open-video').show();
                        // setTimeout(function() {
                        //     $(".overlay-video").addClass("o1");
                        // }, 100);
                    });
                <?php else: ?>
                    Swal.fire(
                        '¡Éxito!',
                        '<?= $response['message'] ?>',
                        'success'
                    );
                <?php endif; ?>
            <?php else: ?>
                Swal.fire(
                    '¡Error!',
                    '<?= $response['message'] ?>',
                    'error'
                );
            <?php endif; ?>
        <?php endif; ?>
    <?php endif; ?>
    // if(true) {
    //     Swal.fire('Atención', 'Nuestra base de datos ha sido actualizada, por favor verifica que tu dirección esté correcta', 'info');
    // }

    $('.open-map').click(function() {
        $modal = $('#map-modal');
        $modal.find('.modal-body').html('');

        let id      = $(this).data('id');
        let salt    = $(this).data('salt');

        $modal.find('.modal-body').html(`<iframe src="https://v-run.mx/recorrido/?runner=${ id }&token=${ salt }" style="    height: calc((100vh / 3) * 2);"></iframe>`)

        $modal.modal('show');
    });

    $('#map-modal').on('hidden.bs.modal', function () {
        $modal = $('#map-modal');

        $modal.find('.modal-body').html('');
    });
}

</script>

<script>
    if(typeof window.history.pushState == 'function') {
        window.history.pushState({}, "Hide", "https://v-run.mx/perfil/");
    }
</script>

<style media="screen">
.swal2-styled.swal2-confirm {
    background-color: #3085d6 !important;
}
</style>

<?php require_once './footer.inc'; ?>
