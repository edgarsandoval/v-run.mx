<?php require_once './header.inc'; ?>

<!-- Retrieve eventData -->
<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$eventId        = $input->get('event_id');
$userId         = $input->get('user_id');
$price          = $input->get('price');
$description    = $input->get('description');
$eventCarrier   = $input->get('event_carrier');
$eventShipping1   = $input->get('event_shipping1') ?: 0;
$eventShipping2   = $input->get('event_shipping2') ?: 0;

$debug = false;

if($debug && $user->email != 'edgar@ec-ideas.com') {
    header("HTTP/1.1 401 Unauthorized");
    exit;
}

if(!isset($eventId) || !isset($userId) || !isset($price) || !isset($description) || !isset($eventCarrier)) {
    header("HTTP/1.1 401 Unauthorized");
    exit;
}

$event  = $pages->get($eventId);
$u      = $users->get($userId);


// Validate address
$errors = [];
// if($u->address->count == 0) {
//     $errors[] = 'La dirección es requerida para completar la compra, ve a tu perfil para más detalles';
// }
function _validate($data, &$errors) {
    if(count($errors) == 0)
        return true;

    if($data->address_state == '')
        $errors[] = 'El estado es requerido';

    if($data->address_city == '')
        $errors[] = 'La ciudad es requerida';

    if($data->address_street == '')
        $errors[] = 'La calle es requerida';

    if($data->address_suburb == '')
        $errors[] = 'La colonía es requerida';

    if(!is_numeric($data->address_outdoor_number))
        $errors[] = 'El número exterior tiene un formato incorrecto';

    // if($data->address_intdoor_number != '' && !is_numeric($data->address_intdoor_number))
    //     $errors[] = 'El número interior tiene un formato incorrecto';

    if(!preg_match('/^[0-9]{5}$/', $data->address_zip_code))
        $errors[] = 'El código postal tiene un formato incorrecto';

    return count($errors) == 0;
}

?>
<style media="screen">
.jp-card-container {
    width: 350px !important;
}
</style>
<div class="container padding-top-1x padding-bottom-3x">
    <div class="row">
        <!-- Start Checkout Payment -->
        <div class="col-lg-12">
            <?php if (!_validate(@$u->address[0], $errors)): ?>
                <h1 style="text-align: center;">Hay algunos errores que no permiten seguir con la compra, favor de corregirlos.</h1>
                <div class="alert alert-danger alert-dismissible fade show text-center margin-bottom-1x">
                    <?php foreach ($errors as $error): ?>
                        <p><i class="fa fa-bell"></i> <?= $error ?> </p>
                    <?php endforeach; ?>
                </div>
            <?php else: ?>
                <h4>Revisa tu pedido</h4>
                <hr class="padding-bottom-1x">
                <div class="table-responsive shopping-cart">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Nombre del producto</th>
                                <th class="text-center">Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="product-item">
                                        <a class="product-thumb" href="#" style="pointer-events: none;"><img src="<?= $event->event_images->eq(0)->httpUrl; ?>" alt=""></a>
                                        <div class="product-info">
                                            <h4 class="product-title">
                                                <a href="#"><?= $event->title; ?><small>x 1</small></a>
                                            </h4>
                                            <span><?= $description ?></span>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center text-lg text-medium">$<?= number_format($price, 2) ?></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <form class="shipping-form">
                                        <?php if ($eventShipping1 == 0 || $eventShipping2 == 0): ?>
                                            <!-- Single option -->
                                            <?php if ($eventShipping1 == 0): ?>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="shipping_method" value="<?= $eventShipping2 ?>">
                                                    <label class="form-check-label">Envío Express (3-5 días) - $<?= number_format($eventShipping2, 2) ?></label>
                                                </div>
                                            <?php else: ?>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="shipping_method" value="<?= $eventShipping1 ?>">
                                                    <label class="form-check-label">Envío Certificado - $<?= number_format($eventShipping1, 2) ?></label>
                                                </div>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="shipping_method" value="<?= $eventShipping1 ?>">
                                                <label class="form-check-label">Envío Certificado - $<?= number_format($eventShipping1, 2) ?></label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="shipping_method" value="<?= $eventShipping2 ?>">
                                                <label class="form-check-label">Envío Express (3-5 días) - $<?= number_format($eventShipping2, 2) ?></label>
                                            </div>
                                        <?php endif; ?>
                                    </form>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="shopping-cart-footer">
                    <div class="column"></div>
                    <div class="column text-lg">Total: <span class="text-medium subtotal">$<?= number_format($price, 2) ?></span></div>
                </div>
                <hr class="padding-bottom-1x">
                <div class="accordion" id="accordion" role="tablist">
                    <div class="card">
                        <div class="card-header" role="tab">
                            <h6><a class="collapsed" href="#spei" data-toggle="collapse" data-parent="#accordion"><i class="fa fa-exchange"></i>Paga con SPEI</a></h6>
                        </div>
                        <div class="collapse" id="spei" role="tabpanel">
                            <div class="card-body">
                                <form action="/user-event-linker/" class="row spei-form" method="post">
                                    <input type="hidden" name="form" value="spei_payment">
                                    <input type="hidden" name="price" value="<?= $price ?>">
                                    <input type="hidden" name="user_id" value="<?= $userId ?>">
                                    <input type="hidden" name="event_id" value="<?= $eventId ?>">
                                    <input type="hidden" name="description" value="<?= $description ?>">
                                    <input type="hidden" name="event_carrier" value="<?= $eventCarrier ?>">
                                    <input class="shipping-input" type="hidden" name="shipping" value="0"/>
                                    <input class="shipping-input" type="hidden" name="shipping_carrier" value="Envío Certificado"/>
                                    <div class="col-12">
                                        <div class="d-flex flex-wrap justify-content-between align-items-center"><a class="navi-link" href="#"></a>
                                            <button class="btn btn-primary btn-block margin-top-none" type="submit">Generar referencia</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" role="tab">
                            <h6><a class="collapsed" href="#card" data-toggle="collapse" data-parent="#accordion"><i class="fa fa-credit-card"></i>Paga con Tarjeta de Crédito/Débito</a></h6>
                        </div>
                        <div class="collapse" id="card" role="tabpanel">
                            <div class="card-body">
                                <div class="card-wrapper"></div>
                                <form action="/user-event-linker/" method="POST" class="interactive-credit-card row" id="card-form">
                                    <input type="hidden" name="form" value="make_payment">
                                    <input type="hidden" name="price" value="<?= $price ?>">
                                    <input type="hidden" name="user_id" value="<?= $userId ?>">
                                    <input type="hidden" name="event_id" value="<?= $eventId ?>">
                                    <input type="hidden" name="description" value="<?= $description ?>">
                                    <input type="hidden" name="event_carrier" value="<?= $eventCarrier ?>">
                                    <input class="shipping-input" type="hidden" name="shipping" value="0"/>
                                    <input class="shipping-input" type="hidden" name="shipping_carrier" value="Envío Certificado"/>
                                    <input type="hidden" data-conekta="card[exp_month]" id="hidden_month" placeholder="Mes"  autocomplete="off" maxlength="2" required>
                                    <input type="hidden" data-conekta="card[exp_year]"  id="hidden_year" placeholder="Año"  autocomplete="off" maxlength="4" required>
                                    <div class="form-group col-sm-6">
                                        <input class="form-control card-number" type="text" name="number" placeholder="Numero de tarjeta" data-conekta="card[number]" required>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <input class="form-control" type="text" name="name" placeholder="Nombre del tarjetahabiente" data-conekta="card[name]" required>
                                    </div>
                                    <div class="form-group col-sm-3">
                                        <input class="form-control exp_date" type="text" name="expiry" placeholder="MM/YYYY" required>
                                    </div>
                                    <div class="form-group col-sm-3">
                                        <input class="form-control" type="text" name="cvc" placeholder="CVC" data-conekta="card[cvc]" required>
                                    </div>
                                    <div class="col-sm-6">
                                        <button class="btn btn-primary btn-block margin-top-none" type="submit">Completar pedido</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" role="tab">
                            <h6><a class="collapsed" href="#oxxo" data-toggle="collapse" data-parent="#accordion"><i class="fa fa-money"></i>Paga con Oxxo Pay</a></h6>
                        </div>
                        <div class="collapse" id="oxxo" role="tabpanel">
                            <div class="card-body">
                                <form action="/user-event-linker/" class="row oxxo-form" method="post">
                                    <input type="hidden" name="form" value="oxxo_payment">
                                    <input type="hidden" name="price" value="<?= $price ?>">
                                    <input type="hidden" name="user_id" value="<?= $userId ?>">
                                    <input type="hidden" name="event_id" value="<?= $eventId ?>">
                                    <input type="hidden" name="description" value="<?= $description ?>">
                                    <input type="hidden" name="event_carrier" value="<?= $eventCarrier ?>">
                                    <input class="shipping-input" type="hidden" name="shipping" value="0"/>
                                    <input class="shipping-input" type="hidden" name="shipping_carrier" value="Envío Certificado"/>
                                    <div class="col-12">
                                        <div class="d-flex flex-wrap justify-content-between align-items-center"><a class="navi-link" href="#"></a>
                                            <button class="btn btn-primary btn-block margin-top-none" type="submit">Generar recibo de pago</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <!-- End Checkout Payment -->
        <!-- Start Sidebar -->
    </div>
</div>

<script src="https://cdn.conekta.io/js/latest/conekta.js"></script>
<!-- End Back To Top -->
<div class="site-backdrop"></div>
<!-- Modernizr JS -->
<script src="<?php echo $config->urls->assets?>js/modernizr.min.js"></script>
<!-- JQuery JS -->
<script src="<?php echo $config->urls->assets?>js/jquery.min.js"></script>
<!-- Popper JS -->
<script src="<?php echo $config->urls->assets?>js/popper.min.js"></script>
<!-- Bootstrap JS -->
<script src="<?php echo $config->urls->assets?>js/bootstrap.min.js"></script>
<!-- CountDown JS -->
<script src="<?php echo $config->urls->assets?>js/count.min.js"></script>
<!-- Gmap JS -->
<script src="<?php echo $config->urls->assets?>js/gmap.min.js"></script>
<!-- ImageLoader JS -->
<script src="<?php echo $config->urls->assets?>js/imageloader.min.js"></script>
<!-- Isotope JS -->
<script src="<?php echo $config->urls->assets?>js/isotope.min.js"></script>
<!-- NouiSlider JS -->
<script src="<?php echo $config->urls->assets?>js/nouislider.min.js"></script>
<!-- OwlCarousel JS -->
<script src="<?php echo $config->urls->assets?>js/owl.carousel.min.js"></script>
<!-- PhotoSwipe UI JS -->
<script src="<?php echo $config->urls->assets?>js/photoswipe-ui-default.min.js"></script>
<!-- PhotoSwipe JS -->
<script src="<?php echo $config->urls->assets?>js/photoswipe.min.js"></script>
<!-- Velocity JS -->
<script src="<?php echo $config->urls->assets?>js/velocity.min.js"></script>
<!-- Card JS -->
<script src="<?php echo $config->urls->assets?>js/card.min.js"></script>
<!-- Main JS -->
<script src="<?php echo $config->urls->assets?>js/script.js"></script>
<!-- Custom JS -->
<script src="<?php echo $config->urls->assets?>js/main.js?v=<?= time(); ?>"></script>

<!-- CDN custom libraries -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/inputmask/4.0.9/inputmask/inputmask.min.js" integrity="sha512-DVfS/GbZzLMmxBL/CW92N84eHP2Fq9d+r9RKbvctcvzISVfu+WvD+MCvbK9j8I6nVLrntGo3UUVrNFUDX0ukBw==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/inputmask/4.0.9/inputmask/jquery.inputmask.min.js" integrity="sha512-BSgdXFh8dxsgqWSpx/j6DiNmcfN+abfgIfKuS8lh7bIfS16w5DSCtVEV7v24VgE4NrN0eDNGhYgNj+BxWatp+Q==" crossorigin="anonymous"></script>

<script type="text/javascript">
    const sandboxMode = false;
    if(sandboxMode)
        Conekta.setPublicKey('key_FgzxNaEDWEDqn8WTEnaVerg');
    else
        Conekta.setPublicKey('key_bFsrqcBVyNgXyEheKkrDEyw');
    (function($) {
        var conektaSuccessResponseHandler = function(token) {
            var $form = $("#card-form");
            //Inserta el token_id en la forma para que se envíe al servidor
            $form.append($('<input type="hidden" name="conektaTokenId" id="conektaTokenId">').val(token.id));
            // $form.get(0).submit(); //Hace submit

            Swal.fire('Espera un momento', 'Se está procesando la información');
            Swal.showLoading();

            $.post($form.attr('action'), $form.serialize()).then(result => {
                if(result.status) {
                    Swal.fire('¡Éxito!', result.message, 'success').then(swalResult => {
                        window.parent.close();
                    });
                } else {
                    Swal.fire('Error', result.message, 'error');
                    $form[0].reset();
                    $form.find('input[name="conektaTokenId"]').remove();
                }
            });
        };
        var conektaErrorResponseHandler = function(response) {
            var $form = $("#card-form");
            // $form.find(".card-errors").text(response.message_to_purchaser);
            $form.find("button").prop("disabled", false);

            Swal.fire('Error', response.message_to_purchaser, 'error');
        };


        $("#card-form").submit(function(event) {
            var $form = $(this);
            // Previene hacer submit más de una vez
            $form.find("button").prop("disabled", true);
            Conekta.Token.create($form, conektaSuccessResponseHandler, conektaErrorResponseHandler);
            return false;
        });

        $('.exp_date').change(function() {
            let expDate = $(this).val().split('/');
            if(expDate.length > 1) {
                let month = expDate[0].trim();
                let year  = expDate[1].trim();

                $('#hidden_month').val(month);
                $('#hidden_year').val(year);
            }
        });

        if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
            || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) {
            $('.exp_date').inputmask('99/9999');
            $('.card-number').inputmask('9999 9999 9999 9999');
        }

        $('.oxxo-form').submit(function(event) {
            event.preventDefault();
            Swal.fire('Espera un momento', 'Se está procesando la información');
            Swal.showLoading();

            var $form = $(this);

            $.post($form.attr('action'), $form.serialize()).then(result => {
                if(result.status) {
                    Swal.fire('¡Éxito!', result.message, 'success').then(swalResult => {
                        window.parent.close();
                    });
                } else {
                    Swal.fire('Error', result.message, 'error');
                }
            });
        });

        $('.spei-form').submit(function(event) {
            event.preventDefault();
            Swal.fire('Espera un momento', 'Se está procesando la información');
            Swal.showLoading();

            var $form = $(this);

            $.post($form.attr('action'), $form.serialize()).then(result => {
                if(result.status) {
                    console.log(result);debugger;
                    Swal.fire({
                        title: '¡Éxito!',
                        icon: 'success',
                        html: `
                            Banco: <span style="font-weight: bold;">${ result.data.bank }</span><br/>
                            CLABE: <span style="font-weight: bold;">${ result.data.number }</span><br/>
                            Cantidad: <span style="font-weight: bold;">${ result.data.amount }</span><br/>
                            Referencia: <span style="font-weight: bold;">TU NOMBRE</span><br/>
                        `
                    }).then(swalResult => {
                        window.parent.close();
                    });
                } else {
                    Swal.fire('Error', result.message, 'error');
                }
            });
        });

        $('.shipping-form input').change(function(event) {
            let eventPrice = <?= $price ?>;
            let shipping = $(this).val();
            let subtotal = eventPrice + parseFloat($(this).val());
            let label = $(this).parents('.form-check').find('label');

            $('.shipping-input').remove();

            let that = this;
            $('.shipping-form input').each(function(index, element) {
                if(element != that)
                    $(element).prop('checked', false);
            });

            if($(this).is(':checked')) {
                $('.subtotal').html('$' + subtotal.toFixed(2));

                $('form.row').each(function(index, element) {
                    $element = $(element);
                    $element.append(`<input class="shipping-input" type="hidden" name="shipping" value="${ shipping }"/>`);
                    $element.append(`<input class="shipping-input" type="hidden" name="shipping_carrier" value="${ label.text() }"/>`);
                });
            } else {
                $('.subtotal').html('$' + eventPrice.toFixed(2));
                label = 'Envío Certificado';
                shipping = 0;

                $('form.row').each(function(index, element) {
                    $element = $(element);
                    $element.append(`<input class="shipping-input" type="hidden" name="shipping" value="${ shipping }"/>`);
                    $element.append(`<input class="shipping-input" type="hidden" name="shipping_carrier" value="${ label }"/>`);
                });
            }
        });
    })(jQuery);
</script>

</body>
</html>

<?php //require_once './footer.inc'; ?>
