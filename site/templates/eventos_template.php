<?php require_once './header.inc'; ?>

<?php
    function generateRandomString($length = 10) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    $privateEvents = $page->children("event_classification=Privado,event_access_code=");

    foreach ($privateEvents as $pEvent) {
        $pEvent->event_access_code = generateRandomString(6);
        $pEvent->of(false);
        $pEvent->save();
    }

    $events     = $page->children("limit=99,event_visible=1,event_finished=0, sort=-id");
    $pastEvents = $page->children("limit=99,event_visible=1,event_finished=1, sort=-id");
    $packages = $pages->find('template=paquete_template, event_visible=1');

    $userEvents = [];

    foreach($user->user_events as $userEvent) {
        if(isset($userEvent->event))
            $userEvents[] = $userEvent->event->id;
    }

    $messages = [];
    $error = false;
    if($input->get('dispatch') !== null) {
        $dispatch = $input->get('dispatch');
        switch ($dispatch) {
            case 'SUCCESS_REGISTER':
                $messages[] = 'Evento añadido correctamente';
                break;
            case 'NO_QUOTA':
                $messages[] = 'Ya el evento de inscripción libre llegó a su límite de cupo, favor de contactar al organizador del evento';
                $error = true;
                break;
            case 'ERROR':
                $messages[] = 'Ocurrió un error, favor de contactar al organizador del evento';
                $error = true;
                break;
        }
    }

    $message = array_shift($messages);

    $privateEvent = false;
    if ($input->get('code') !== null) {
        $code = $input->get('code');

        $event = $page->get('template=evento_template, event_access_code=' . $code);

        if($event->id) {
            $privateEvent = true;
            $events = [ $event ];
        }
    }
?>
<?php if(!$privateEvent): ?>
<div class="container">
    <div class="row">
        <div class="col-md-12 padding-top-1x">
            <ul class="nav nav-tabs event-tabs">
                <li>
                    <a  href="#eventos" class="active show" data-toggle="tab">Eventos</a>
                 </li>
                <li >
                    <a  href="#eventos-pasados" data-toggle="tab">Terminados</a>
                 </li>
                <li>
                    <a  href="#paquetes" data-toggle="tab">Paquetes</a>
                 </li>
                 <li>
                     <button class="btn pull-right btn-private" type="button">¿Código a evento privado?</button>
                 </li>
            </ul>
        </div>
    </div>
</div>
<?php endif; ?>

<div class="event-container">
    <div class="tab-content event-tab-container">
        <div class="tab-pane active" id="eventos">
            <div class="container">
                <div class="row">
                    <?php foreach($events as $event): ?>
                        <div id="event-<?php echo urlencode(str_replace(' ', '-', mb_strtolower($event->title))); ?>" class="col-md-4 event-details event-element <?= $event->event_classification->title == 'Privado' && !$privateEvent ? 'd-none' : ''; ?>"  data-id="<?= $event->id; ?>" data-logged="<?= $user->isLoggedIn() ? '1' : '0' ?>" data-already="<?= in_array($event->id, $userEvents) ? '1' : '0'  ?>">
                            <img src="<?= $event->event_images->eq(0)->httpUrl; ?>" alt="">
                            <p><?= $event->title;  ?></p>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <?php if (false): ?>
                <?php foreach($events as $event): ?>
                    <div id="event-<?php echo urlencode(str_replace(' ', '-', mb_strtolower($event->title))); ?>" class="event-details container padding-top-1x padding-bottom-3x d-none" data-id="<?= $event->id; ?>">
                        <div class="row">
                            <!-- Start Product Gallery -->
                            <div class="col-md-6">
                                <div class="product-gallery">
                                    <div class="gallery-wrapper">
                                        <?php $isFirstImage = true; ?>
                                        <?php foreach ($event->event_images as $key => $image): ?>
                                            <?php if ($isFirstImage): ?>
                                                <?php $isFirstImage = false; ?>
                                                <div class="gallery-item active"><a href="<?php echo $image->url; ?>" data-hash="<?php echo $event->id . crc32($key); ?>" data-size="1000x667"></a></div>
                                            <?php else: ?>
                                                <div class="gallery-item"><a href="<?php echo $image->url; ?>" data-hash="<?php echo $event->id . crc32($key); ?>" data-size="1000x667"></a></div>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </div>
                                    <div class="product-carousel owl-carousel">
                                        <?php foreach ($event->event_images as $key => $image): ?>
                                            <div data-hash="<?php echo $event->id . crc32($key); ?>"><img src="<?php echo $image->url; ?>"></div>
                                        <?php endforeach; ?>
                                    </div>
                                    <ul class="product-thumbnails">
                                        <?php $isFirstImage = true; ?>
                                        <?php foreach ($event->event_images as $key => $image): ?>
                                            <?php if ($isFirstImage): ?>
                                                <?php $isFirstImage = false; ?>
                                                <li class="active"><a href="#<?php echo $event->id . crc32($key); ?>"><img src="<?php echo $image->url; ?>"></a></li>
                                            <?php else: ?>
                                                <li><a href="#<?php echo $event->id . crc32($key); ?>"><img src="<?php echo $image->url; ?>"></a></li>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                            <!-- End Product Gallery -->
                            <!-- Start Product Info -->
                            <div class="col-md-6 single-shop">
                                <div class="hidden-md-up"></div>
                                <h2 class="padding-top-1x text-normal with-side"><?php echo $event->title; ?>
                                    <?php if ($event->event_classification->title == 'Privado'): ?>
                                        <img class="private-icon" src="<?= $config->urls->assets?>images/team.png" />
                                    <?php endif; ?></h2>
                                    <span class="h2 d-block with-side"><?php
                                    if($event->event_type->title == 'Distancia' || $event->event_type->title == 'Entrenamiento')
                                    echo 'DISTANCIA: ' . floatval($event->event_distance) . ' KM';
                                    else
                                    echo 'DURACIÓN: ' . floatval($event->event_duration) . ' minutos';
                                    ?></span>
                                    <div class="event_price">
                                        <?= $event->event_price; ?>
                                        <!-- <p><b>$95</b> correo standard</p> -->
                                        <!-- <p><b>$125 </b>correo certificado <u>(incluye número de seguimiento)</u></p> -->
                                    </div>
                                    <div class="event__description">
                                        <?php echo $event->event_description; ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <hr class="mt-30 mb-30">
                                    <div class="d-flex flex-wrap justify-content-between mb-30">
                                        <div class="sp-buttons mt-2 mb-2">
                                            <?php if (!in_array($event->id, $userEvents)): ?>
                                                <?php if($user->isLoggedIn()): ?>
                                                    <div class="register-modal" style="display: none;">
                                                        <?php if ($event->id_boletify != null && $event->id_boletify != 0): ?>
                                                            <button class="btn btn-primary btn-boletify" data-url="<?= $event->boletify_url; ?>" <?php echo $event->boletify_url == '' ? 'disabled' : ''; ?>><?= $event->event_text_button; ?></button>
                                                        <?php endif; ?>

                                                        <?php if ($event->id_boletify2 != null && $event->id_boletify2 != 0): ?>
                                                            <button class="btn btn-primary btn-boletify" data-url="<?= $event->boletify_url2; ?>" <?php echo $event->boletify_url2 == '' ? 'disabled' : ''; ?>><?= $event->event_text_button2; ?></button>
                                                        <?php endif; ?>

                                                        <?php if ($event->id_boletify3 != null && $event->id_boletify3 != 0): ?>
                                                            <button class="btn btn-primary free-register" data-url="/user-event-linker/?dispatch=free_register&event_id=<?= $event->id_boletify3 ?>" data-request="<?= $event->event_request_text ?>" data-title="<?= $event->event_extra_label_1; ?>" style="width: 90%;"><?= $event->event_text_button3; ?></button>
                                                        <?php endif; ?>
                                                    </div>
                                                    <button type="button" class="btn btn-primary btn-register-modal" id="btn-event<?= $event->id ?>"><i class="icon-plus"></i> Inscribirte</button>
                                                    <!-- <form action="/user-event-linker/" method="POST">
                                                    <input type="hidden" name="form" value="event_register">
                                                    <input type="hidden" name="event" value="<?php echo $event->id ?>">
                                                    <button class="btn btn-primary" name="submit" type="submit"><i class="icon-plus"></i> Inscribirte</button>
                                                </form> -->
                                            <?php else: ?>
                                                <form action="/login/" method="GET">
                                                    <input type="hidden" name="dispatch" value="LOGIN_FIRST"/>
                                                    <input type="hidden" name="event_id" value="<?= $event->id ?>"/>
                                                    <button class="btn btn-primary" name="submit" type="submit"><i class="icon-plus"></i> Inscribirte</button>
                                                </form>
                                            <?php endif; ?>

                                        <?php endif; ?>
                                        <a href="/resultados/?event=<?php echo $event->name; ?>" class="btn btn-primary" target="_blank"><i class="icon-align-justify"></i> Resultados</a>
                                    </div>
                                </div>
                            </div>
                            <!-- End Product Info -->
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div class="tab-pane" id="eventos-pasados">
            <?php foreach($pastEvents as $event): ?>
                <div id="event-<?php echo urlencode(str_replace(' ', '-', mb_strtolower($event->title))); ?>" class="event-details container padding-top-1x padding-bottom-3x <?php echo $event->event_classification->title == 'Privado' && !$privateEvent ? 'd-none' : ''; ?>" data-id="<?= $event->id; ?>">
                    <div class="row">
                        <!-- Start Product Gallery -->
                        <div class="col-md-6">
                            <div class="product-gallery">
                                <div class="gallery-wrapper">
                                    <?php $isFirstImage = true; ?>
                                    <?php foreach ($event->event_images as $key => $image): ?>
                                        <?php if ($isFirstImage): ?>
                                            <?php $isFirstImage = false; ?>
                                            <div class="gallery-item active"><a href="<?php echo $image->url; ?>" data-hash="<?php echo $event->id . crc32($key); ?>" data-size="1000x667"></a></div>
                                        <?php else: ?>
                                            <div class="gallery-item"><a href="<?php echo $image->url; ?>" data-hash="<?php echo $event->id . crc32($key); ?>" data-size="1000x667"></a></div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </div>
                                <div class="product-carousel owl-carousel">
                                    <?php foreach ($event->event_images as $key => $image): ?>
                                        <div data-hash="<?php echo $event->id . crc32($key); ?>"><img src="<?php echo $image->url; ?>"></div>
                                    <?php endforeach; ?>
                                </div>
                                <ul class="product-thumbnails">
                                    <?php $isFirstImage = true; ?>
                                    <?php foreach ($event->event_images as $key => $image): ?>
                                        <?php if ($isFirstImage): ?>
                                            <?php $isFirstImage = false; ?>
                                            <li class="active"><a href="#<?php echo $event->id . crc32($key); ?>"><img src="<?php echo $image->url; ?>"></a></li>
                                        <?php else: ?>
                                            <li><a href="#<?php echo $event->id . crc32($key); ?>"><img src="<?php echo $image->url; ?>"></a></li>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                        <!-- End Product Gallery -->
                        <!-- Start Product Info -->
                        <div class="col-md-6 single-shop">
                            <div class="hidden-md-up"></div>
                            <h2 class="padding-top-1x text-normal with-side"><?php echo $event->title; ?>
                            <?php if ($event->event_classification->title == 'Privado'): ?>
                                <img class="private-icon" src="<?= $config->urls->assets?>images/team.png" />
                            <?php endif; ?></h2>
                            <span class="h2 d-block with-side"><?php
                                if($event->event_type->title == 'Distancia' || $event->event_type->title == 'Entrenamiento')
                                    echo 'DISTANCIA: ' . floatval($event->event_distance) . ' KM';
                                else
                                    echo 'DURACIÓN: ' . floatval($event->event_duration) . ' minutos';
                            ?></span>
                            <div class="event_price">
                                <!-- <?= $event->event_price; ?> -->
                                <!-- <p><b>$95</b> correo standard</p> -->
                                <!-- <p><b>$125 </b>correo certificado <u>(incluye número de seguimiento)</u></p> -->
                            </div>
                            <div class="event__description">
                              <?php echo $event->event_description; ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr class="mt-30 mb-30">
                            <div class="d-flex flex-wrap justify-content-between mb-30">
                                <div class="sp-buttons mt-2 mb-2">
                                    <a href="/resultados/?event=<?php echo $event->name; ?>" class="btn btn-primary" target="_blank"><i class="icon-align-justify"></i> Resultados</a>
                                </div>
                            </div>
                        </div>
                        <!-- End Product Info -->
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="tab-pane" id="paquetes">
            <?php foreach ($packages as $package): ?>
                <div id="package-<?php echo urlencode(str_replace(' ', '-', mb_strtolower($event->title))); ?>" class="event-details container padding-top-1x padding-bottom-3x" data-id="<?= $package->id; ?>">
                    <div class="row">
                        <!-- Start Product Gallery -->
                        <div class="col-md-6">
                            <div class="product-gallery">
                                <div class="gallery-wrapper">
                                    <?php $isFirstImage = true; ?>
                                    <?php foreach ($package->event_images as $key => $image): ?>
                                        <?php if ($isFirstImage): ?>
                                            <?php $isFirstImage = false; ?>
                                            <div class="gallery-item active"><a href="<?php echo $image->url; ?>" data-hash="<?php echo $package->id . crc32($key); ?>" data-size="1000x667"></a></div>
                                        <?php else: ?>
                                            <div class="gallery-item"><a href="<?php echo $image->url; ?>" data-hash="<?php echo $package->id . crc32($key); ?>" data-size="1000x667"></a></div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </div>
                                <div class="product-carousel owl-carousel">
                                    <?php foreach ($package->event_images as $key => $image): ?>
                                        <div data-hash="<?php echo $package->id . crc32($key); ?>"><img src="<?php echo $image->url; ?>"></div>
                                    <?php endforeach; ?>
                                </div>
                                <ul class="product-thumbnails">
                                    <?php $isFirstImage = true; ?>
                                    <?php foreach ($package->event_images as $key => $image): ?>
                                        <?php if ($isFirstImage): ?>
                                            <?php $isFirstImage = false; ?>
                                            <li class="active"><a href="#<?php echo $package->id . crc32($key); ?>"><img src="<?php echo $image->url; ?>"></a></li>
                                        <?php else: ?>
                                            <li><a href="#<?php echo $package->id . crc32($key); ?>"><img src="<?php echo $image->url; ?>"></a></li>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                        <!-- End Product Gallery -->
                        <!-- Start Product Info -->
                        <div class="col-md-6 single-shop">
                            <div class="hidden-md-up"></div>
                            <h2 class="padding-top-1x text-normal with-side"><?php echo $package->title; ?>
                            <div class="event_price">
                                <?= $package->event_price; ?>
                                <!-- <p><b>$95</b> correo standard</p> -->
                                <!-- <p><b>$125 </b>correo certificado <u>(incluye número de seguimiento)</u></p> -->
                            </div>
                            <div class="event__description">
                              <?php echo $package->event_description; ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr class="mt-30 mb-30">
                            <div class="d-flex flex-wrap justify-content-between mb-30">
                                <div class="sp-buttons mt-2 mb-2">
                                    <?php if($user->isLoggedIn()): ?>
                                        <div class="register-modal" style="display: none;">
                                        <?php if ($package->is_conekta): ?>
                                                <?php if($package->event_price1 != null && $package->event_price1 > 0 && $package->event_text_button != ''): ?>
                                                    <?php $conektaUrl1 = 'https://v-run.mx/conekta-checkout/?' . http_build_query([
                                                        'user_id'   => $user->id,
                                                        'event_id'  => $package->id,
                                                        'price'     => $package->event_price1,
                                                        'description' => $package->event_text_button,
                                                        'event_carrier' => $package->event_carrier,
                                                        'event_shipping1' => $package->event_shipping1a,
                                                        'event_shipping2' => $package->event_shipping1b
                                                    ]); ?>
                                                    <button class="btn btn-primary btn-conekta" data-url="<?= $conektaUrl1 ?>"><?= $package->event_text_button ?></button>
                                                <?php endif; ?>

                                                <?php if($package->event_price2 != null && $package->event_price2 > 0 && $package->event_text_button2 != ''): ?>
                                                    <?php $conektaUrl2 = 'https://v-run.mx/conekta-checkout/?' . http_build_query([
                                                        'user_id'   => $user->id,
                                                        'event_id'  => $package->id,
                                                        'price'     => $package->event_price2,
                                                        'description' => $package->event_text_button2,
                                                        'event_carrier' => $package->event_carrier2,
                                                        'event_shipping1' => $package->event_shipping2a,
                                                        'event_shipping2' => $package->event_shipping2b
                                                    ]); ?>
                                                    <button class="btn btn-primary btn-conekta" data-url="<?= $conektaUrl2 ?>"><?= $package->event_text_button2 ?></button>
                                                <?php endif; ?>
                                            <?php else: ?>
                                                <?php if ($package->id_boletify != null && $package->id_boletify != 0): ?>
                                                    <button class="btn btn-primary btn-boletify" data-url="<?= $package->boletify_url; ?>" <?php echo $package->boletify_url == '' ? 'disabled' : ''; ?>><?= $package->event_text_button; ?></button>
                                                <?php endif; ?>

                                                <?php if ($package->id_boletify2 != null && $package->id_boletify2 != 0): ?>
                                                    <button class="btn btn-primary btn-boletify" data-url="<?= $package->boletify_url2; ?>" <?php echo $package->boletify_url2 == '' ? 'disabled' : ''; ?>><?= $package->event_text_button2; ?></button>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                        <button type="button" class="btn btn-primary btn-register-modal"><i class="icon-cart"></i> Comprar ahora</button>
                                        <!-- <form action="/user-event-linker/" method="POST">
                                            <input type="hidden" name="form" value="event_register">
                                            <input type="hidden" name="event" value="<?php echo $event->id ?>">
                                            <button class="btn btn-primary" name="submit" type="submit"><i class="icon-cart"></i> Inscribirte</button>
                                        </form> -->
                                    <?php else: ?>
                                        <form action="/login/" method="GET">
                                            <input type="hidden" name="dispatch" value="LOGIN_FIRST">
                                            <button class="btn btn-primary" name="submit" type="submit"><i class="icon-cart"></i> Comprar ahora</button>
                                        </form>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <!-- End Product Info -->
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

</div>
    <?php if (isset($message)): ?>
        <script type="text/javascript">
            function onInit() {
                Swal.fire({
                    title: '<?= $error ? 'ERROR' : '¡Éxito!' ?>',
                    text: "<?= $message ?>",
                    type: '<?= $error ? 'error' : 'success' ?>',
                    showCancelButton: true,
                    confirmButtonText: 'Ir a mi perfil',
                    cancelButtonText: 'Cerrar',
                    reverseButtons: true
                }).then((result) => {
                    if (result.value) {
                        window.location.href = '/perfil/';
                    }
                })
            }
        </script>
    <?php endif; ?>

    <?php if ($session->event_id): ?>
        <?php
            $eventPage = $pages->get($session->event_id);
            $event = [
                'id'            => $eventPage->id,
                'title'         => $eventPage->title,
                'name'          => $eventPage->name,
                'end_date'      => $eventPage->event_end_date,
                'price'         => $eventPage->event_price,
                'image'         => $eventPage->event_images->eq(0)->httpUrl,
                'id_boletify'   => $eventPage->id_boletify,
                'id_boletify2'  => $eventPage->id_boletify2,
                'id_boletify3'  => $eventPage->id_boletify3,
                'boletify_url'  => $eventPage->boletify_url,
                'boletify_url2' => $eventPage->boletify_url2,
                'event_text_button'     => $eventPage->event_text_button,
                'event_text_button2'    => $eventPage->event_text_button2,
                'event_text_button2'    => $eventPage->event_text_button2,
                'event_request_text'    => $eventPage->event_request_text,
                'event_extra_label_1'   => $eventPage->event_extra_label_1,
                'is_conekta'            => $eventPage->is_conekta,
                'event_price1'          => $eventPage->event_price1,
                'event_price2'          => $eventPage->event_price2,
                'event_carrier'         => $eventPage->event_carrier,
                'event_carrier2'        => $eventPage->event_carrier2,
                'event_shipping1a'      => $eventPage->event_shipping1a,
                'event_shipping1b'      => $eventPage->event_shipping1b,
                'event_shipping2a'      => $eventPage->event_shipping2a,
                'event_shipping2b'      => $eventPage->event_shipping2b,
            ];
        ?>
        <script type="text/javascript">
            // try {
                var selectedEvent = JSON.parse('<?= utf8_decode(json_encode($event)); ?>');
            // } catch(e) {
            //     var selectedEvent = null;
            // }
            function onInit() {
                let event_id = <?= $session->event_id ?>;
                let user_id  = <?= $user->id; ?>;
                let $events = $('.event-details');
                $events.addClass('d-none');
                $events.each(function(idx, item) {
                    if($(item).data('id') == event_id) {
                        $(item).removeClass('d-none');
                    }
                });

                $('#btn-event' + event_id).click();

                console.log(selectedEvent);
                if(selectedEvent !== null) {
                    let registerButton = '';
                    if(selectedEvent.is_conekta) {
                        if(selectedEvent.event_price1 != null && selectedEvent.event_price1 > 0 && selectedEvent.event_text_button != '') {
                            const conektaUrl1 = `https://v-run.mx/conekta-checkout/?user_id=${ user_id }&event_id=${ selectedEvent.id }&price=${ selectedEvent.event_price1 }&description=${  encodeURIComponent(selectedEvent.event_text_button) }&event_carrier=${ selectedEvent.event_carrier }&event_shipping1=${ selectedEvent.event_shipping1a }&event_shipping2=${ selectedEvent.event_shipping1b }`;
                            registerButton += `<button class="btn btn-primary btn-conekta" data-url="${ conektaUrl1 }">${ selectedEvent.event_text_button }</button>`;
                        }

                        if(selectedEvent.event_price2 != null && selectedEvent.event_price2 > 0 && selectedEvent.event_text_button2 != '') {
                            const conektaUrl2 = `https://v-run.mx/conekta-checkout/?user_id=${ user_id }&event_id=${ selectedEvent.id }&price=${ selectedEvent.event_price2 }&description=${  encodeURIComponent(selectedEvent.event_text_button2) }&event_carrier=${ selectedEvent.event_carrier2 }&event_shipping1=${ selectedEvent.event_shipping2a }&event_shipping2=${ selectedEvent.event_shipping2b }`;
                            registerButton += `<button class="btn btn-primary btn-conekta" data-url="${ conektaUrl2 }">${ selectedEvent.event_text_button2 }</button>`;
                        }
                    } else {
                        if(selectedEvent.id_boletify != null && selectedEvent.id_boletify != 0)
                            registerButton += `<button class="btn btn-primary btn-boletify" data-url="${ selectedEvent.boletify_url }" ${ selectedEvent.boletify_url == '' ? 'disabled' : '' }>${ selectedEvent.event_text_button }</button>`;

                        if(selectedEvent.id_boletify2 != null && selectedEvent.id_boletify2 != 0)
                            registerButton += `<button class="btn btn-primary btn-boletify" data-url="${ selectedEvent.boletify_url2 }" ${ selectedEvent.boletify_url2 == '' ? 'disabled' : '' }>${ selectedEvent.event_text_button2 }</button>`;

                        if(selectedEvent.id_boletify3 != null && selectedEvent.id_boletify3 != 0)
                            registerButton += `<button class="btn btn-primary free-register" data-url="/user-event-linker/?dispatch=free_register&event_id=${ response.data.event.id_boletify3 }" data-request="${ response.data.event.event_request_text }" data-title="${ response.data.event.event_extra_label_1 }" style="width: 90%;">${ response.data.event.event_text_button3 }</button>`;
                    }

                    $('#register-modal .modal-body').html(registerButton);


                    $('.btn-conekta').click(function() {
                        let url = $(this).data('url');

                        if(windowPrompt)
                            windowPrompt.close();

                        var w = 1013;
                        var h = window.innerHeight;

                        let code = `
                        <html>
                        <head>
                            <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        </head>
                        <body><iframe src="${ url }" width="100%" height="100%" scrolling="auto"></iframe></body>
                        </html>`;

                        windowPrompt = window.open("", "", `width=${ w },height=${ h }`);
                        windowPrompt.document.open("text/html", "replace");
                        windowPrompt.document.write(code);

                        windowPrompt.onbeforeunload = function() {
                            window.location.reload();
                        }
                    });

                    $('.btn-boletify').click(function() {
                        let url = $(this).data('url');

                        if(windowPrompt)
                            windowPrompt.close();

                        var w = 1013;
                        var h = window.innerHeight;

                        let code = '<iframe src="' + url + '" width="100%" height="100%" scrolling="auto"></iframe>';

                        windowPrompt = window.open("", "", `width=${ w },height=${ h }`);
                        windowPrompt.document.open("text/html", "replace");
                        windowPrompt.document.write(code);

                        windowPrompt.onbeforeunload = function() {
                            window.location.reload();
                        }
                    });

                    $('#register-modal').modal('show');

                }
            }
        </script>
        <?php $session->event_id = null; ?>
    <?php endif; ?>

<?php require_once './footer.inc'; ?>
