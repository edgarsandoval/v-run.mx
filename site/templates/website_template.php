<?php require_once './header.inc'; ?>

<div class="container home-container">
    <?php foreach ($page->section as $i => $section): ?>
        <?php if ($i % 2 == 0): ?>
            <div class="row align-items-center padding-bottom-3x">
                <div class="col-md-5">
                    <img class="d-block w-270 m-auto" src="<?= $section->image->httpUrl; ?>">
                </div>
                <div class="col-md-7 text-md-left text-center">
                    <div class="hidden-md-up"></div>
                    <h2><?= $section->title; ?></h2>
                    <div style="color: #00b3ff; ">
                        <?= $section->body;  ?>
                    </div>
                </div>
            </div>
            <hr>
            <br/>
        <?php else: ?>
            <div class="row align-items-center padding-top-3x padding-bottom-3x">
                <div class="col-md-5 order-md-2">
                    <img class="d-block w-270 m-auto" src="<?= $section->image->httpUrl; ?>">
                </div>
                <div class="col-md-7 order-md-1 text-md-left text-center">
                    <div class="hidden-md-up"></div>
                    <h2><?= $section->title; ?></h2>
                    <div style="color: #00b3ff; ">
                        <?= $section->body;  ?>
                    </div>
                </div>
            </div>
            <hr>
            <br/>
        <?php endif; ?>
    <?php endforeach; ?>
</div>

<?php require_once './footer.inc'; ?>
