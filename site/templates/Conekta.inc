<?php

class Conekta {
    public static $sandboxMode = false;
    public static function initConfig() {
        if(self::$sandboxMode)
            \Conekta\Conekta::setApiKey('key_j6SXNP4d5SUCPsqMnTaBgw');
        else
            \Conekta\Conekta::setApiKey('key_iDokYyeX7PPqF4mmgiVyUQ');

        \Conekta\Conekta::setLocale('es');
    }

    public static function cardPayment($data, $user) {
        try {
            $data['amount'] = $data['price'];
            $address = self::buildAddress($user);

            $price = round($data['amount'] / 1.16, 2) * 100;
            $tax = $data['amount'] * 100 - $price;

            $order = \Conekta\Order::create([
                'line_items' => [
                    [
                        'name' => 'Inscripción electrónica V-RUN',
                        'unit_price' => intval($price),
                        'quantity' => 1
                    ]
                ], //line_items,
                'tax_lines' => [
                    [
    					'description' 	=> "IVA",
    					'amount' 		=> intval($tax),
    				]
                ],
                'shipping_lines' => [
                    [
                        'amount'    => intval($data['shipping']) * 100,
                        'carrier'   => $data['carrier']
                    ]
                ],
                'shipping_contact' => [
                    'address' => $address
                ],
                'currency' => 'MXN',
                'customer_info' => [
                    'name' => trim(sprintf('%s %s', $user->first_name, $user->last_name)),
                    'email' => $user->email,
                    'phone' => '+521' . rand(10000000, 99999999)
                ],
                'charges' => [
                    [
                        'payment_method' => [
                            'type' => 'card',
                            'token_id' => $data['conektaTokenId']
                        ]
                    ]
                ],
                'metadata' => [
                    'evento'        => $data['event']->title,
                    'inscripcion'   => $data['description'],
                    'email'         => $user->email,
                    'carrier'       => $data['carrier'],
                    'eventId'       => $data['event']->id,
                    'shipping_amount' => $data['shipping']
                ]
            ]);
        }
        catch(\Conekta\ProcessingError $error) {
            return $error;
        }
        catch(\Conekta\ParameterValidationError $error) {
            return $error->getMessage();
        }
        catch(\Conekta\Handler $error) {
            return $error->getMessage();
        }

        return $order;
    }


    public static function oxxoPayment($data, $user) {
        try {
            $data['amount'] = $data['price'];
            $limit = (new \DateTime())->add(new \DateInterval('P7D'))->getTimestamp();
            $address = self::buildAddress($user);

            $price = round($data['amount'] / 1.16, 2) * 100;
            $tax = $data['amount'] * 100 - $price;

            $order = \Conekta\Order::create([
                'livemode' => !self::$sandboxMode,
                'line_items' => [
                    [
                        'name' => 'Inscripción electrónica V-RUN',
                        'unit_price' => intval($price),
                        'quantity' => 1
                    ]
                ], //line_items,
                'tax_lines' => [
                    [
    					'description' 	=> "IVA",
    					'amount' 		=> intval($tax),
    				]
                ],
                'shipping_lines' => [
                    [
                        'amount'    => intval($data['shipping']) * 100,
                        'carrier'   => $data['carrier']
                    ]
                ],
                'shipping_contact' => [
                    'address' => $address
                ],
                'currency' => 'MXN',
                'customer_info' => [
                    'name' => trim(sprintf('%s %s', $user->first_name, $user->last_name)),
                    'email' => $user->email,
                    'phone' => '+521' . rand(10000000, 99999999)
                ],
                'charges' => [
                    [
                        'payment_method' => [
                            'type' => 'oxxo_cash',
                            'expires_at' => $limit
                        ]
                    ]
                ],
                'metadata' => [
                    'evento'        => $data['event']->title,
                    'inscripcion'   => $data['description'],
                    'email'         => $user->email,
                    'carrier'       => $data['carrier'],
                    'eventId'       => $data['event']->id,
                    'shipping_amount' => $data['shipping']
                ]
            ]);
        }
        catch(\Conekta\ProcessingError $error) {
            return $error;
        }
        catch(\Conekta\ParameterValidationError $error) {
            return $error->getMessage();
        }
        catch(\Conekta\Handler $error) {
            return $error->getMessage();
        }

        return $order;
    }

    public static function speiPayment($data, $user) {
        try {
            $data['amount'] = $data['price'];
            $limit = (new \DateTime())->add(new \DateInterval('P7D'))->getTimestamp();
            $address = self::buildAddress($user);

            $price = round($data['amount'] / 1.16, 2) * 100;
            $tax = $data['amount'] * 100 - $price;

            $order = \Conekta\Order::create([
                'livemode' => !self::$sandboxMode,
                'line_items' => [
                    [
                        'name' => 'Inscripción electrónica V-RUN',
                        'unit_price' => intval($price),
                        'quantity' => 1
                    ]
                ], //line_items,
                'tax_lines' => [
                    [
    					'description' 	=> "IVA",
    					'amount' 		=> intval($tax),
    				]
                ],
                'shipping_lines' => [
                    [
                        'amount'    => intval($data['shipping']) * 100,
                        'carrier'   => $data['carrier']
                    ]
                ],
                'shipping_contact' => [
                    'address' => $address
                ],
                'currency' => 'MXN',
                'customer_info' => [
                    'name' => trim(sprintf('%s %s', $user->first_name, $user->last_name)),
                    'email' => $user->email,
                    'phone' => '+521' . rand(10000000, 99999999)
                ],
                'charges' => [
                    [
                        'payment_method' => [
                            'type' => 'spei',
                            'expires_at' => $limit
                        ]
                    ]
                ],
                'metadata' => [
                    'evento'        => $data['event']->title,
                    'inscripcion'   => $data['description'],
                    'email'         => $user->email,
                    'carrier'       => $data['carrier'],
                    'eventId'       => $data['event']->id,
                    'shipping_amount' => $data['shipping']
                ]
            ]);
        }
        catch(\Conekta\ProcessingError $error) {
            return $error;
        }
        catch(\Conekta\ParameterValidationError $error) {
            return $error->getMessage();
        }
        catch(\Conekta\Handler $error) {
            return $error->getMessage();
        }

        return $order;
    }

    private static function buildAddress($u) {
        try {
            if($u->address[0] === false) {
                return [
                    'street1' => 'Alvarez del Castillo 814',
                    'postal_code' => '45027',
                    'country' => 'MX'
                ];
            } else {
                return [
                    'street1' => $u->address[0]->address_street . ' ' . $u->address[0]->address_outdoor_number,
                    'postal_code' => $u->address[0]->address_zip_code,
                    'country' => 'MX'
                ];
            }
        } catch(\Exception $e) {
            return [
                'street1' => 'Alvarez del Castillo 814',
                'postal_code' => '45027',
                'country' => 'MX'
            ];
        }
    }
}
