<?php require_once './header.inc'; ?>
<?php

$render = 'contact_form';

if($input->post->submit !== null) {
    if($input->post->form == 'contact') {
        $data = array(
            'full_name' => wire('sanitizer')->text(wire('input')->post('fulll_name')),
            'email' => wire('sanitizer')->email(wire('input')->post('email')),
            'phone' => wire('sanitizer')->text(wire('input')->post('phone')),
            'message' => wire('sanitizer')->text(wire('input')->post('message'))
        );

        $m = wireMail();

        $recipients = array('contacto@v-run.mx'); // vagrant@localhost.localdomain

        $bodyHTML = file_get_contents('mail/contact.html');

        $vars = [
            '{$full_name}' => $data['full_name'],
            '{$email}' => $data['email'],
            '{$phone}' => $data['phone'],
            '{$message}' => $data['message']
        ];

        $bodyHTML = strtr($bodyHTML, $vars);

        foreach ($recipients as $recipient) {
            $m->to($recipient)
                ->from('contacto@v-run.mx')
                ->fromName('V Run')
                ->subject('Contacto')
                ->bodyHTML($bodyHTML)
                ->send();
        }

        $render = 'message_sent';
    }
}

?>

<div class="container padding-top-1x padding-bottom-3x">
    <?php if ($render == 'contact_form'): ?>
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <form class="login-box" method="post">
                    <input type="hidden" name="form" value="contact">
                    <!-- <h4 class="margin-bottom-1x">Inicia sesión con una red social</h4> -->
                    <!-- <div class="row margin-bottom-1x"> -->
                        <!-- <div class="col-xl-4 col-md-6 col-sm-4"><a class="btn btn-sm btn-block facebook-btn" href="#"><i class="socicon-facebook"></i>&nbsp;Facebook login</a></div> -->
                        <!-- <div class="col-xl-4 col-md-6 col-sm-4"><a class="btn btn-sm btn-block twitter-btn" href="#"><i class="socicon-twitter"></i>&nbsp;Twitter login</a></div> -->
                        <!-- <div class="col-xl-4 col-md-6 col-sm-4"><a class="btn btn-sm btn-block google-btn" href="#"><i class="socicon-googleplus"></i>&nbsp;Google+ login</a></div> -->
                    <!-- </div> -->
                    <h4 class="margin-bottom-1x">Déjanos un mensaje. Te contactaremos en menos de 24 horas </br><small>(generalmente en minutos)</small></h4>
                    <div class="form-group input-group">
                        <input class="form-control" type="text" name="fulll_name" placeholder="Ingresa nombre completo" required><span class="input-group-addon"><i class="fa fa-user"></i></span>
                    </div>
                    <div class="form-group input-group">
                        <input class="form-control" type="email" name="email" placeholder="Ingresa correo electronico" required><span class="input-group-addon"><i class="icon-mail"></i></span>
                    </div>
                    <div class="form-group input-group">
                        <input class="form-control" type="text" name="phone" placeholder="Ingresa teléfono" required><span class="input-group-addon"><i class="fa fa-phone"></i></span>
                    </div>
                    <div class="form-group input-group">
                        <textarea class="form-control" name="message" style="height: 150px;" placeholder="Deja tu mensaje" required></textarea>
                        <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                    </div>

                    <div class="text-center text-sm-right">
                        <button class="btn btn-primary margin-bottom-none" type="submit" name="submit">Enviar</button>
                    </div>
                </form>
            </div>
        </div>
    <?php else: ?>
        <div class="alert alert-success fade show text-center margin-bottom-1x"></span>
            <p><i class="fa fa-bell"></i> El correo ha sido enviado correctamente, te contactaremos pronto. </p>
        </div>
    <?php endif; ?>

</div>

<?php require_once './footer.inc'; ?>
