<?php require_once './header.inc'; ?>

<div class="container padding-top-1x padding-bottom-3x">
        <div class="row">
            <!-- Start Title -->
            <div class="col-lg-3 col-md-4">
                <nav class="list-group">
                    <?php $isFirstItem = true; ?>
                    <?php foreach ($page->categories as $category): ?>
                        <?php if ($isFirstItem): ?>
                            <?php $isFirstItem = false; ?>
                            <a class="list-group-item active" data-toggle="tab" href="#cat-<?= $category->id ?>"><?= $category->title; ?></a>
                        <?php else: ?>
                            <a class="list-group-item" data-toggle="tab" href="#cat-<?= $category->id ?>"><?= $category->title; ?></a>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </nav>
                <div class="padding-bottom-3x hidden-md-up"></div>
            </div>
            <!-- End Title -->
            <!-- Start Content -->
            <div class="col-lg-9 col-md-8">
                <div class="tab-content faq-content">
                    <?php $isFirstItem = true; ?>
                    <?php foreach ($page->categories as $category): ?>
                        <div id="cat-<?= $category->id ?>" class="tab-pane fade active <?php echo $isFirstItem ? 'show' : ''; ?>">
                            <?php $isFirstQuestion = true; ?>
                            <?php foreach ($category->questions as $question): ?>
                                <div class="accordion" role="tablist" id="acc-<?= $category->id ?>">
                                    <div class="card">
                                        <div class="card-header" role="tab">
                                            <h6><a href="#q-<?= $question->id; ?>" data-toggle="collapse" data-parent="acc-<?= $category->id ?>"><?= $question->title; ?></a></h6>
                                        </div>
                                        <div class="collapse <?php echo $isFirstQuestion ? 'show' : ''; ?>" id="q-<?= $question->id; ?>" role="tabpanel">
                                            <div class="card-body">
                                                <?= $question->body ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php $isFirstQuestion = false; ?>
                            <?php endforeach; ?>
                        </div>
                        <?php $isFirstItem = false; ?>
                    <?php endforeach; ?>
                </div>
            </div>
            <!-- End Content -->
        </div>
    </div>

<?php require_once './footer.inc'; ?>
