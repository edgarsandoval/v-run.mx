        <?php $events = $pages->find("template=evento_template, sort=-id, limit=5, event_visible=1, event_classification=Público", 'findAll=true'); ?>
        <!-- End Page Content -->
        <!-- Start Footer -->
        <footer class="site-footer">
            <div class="container">
                <!-- Start Footer Info -->
                <div class="row">
                    <!-- Start Contact Info -->
                    <div class="col-lg-6 col-md-6">
                        <section class="widget widget-light-skin">
                            <h3 class="widget-title">V-RUN.MX</h3>
                            <p class="text-white">
                                <b>Never Stop Running</b><br/>
                                Forma parte nuestra comunidad de corredores, únete a eventos virtuales, entérate de los resultados de toda la comunidad, recibe tu medalla a la puerta de tu casa, apoya Asociaciones Civiles mediante tu inscripción, participa en sorteos y mantente activo siempre !
                            </p>
                            <a class="social-button shape-circle sb-facebook sb-light-skin" href="https://www.facebook.com/vrun.mx/" target="_blank" title="Facebook">
                                <i class="socicon-facebook"></i>
                            </a>
                            <a class="social-button shape-circle sb-facebook sb-light-skin" href="mailto:contacto@v-run.mx" target="_blank" title="Correo electrónico">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                            </a>
                            <a class="social-button shape-circle sb-facebook sb-light-skin" href="https://forms.monday.com/forms/49138a8126dab53ff443826c44c536cc" target="_blank" title="Buzón de sugerencias">
                                <i class="fa fa-external-link" aria-hidden="true"></i>
                            </a>
                            <!-- <a class="social-button shape-circle sb-twitter sb-light-skin" href="#">
                                <i class="socicon-twitter"></i>
                            </a>
                            <a class="social-button shape-circle sb-instagram sb-light-skin" href="#">
                                <i class="socicon-googleplus"></i>
                            </a>
                            <a class="social-button shape-circle sb-instagram sb-light-skin" href="#">
                                <i class="socicon-instagram"></i>
                            </a> -->
                        </section>
                    </div>
                    <!-- End Contact Info -->
                    <!-- Start Mobile Apps -->
                    <div class="col-lg-3 col-md-6">
                        <section class="widget widget-links widget-light-skin">
                            <h3 class="widget-title">Eventos</h3>
                            <ul>
                                <?php foreach($events as $event): ?>
                                    <li><a href="http://v-run.mx/eventos/#event-<?php echo urlencode(str_replace(' ', '-', mb_strtolower($event->title))); ?>"><?= $event->title?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </section>
                    </div>
                    <!-- End Mobile Apps -->
                    <!-- Start About Us -->
                    <div class="col-lg-3 col-md-6">
                        <section class="widget widget-links widget-light-skin">
                            <h3 class="widget-title">Sitemap</h3>
                            <ul>
                                <li><a href="/como-fuciona/">Cómo funciona</a></li>
                                <li><a href="/quienes-somos/">Quiénes somos</a></li>
                                <li><a href="/aviso-de-privacidad/">Aviso de privacidad</a></li>
                            </ul>
                        </section>
                    </div>
                    <!-- End About Us -->
                </div>
                <!-- End Footer Info -->
                <hr class="hr-light">
                <!-- Start Copyright -->
                <p class="footer-copyright text-center">© 2020 V-RUN | Todos los derechos reservados</p>
                <!-- End Copyright -->
            </div>
        </footer>
        <!-- End Footer -->
    </div>
    <?php if ($page->name == 'eventos'): ?>
    <div class="modal fade" tabindex="-1" role="dialog" id="register-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Seleccionar tipo de inscripción</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="text-align: center;"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="event-detail-modal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="text-align: center; max-height: 400px; overflow-y: scroll;"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="private-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Ingresa el código de invitación</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="row private-form" method="post">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="reg-fn">Código</label>
                                <input class="form-control" type="text" name="code" maxlength="6" required>
                            </div>
                        </div>
                        <button type="submit" name="submit" style="display: none;">Enviar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button id="private-btn-send" type="button" class="btn btn-secondary">Enviar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Start Photoswipe Container -->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="pswp__bg"></div>
        <div class="pswp__scroll-wrap">
            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>
            <div class="pswp__ui pswp__ui--hidden">
                <div class="pswp__top-bar">
                    <div class="pswp__counter"></div>
                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                    <button class="pswp__button pswp__button--share" title="Share"></button>
                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div>
                </div>
                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Photoswipe Container -->
    <?php endif; ?>
    <!-- Start Back To Top -->
    <a class="scroll-to-top-btn" href="#">
        <i class="icon-arrow-up"></i>
    </a>
    <?php if ($page->name == 'perfil' || $page->name == 'resultados'): ?>
        <!-- Modal -->
        <div class="modal fade" id="map-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header"  style="padding-top: 10px !important;padding-bottom: 10px !important;">
                        <h5 class="modal-title">Recorrido virtual</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    </div>
                </div>
            </div>
        </div>

        <?php //$videoUrl = $pages->get('template=video_template')->video_url; ?>
        <div class="overlay-video">
            <div class="videoWrapperExt">
                <div class="videoWrapper">
    				<div class="close"></div>
                    <iframe id="player" width="853" height="480" src="" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>

        <div class="modal" tabindex="-1" role="dialog" id="evidence-modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Agregar evidencias</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="/user-event-linker/" method="POST" enctype="multipart/form-data" class="evidence-form">
                            <input type="hidden" name="form" value="submit_evidence">
                            <div class="input-container row">
                            </div>
                            <hr/>
                            <!-- <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label></label>
                                        <input class="form-control" type="file" name="image[]" accept="image/*" required>
                                    </div>
                                </div>
                            </div> -->
                            <div class="file-upload">
                                <div class="image-upload-wrap">
                                    <input class="file-upload-input" type="file" name="image[]" onchange="readURL(this);" accept="image/*" required/>
                                    <div class="drag-text">
                                        <h3>Arrastra y suelta una foto o click para seleccionar una imagen</h3>
                                    </div>
                                </div>
                                <div class="file-upload-content">
                                    <img class="file-upload-image" src="#" alt="your image" />
                                    <div class="image-title-wrap">
                                        <button type="button" onclick="removeUpload()" class="remove-image">Eliminar <span class="image-title">Imagen</span></button>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" name="submit" style="visibility: hidden;">Enviar</button>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button id="evidence-btn" type="button" class="btn btn-secondary">Enviar</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal" tabindex="-1" role="dialog" id="evidence-modal-alt">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><a href="#"class="open-video" style="display: none;"> <i class="fa fa-play" aria-hidden="true"></i> </a>¡Completa tu tabla de lotería!</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="/user-event-linker/" method="POST" enctype="multipart/form-data" class="evidence-form-alt">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button id="evidence-btn-alt" type="button" class="btn btn-secondary">Enviar</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal " tabindex="-1" role="dialog" id="evidences-modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Registro</th>
                                        <th>Evidencia</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal " tabindex="-1" role="dialog" id="tracking-modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Guía de rastreo</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal" tabindex="-1" role="dialog" id="invitation-modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Inscribir por invitación</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="/user-event-linker/" method="POST" enctype="multipart/form-data" class="invitation-form">
                            <input type="hidden" name="form" value="submit_invitation">
                            <div class="input-container row">
                            </div>
                            <button type="submit" name="submit" style="visibility: hidden;">Enviar</button>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button id="invitation-btn" type="button" class="btn btn-secondary">Enviar</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="finish-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                </div>
            </div>
        </div>

        <!-- Profile image modal -->
        <div id="image-modal" class="modal">
            <!-- The Close Button -->
            <span class="close">&times;</span>
            <!-- Modal Content (The Image) -->
            <img class="modal-content">
            <!-- Modal Caption (Image Text) -->
            <!-- <div id="caption"></div> -->
        </div>
    <?php endif; ?>
    <!-- End Back To Top -->
    <div class="site-backdrop"></div>
    <!-- Modernizr JS -->
    <script src="<?php echo $config->urls->assets?>js/modernizr.min.js"></script>
    <!-- JQuery JS -->
    <script src="<?php echo $config->urls->assets?>js/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="<?php echo $config->urls->assets?>js/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="<?php echo $config->urls->assets?>js/bootstrap.min.js"></script>
    <!-- CountDown JS -->
    <script src="<?php echo $config->urls->assets?>js/count.min.js"></script>
    <!-- Gmap JS -->
    <script src="<?php echo $config->urls->assets?>js/gmap.min.js"></script>
    <!-- ImageLoader JS -->
    <script src="<?php echo $config->urls->assets?>js/imageloader.min.js"></script>
    <!-- Isotope JS -->
    <script src="<?php echo $config->urls->assets?>js/isotope.min.js"></script>
    <!-- NouiSlider JS -->
    <script src="<?php echo $config->urls->assets?>js/nouislider.min.js"></script>
    <!-- OwlCarousel JS -->
    <script src="<?php echo $config->urls->assets?>js/owl.carousel.min.js"></script>
    <!-- PhotoSwipe UI JS -->
    <script src="<?php echo $config->urls->assets?>js/photoswipe-ui-default.min.js"></script>
    <!-- PhotoSwipe JS -->
    <script src="<?php echo $config->urls->assets?>js/photoswipe.min.js"></script>
    <!-- Velocity JS -->
    <script src="<?php echo $config->urls->assets?>js/velocity.min.js"></script>
    <!-- Card JS -->
    <script src="<?php echo $config->urls->assets?>js/card.min.js"></script>
    <!-- Main JS -->
    <script src="<?php echo $config->urls->assets?>js/script.js"></script>
    <!-- Custom JS -->
    <script src="<?php echo $config->urls->assets?>js/main.js?v=<?= time(); ?>"></script>

    <!-- CDN custom libraries -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/mathusummut/confetti.js/confetti.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!-- Datatables Stack -->
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
</body>
</html>
