<?php require_once './header.inc'; ?>

<div class="container padding-top-1x padding-bottom-3x">
        <div class="row justify-content-center">
            <!-- Start Blog Single Content -->
            <div class="col-lg-9">
                <h2 class="padding-top-2x text-center"><?= $page->title; ?></h2>
                <div class="ac-content">
                    <?= $page->body; ?>
                </div>
                <div class="owl-carousel" data-owl-carousel='{ "nav": true, "dots": false, "loop": true }'>
                    <?php foreach ($page->images as $image): ?>
                        <figure><img src="<?php echo $image->url ?>" alt="Image">
                            <figcaption class="text-white"><?php echo $image->description ?></figcaption>
                        </figure>
                    <?php endforeach; ?>
                </div>
            </div>
            <!-- End Blog Single Content -->
        </div>
    </div>

<?php require_once './footer.inc'; ?>
