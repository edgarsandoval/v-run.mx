/*
 *	STRIWENSKO LIBRARY HTML ELEMENT CLASS
 *  v 1.2
 */
if(typeof STRIWENSKO == "undefined")
{
	var STRIWENSKO = new Object();
}
STRIWENSKO.TIMER = new Object();

STRIWENSKO.TIMER.timeLineId = 0;
STRIWENSKO.TIMER.timeLineDict = {};
STRIWENSKO.TIMER.start = (new Date()).getTime();
STRIWENSKO.TIMER.getTimer = function()
{
	return (new Date()).getTime() - STRIWENSKO.TIMER.start;
}

function TimeLine(duration, interval)
{
	this.duration = duration;
	this.direction = 1;
	this.position = 0;
	this.status = "STOP";
	this.startTime = 0;
	this.interval = interval;

	this.intervalObj;
	this.id = STRIWENSKO.TIMER.timeLineId.toString();
	STRIWENSKO.TIMER.timeLineDict[this.id] = this;
	STRIWENSKO.TIMER.timeLineId++;

	this.data = {};
	this.events = {}
}

TimeLine.prototype.addEventListener = function(type, listener, scope)
{
	if (!this.events)
	{
		this.events = {};
	}
	if (!this.events[type])
	{
		this.events[type] = new Array();
	}
	this.events[type].push({listener:listener, scope:scope});
}

TimeLine.prototype.dispatchEvent = function(type)
{
	if (!this.events)
	{
		this.events = {};
	}

	if (this.events[type])
	{
		var events = this.events[type];
		for (var iEvent = 0; iEvent < events.length; iEvent++)
		{
			var listener = events[iEvent].listener;
			var scope = events[iEvent].scope;

			if ((typeof listener).toString() == "function")
			{
				scope.eventRecieverFunction = listener;
				scope.eventRecieverFunction({currentTarget:this, type:type});
			}
			else
			{
				var event = {currentTarget:this, type:type, data:this.data}
				eval("scope." + listener +"(event)");
			}
		}
	}
}

TimeLine.prototype.removeEventListener = function(type, listener, scope)
{
	if (!this.events)
	{
		return false;
	}
	if (!this.events[type])
	{
		return false;
	}

	var events = this.events[type];
	for (var iEvent = 0; iEvent < events.length; iEvent++)
	{
		if ((listener == events[iEvent].listener) && (scope == events[iEvent].scope))
		{
			events.splice(iEvent, 1);
		}
	}
}

TimeLine.prototype.play = function ()
{
	if (((this.direction == 1) && (this.position == this.duration)) || ((this.direction == -1) && (this.position == 0)))
	{
	}
	else
	{
		this.startTime = STRIWENSKO.TIMER.getTimer() - this.position;
		if (this.status != 'PLAY')
		{
			this.intervalObj = setInterval("STRIWENSKO.TIMER.timeLineDict[" + this.id + "].update();", this.interval);
		}
		this.status = 'PLAY';
	}
}

TimeLine.prototype.pause = function ()
{
	this.status = 'PAUSE';
	clearInterval(this.intervalObj);
}

TimeLine.prototype.stop = function ()
{
	this.status = 'STOP';
	clearInterval(this.intervalObj);
}

TimeLine.prototype.position = function (value)
{
	this.position = value;
	this.startTime = STRIWENSKO.TIMER.getTimer() - this.position;
}

TimeLine.prototype.update = function ()
{
	if (this.direction == 1)
	{
		this.position =  STRIWENSKO.TIMER.getTimer() - this.startTime;
		this.position = Math.min(this.duration, this.position);
		if (this.position == this.duration)
		{
			this.status = 'STOP';
			clearInterval(this.intervalObj);
			this.dispatchEvent(Event.COMPLETE);

			if (this.onComplete)
			{
				this.onComplete();
			}
		}
	}
	else
	{
		this.position = Math.max((2 * this.position) - (STRIWENSKO.TIMER.getTimer() - this.startTime), 0);
		this.startTime = STRIWENSKO.TIMER.getTimer() - this.position;
		if (this.position == 0)
		{
			this.status = 'STOP';
			clearInterval(this.intervalObj);
			this.dispatchEvent(Event.COMPLETE);

			if (this.onComplete)
			{
				this.onComplete();
			}
		}
	}

	this.dispatchEvent(Event.CHANGE);
}


TimeLine.prototype.getTime = function(timeOffset, duration, easeFunction)
{
	var time = Math.min(Math.max(this.position - timeOffset, 0), duration);
	if (easeFunction != null)
	{
		return easeFunction(time, 0, 1, duration);
	}
	return this.easeInOut(time, 0, 1, duration);
}

TimeLine.prototype.easeInOut = function (t, b, c, d)
{
	return -c / 2 * (Math.cos(Math.PI * t / d) - 1) + b;
}
