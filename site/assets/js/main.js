if(typeof onInit === 'function') {
    document.getElementsByTagName('body')[0].onload = onInit;
}
var windowPrompt;
var $modalInputContainer = $('.input-container');
// <div class="col-md-12">
//     <div class="form-group">
//         <label></label>
//         <input class="form-control" type="number" name="" step="0.1"required>
//     </div>
// </div>
$(document).ready(function() {
    $('.add-evidence').click(async function() {
        let eventId     = $(this).data('id');
        let eventType   = $(this).data('type');
        let isTraining  = $(this).data('training') == '1';
        let videoUrl    = $(this).data('video-url');

        console.log(videoUrl);

        if(isTraining) {
            // Then is a custom evidence.
            let data = {
                form: 'get_evidences',
                event: eventId,
                submit: 1,
            };

            let response = await $.post('/user-event-linker/', data);

            let { htmlResponse, isTrainningFinished } = response.data;

            let gonnaShowSubEvidences = false;
            if(!isTrainningFinished)
                gonnaShowSubEvidences = true;

            if(gonnaShowSubEvidences) {
                $('.evidence-form-alt').html(htmlResponse);
                $('.evidence-form-alt').append('<button type="submit" name="submit" style="visibility: hidden;">Enviar</button>');
                $('.evidence-form-alt').prepend('<input type="hidden" name="form" value="submit_subevidence">');

                $('#evidence-modal-alt').modal('show');


                $('.lottery-image').click(function() {
                    let src = $(this).attr('src');
                    $('#image-modal img').attr('src', src);
                    $('#image-modal').css('display', 'flex');
                });

                $('.file-input').change(function() {
                    $('.file-name').text($(this)[0].files[0].name);
                    $('.file-name').css('display', 'block');

                    $(this).parent().find('button').hide();
                });

                $('.edit-subevidence').click(function() {
                    let idx = $(this).data('idx');
                    let id  = $(this).data('id');
                    let isAnswerType = $(this).data('ans');

                    console.log(isAnswerType);

                    // Remove other blocks from this to down

                    $('.lottery-slot').each(function(i, el) {
                        if((i + 1) <= idx) return;
                        $(el).remove();
                    });

                    $(this).parent().parent().hide()
                    if(isAnswerType) {
                        $(this).parents('.lottery-slot').find('.answer-input').show();
                        $(this).parents('.lottery-slot').find('.answer-input').find('input').attr({
                            'name': 'answer',
                            'required': true
                        });
                    } else {
                        $(this).parents('.lottery-slot').find('.upload-btn-wrapper').show();
                        $(this).parents('.lottery-slot').find('.upload-btn-wrapper').find('input').attr({
                            'name': 'file',
                            'required': true,
                        });
                    }

                    $form = $(this).parents('form');

                    $form.prepend('<input type="hidden" name="subevent_id" value="' + id+ '"/>');
                    // $form.find('input[name="id"]').val(id);
                    $form.find('input[name="order"]').val(idx);
                });

                if($('.lottery-slot').length == 1) {
                    $(".overlay-video").find('iframe').attr('src', videoUrl);
                    $(".overlay-video").show();
                    $('.open-video').show();
                    setTimeout(function() {
                    	$(".overlay-video").addClass("o1");
                    }, 100);
                }
                else if($('.lottery-slot').length > 1) {
                    $('.open-video').attr('data-video-url', videoUrl);
                    $('.open-video').show();
                }

                return;
            }
        }


        $modalInputContainer.html('');

        let distanceInput = `
            <div class="col-md-12">
               <div class="form-group">
                   <label>Ingresa la <b>DISTANCIA</b> recorrida en <b>KILOMETROS</b> <b class="text-danger">SOLO LA CANTIDAD</b></label>
                   <input class="form-control" type="number" name="evidence_distance" min="0" step="any" required>
               </div>
           </div>
        `;

        let durationInput = `
            <div class="col-md-12">
                <div class="form-group">
                    <label>Ingresa la <b>DURACIÓN</b> <b class="text-danger">(hh:mm:ss)</b></label>
                    <div class="row">
                        <div class="col-4">
                            <input class="form-control" type="number" name="evidence_duration_h" min="0" step="1" value="0" required>
                        </div>
                        <div class="col-4">
                            <input class="form-control" type="number" name="evidence_duration_m" min="0" step="1" value="0" required>
                        </div>
                        <div class="col-4">
                            <input class="form-control" type="number" name="evidence_duration_s" min="0" step="1" value="0" required>
                        </div>
                    </div>
                </div>
            </div>
        `;

        if(false) {
            $modalInputContainer.append(durationInput);
            $modalInputContainer.append(distanceInput);
        } else {
            $modalInputContainer.append(distanceInput);
            $modalInputContainer.append(durationInput);
        }

        $('#evidence-modal').modal('show');
        let eventField = $('.evidence-form').find('input[name="id"]');
        if(eventField.length !== 0)
            eventField.remove();

        $('.evidence-form').append(`<input type="hidden" name="id" value="${ eventId }">`);
        // Refresh input value
        removeUpload();
    });

    $('#evidence-btn').click(function() {
        var isMobile = false; //initiate as false
        var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
        // device detection
        if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
            || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) {
            isMobile = true;
        }

        if(isSafari) {
            document.querySelector('.evidence-form button[type="submit"]').click();
            return;
        }

        if(isMobile) {
            $('.evidence-form button').click();
            return;
        }

        if($('.evidence-form')[0].checkValidity())
            $('.evidence-form button').click();
        else
            alert('Formulario incompleto');
    });

    $('#evidence-btn-alt').click(function(event) {
        // event.preventDefault();
        //
        // $('.evidence-form-alt button').click();
        // $('#evidence-modal-alt').modal('hide');
        //
        // Swal.fire('Espera un momento', 'Se está procesando la información');
        // Swal.showLoading();
        // return;
        var isMobile = false; //initiate as false
        var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
        // device detection
        if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
            || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) {
            isMobile = true;
        }

        if(isSafari) {
            document.querySelector('.evidence-form button[type="submit"]').click();
            return;
        }

        if(isMobile) {
            $('.evidence-form-alt button').click();
            return;
        }

        if($('.evidence-form-alt')[0].checkValidity())
            $('.evidence-form-alt button').click();
        else
            alert('Formulario incompleto');
    });

    $('.show-evidences').click(async function() {

        let id      = $(this).data('id');
        let event   = $(this).data('event');
        let isTraining  = $(this).data('training') == '1';
        let isFinished  = $(this).data('finished');

        if(isTraining) {
            let result = null;
            if(isFinished) {
                result = await Swal.fire({
                    reverseButtons: true,
                    showCancelButton: true,
                    cancelButtonText: 'Mostrar Tabla de Lotería',
                    confirmButtonText: 'Carrera 5 km',
                    customClass: {
                        confirmButton: 'btn-block',
                        cancelButton: 'btn-block swal2-confirm',
                    }
                });
            }

            let gonnaShowSubEvidences = false;

            if(!isFinished || result.dismiss === Swal.DismissReason.cancel)
                gonnaShowSubEvidences = true;

            if(gonnaShowSubEvidences) {
                let data = {
                    form: 'get_evidences',
                    event: id,
                    submit: 1,
                };

                let response = await $.post('/user-event-linker/', data);
                let { htmlResponse, isTrainningFinished } = response.data;

                $('.evidence-form-alt').html(htmlResponse);
                $('.evidence-form-alt').append('<button type="submit" name="submit" style="visibility: hidden;">Enviar</button>');
                $('.evidence-form-alt').prepend('<input type="hidden" name="form" value="submit_subevidence">');

                $('#evidence-modal-alt').modal('show');


                $('.lottery-image').click(function() {
                    let src = $(this).attr('src');
                    $('#image-modal img').attr('src', src);
                    $('#image-modal').css('display', 'flex');
                });

                $('.file-input').change(function() {
                    $('.file-name').text($(this)[0].files[0].name);
                    $('.file-name').css('display', 'block');
                    $(this).parent().find('button').hide();
                });

                $('.edit-subevidence').click(function() {
                    let idx = $(this).data('idx');
                    let id  = $(this).data('id');
                    let isAnswerType = $(this).data('ans');

                    console.log(isAnswerType);

                    // Remove other blocks from this to down

                    $('.lottery-slot').each(function(i, el) {
                        if((i + 1) <= idx) return;
                        $(el).remove();
                    });

                    $(this).parent().parent().hide()
                    if(isAnswerType) {
                        $(this).parents('.lottery-slot').find('.answer-input').show();
                        $(this).parents('.lottery-slot').find('.answer-input').find('input').attr({
                            'name': 'answer',
                            'required': true
                        });
                    } else {
                        $(this).parents('.lottery-slot').find('.upload-btn-wrapper').show();
                        $(this).parents('.lottery-slot').find('.upload-btn-wrapper').find('input').attr({
                            'name': 'file',
                            'required': true,
                        });
                    }

                    $form = $(this).parents('form');

                    $form.prepend('<input type="hidden" name="subevent_id" value="' + id+ '"/>');
                    // $form.find('input[name="id"]').val(id);
                    $form.find('input[name="order"]').val(idx);
                });

                return;
            }
        }

        $('#evidences-modal .modal-title').html('Evidencias registradas: ' + event);

        let tableContent = $(this).closest('td').children('.evidence-table').find('tbody').html();
        if(tableContent === undefined) {
            tableContent = $(this).closest('.action-details-mobile').children('.evidence-table').find('tbody').html();
        }
        $('#evidences-modal .modal-body tbody').html(tableContent);
        $('#evidences-modal').modal('show');
    });

    $('.edit-avatar').click(function() {
        alert('Resolución recomendada > 210px x 210px');

        $('#profile-form input').click();
    });

    $('#profile-form input').change(function() {
        if(confirm('¿Estás seguro que deseas cambiar tu imagen de perfil'))
            $('#profile-form button').click();
    });

    $('.btn-menu').click(function() {
        let href = $(this).attr('href');
        href = href.replace('/perfil/', '');

        let a = $('a[href="' + href + '"]');
        if(a.length) {
            a.click();
            return false;
        }
    });

    $('.btn-register-modal').click(function() {
        let htmlContent = $(this).parent().children('.register-modal').html();
        $('#register-modal .modal-body').html(htmlContent);

        $('.free-register').click(function(event) {
            event.preventDefault();

            let requestText = $(this).data('request') == '1';
            let url = $(this).data('url');
            let title = $(this).data('title');

            $('#register-modal').modal('hide');
            if(requestText) {
                Swal.fire({
                    title,
                    input: 'text',
                    showCancelButton: true,
                    cancelButtonText: 'Cancelar',
                    confirmButtonText: 'Enviar',
                    showLoaderOnConfirm: true,
                    reverseButtons: true
                }).then(result => {
                    url += `&extra_field_1=${  encodeURIComponent(result.value) }`;
                    window.location.href = url;
                })
            } else {
                window.location.href = url;
            }
        });

        $('.btn-conekta').click(function() {
            let url = $(this).data('url');

            if(windowPrompt)
                windowPrompt.close();

            var w = 1013;
            var h = window.innerHeight;

            let code = `
            <html>
            <head>
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
            </head>
            <body><iframe src="${ url }" width="100%" height="100%" scrolling="auto"></iframe></body>
            </html>`;

            windowPrompt = window.open("", "", `width=${ w },height=${ h }`);
            windowPrompt.document.open("text/html", "replace");
            windowPrompt.document.write(code);

            windowPrompt.onbeforeunload = function() {
                window.location.reload();
            }
        });

        $('.btn-boletify').click(function() {
            let url = $(this).data('url');

            if(windowPrompt)
                windowPrompt.close();

            var w = 1013;
            var h = window.innerHeight;

            let code = '<iframe src="' + url + '" width="100%" height="100%" scrolling="auto"></iframe>';

            windowPrompt = window.open("", "", `width=${ w },height=${ h }`);
            windowPrompt.document.open("text/html", "replace");
            windowPrompt.document.write(code);

            windowPrompt.onbeforeunload = function() {
                window.location.reload();
            }
        });

        $('#register-modal').modal('show');
    });

    $('.finish_event').click(function() {
        if(confirm('¿Estás seguro que deseas terminar el evento? \n No podrás seguir acomulando registros.')) {
            $(this).parent().find('button[type="submit"]').click();
        }
    });

    $('.btn-private').click(function() {
        $('.private-form input').val('');

        $('#private-modal').modal('show');
    });

    $('#private-btn-send').click(function() {
        $('.private-form button').click();
    });

    $('.private-form').submit(function(event) {
        event.preventDefault();
        // showLoading
        let data = new FormData(this);
        data.append('form', 'event_searcher');
        data.append('submit', '');
        // console.log(data);
        // $.post('/user-event-linker/', data, function(response) {
            // console.log(response);
        // });
        $('#private-modal').modal('hide');
        Swal.fire('Espera un momento', 'Se está procesando la información');
        Swal.showLoading();
        $.ajax({
            url: '/user-event-linker/',
            method: 'POST',
            processData: false,
            contentType: false,
            cache: false,
            data: data,
            success: function (response, status, jqXHR) {
                if(response.status) {
                    let $events = $('.event-details');
                    $events.addClass('d-none');
                    $events.each(function(idx, item) {
                        if($(item).data('id') == response.data.id) {
                            $(item).removeClass('d-none');
                        }
                    });
                    Swal.close();
                    $('.btn-private').hide();
                } else {
                    Swal.fire(
                        '¡Error!',
                        response.message,
                        'error'
                    );
                }
                // $('.event-container').html('');
                // $('.event-container').append(response);
            }
        })
    });

    $('.show-evidences-prvt').click(async function() {
        Swal.fire('Espera un momento', 'Se está procesando la información');
        Swal.showLoading();

        let id      = $(this).data('id');
        let event   = $(this).data('event');
        let lock    = $(this).data('lock');
        let isTraining  = $(this).data('training') == '1';
        let runner = $(this).data('runner');

        if(isTraining) {

            let result = await Swal.fire({
                reverseButtons: true,
                showCancelButton: true,
                cancelButtonText: 'Mostrar Tabla de Lotería',
                confirmButtonText: 'Carrera 5 km',
                customClass: {
                    confirmButton: 'btn-block',
                    cancelButton: 'btn-block swal2-confirm',
                }
            });

            let gonnaShowSubEvidences = false;

            if(result.dismiss === Swal.DismissReason.cancel)
                gonnaShowSubEvidences = true;

            if(gonnaShowSubEvidences) {
                let data = {
                    form: 'get_evidences_member',
                    user_event_id: id,
                    lock: lock,
                    submit: 1
                };

                let htmlResponse = await $.post('/user-event-linker/', data);

                $('.evidence-form-alt').html(htmlResponse);
                $('.evidence-form-alt').append('<button type="submit" name="submit" style="visibility: hidden;">Enviar</button>');
                $('.evidence-form-alt').prepend('<input type="hidden" name="form" value="submit_subevidence">');
                $('#evidence-modal-alt').find('.modal-title').html(runner);

                $('#evidence-modal-alt').modal('show');

                $('.lottery-image').click(function() {
                    let src = $(this).attr('src');
                    $('#image-modal img').attr('src', src);
                    $('#image-modal').css('display', 'flex');
                });

                if(lock) {
                    $('.file-input').change(function() {
                        $('.file-name').text($(this)[0].files[0].name);
                        $('.file-name').css('display', 'block');
                        $(this).parent().find('button').hide();
                    });

                    $('.edit-subevidence').click(function() {
                        let idx = $(this).data('idx');
                        let id  = $(this).data('id');
                        let isAnswerType = $(this).data('ans');

                        console.log(isAnswerType);

                        // Remove other blocks from this to down

                        $('.lottery-slot').each(function(i, el) {
                            if((i + 1) <= idx) return;
                            $(el).remove();
                        });

                        $(this).parent().parent().hide()
                        if(isAnswerType) {
                            $(this).parents('.lottery-slot').find('.answer-input').show();
                            $(this).parents('.lottery-slot').find('.answer-input').find('input').attr({
                                'name': 'answer',
                                'required': true
                            });
                        } else {
                            $(this).parents('.lottery-slot').find('.upload-btn-wrapper').show();
                            $(this).parents('.lottery-slot').find('.upload-btn-wrapper').find('input').attr({
                                'name': 'file',
                                'required': true,
                            });
                        }

                        $form = $(this).parents('form');

                        $form.prepend('<input type="hidden" name="subevent_id" value="' + id+ '"/>');
                        // $form.find('input[name="id"]').val(id);
                        $form.find('input[name="order"]').val(idx);
                    });
                    $('#evidence-btn-alt').show();
                } else {
                    $('#evidence-btn-alt').hide();
                }
                return;
            }
        }

        let data = {
            id: id,
            form: 'private_evidences',
            submit: '',
            lock
        };

        $.ajax({
            url: '/user-event-linker/',
            method: 'POST',
            data: data,
            success: function (response, status, jqXHR) {
                Swal.close();
                if(response.status) {
                    let tableContent = response.data.htmlContent;
                    $('#evidences-modal .modal-title').html('Evidencias registradas: ' + event);
                    $('#evidences-modal .modal-body tbody').html(tableContent);
                    $('#evidences-modal').modal('show');

                    $('.btn-edit').click(function() {
                        $(this).parents('tr').find('.read-evt').hide();
                        $(this).parents('tr').find('.write-evt').show();

                        $('#evidences-modal .modal-dialog').addClass('modal-lg');
                    });

                    $('.btn-cancel').click(function() {
                        $(this).parents('tr').find('.read-evt').show();
                        $(this).parents('tr').find('.write-evt').hide();

                        $('#evidences-modal .modal-dialog').removeClass('modal-lg');
                    });

                    $('.btn-send').click(function() {
                        if(confirm('¿Estás seguro que deseas modificar el registro?')) {
                            var $tr  = $(this).parents('tr');
                            let id  = $tr.data('id');
                            let user_event_id  = $tr.data('ue');

                            let distance = $tr.find('.distance-evt').val();
                            let duration = $tr.find('.duration-evt').val();

                            let data = {
                                id,
                                user_event_id,
                                distance,
                                duration,
                                form: 'edit_evidence',
                                submit: ''
                            };

                            $.ajax({
                                url: '/user-event-linker/',
                                method: 'POST',
                                data: data,
                                success: function (response, status, jqXHR) {
                                    if(response.status) {
                                        alert(response.message);

                                        $tr.find('.read-evt').first().html(response.data.label);

                                        $('#evidences-modal').find('.read-evt').show();
                                        $('#evidences-modal').find('.write-evt').hide();

                                        if($('#evidences-modal').find('.write-evt:visible').length == 0)
                                            $('#evidences-modal .modal-dialog').removeClass('modal-lg');
                                    }
                                }
                            });
                        }
                    });

                    $('.btn-delete').click(function() {
                        if(confirm('¿Estás seguro que deseas eliminar el registro?')) {
                            let $tr  = $(this).parents('tr');
                            let id  = $tr.data('id');

                            let data = {
                                id,
                                form: 'remove_evidence',
                                submit: ''
                            };

                            $.ajax({
                                url: '/user-event-linker/',
                                method: 'POST',
                                data: data,
                                success: function (response, status, jqXHR) {
                                    if(response.status) {
                                        alert(response.message);

                                        $tr.remove();

                                        if($('#evidences-modal').find('.write-evt:visible').length == 0)
                                            $('#evidences-modal .modal-dialog').removeClass('modal-lg');
                                    }
                                }
                            });
                        }
                    });
                } else {
                    alert('Ocurrió un error');
                    console.error(response);
                }
                // $('.event-container').html('');
                // $('.event-container').append(response);
            }
        })
    });

    $('#evidences-modal').on('hidden.bs.modal', function () {
        // window.location.reload();
        // $('#evidences-modal .modal-dialog').removeClass('modal-lg');
        // $('#evidences-modal').find('.read-evt').show();
        // $('#evidences-modal').find('.write-evt').hide();
    })

    $('.btn-send').click(function() {
        let id = $(this).data('id');
        // let trackingGuide = $(this).parent().parent().find('input').val();
        let $tr = $(this).closest('tr');

        let $tdTracking = $tr.find('td:nth-child(9)');
        let $tdExtraField1 = $tr.find('td:nth-child(10)');
        let $tdExtraField2 = $tr.find('td:nth-child(11)');

        if(trackingGuide == '') {
            alert('La guía de rastreo no puede estar vacía');
            return;
        }

        if(confirm('¿Estás seguro que deseas agregar una guía de rastreo?')) {
            Swal.fire('Espera un momento', 'Se está procesando la información');
            Swal.showLoading();

            let data = {
                id: id,
                tracking_guide: trackingGuide,
                form: 'tracking_submit',
                submit: ''
            };

            $.ajax({
                url: '/user-event-linker/',
                method: 'POST',
                data: data,
                success: function (response, status, jqXHR) {
                    Swal.fire({
                        type: 'success',
                        title: '¡Éxito!',
                        text: response.message,
                    }).then(result => {
                        window.location.reload();
                    });
                }
            })
        }
    });

    $('.show-guide').click(function() {
        let $modal = $('#tracking-modal');
        $modal.find('.modal-body').html('');
        $modal.find('.modal-body').append(`<p><b>Fecha de enviado:</b> ${ $(this).data('date') } </p>`);
        $modal.find('.modal-body').append(`<p><b>Guía de rastreo:</b> ${ $(this).data('tracking') } </p>`);
        $modal.modal('show');
    });

    $('#password_reset_form').submit(function() {
        Swal.fire('Espera un momento', 'Se está procesando la información');
        Swal.showLoading();
    });

    $('.evidence-form').submit(function() {
        $('#evidence-modal').modal('hide');

        Swal.fire('Espera un momento', 'Se está procesando la información');
        Swal.showLoading();
    });

    $('.evidence-form-alt').submit(function() {
        $('#evidence-modal').modal('hide');

        Swal.fire('Espera un momento', 'Se está procesando la información');
        Swal.showLoading();
    });

    $('.btn-search-zip').click(function() {
        let zipCode = $('#zip_code').val();

        Swal.fire('Espera un momento', 'Se está procesando la información');
        Swal.showLoading();

        $.get('/user-event-linker/?dispatch=zip_search&zip_code=' + zipCode, function(response) {
            if(response.data.length == 0) {
                Swal.fire('¡Error!', 'No se encontró el C.P recibido', 'error');

                $('#state').val(null);
                $('#city').val(null);
            } else {
                $('#state').val(response.data[1]);
                $('#city').val(response.data[0]);

                Swal.close();
            }
        });

    });

    $('.member-image').click(function() {
        let src = $(this).attr('src');
        $('#image-modal img').attr('src', src);
        $('#image-modal').css('display', 'flex');
    });

    $('#image-modal span').click(function() {
        $('#image-modal').hide();
    });

    $('#image-modal').click(function() {
        $(this).hide();
    });

    $(".overlay-video").click(function(event) {
    	if (!$(event.target).closest(".videoWrapperExt").length) {
            var PlayingVideoSrc = $("#player").attr("src");//.replace("&autoplay=1", "");
            $('.open-video').attr("data-video-url", PlayingVideoSrc);
            $("#player").attr("src", '');
            $(".overlay-video").removeClass("o1");
    		setTimeout(function() {
    			$(".overlay-video").hide();
    		}, 600);
    	}
    });

    // video overlayer: close it via the X icon

    $('.videoWrapper .close').click(function(event) {
    		var PlayingVideoSrc = $("#player").attr("src");//.replace("&autoplay=1", "");
            $('.open-video').attr("data-video-url", PlayingVideoSrc);
            $("#player").attr("src", '');
    		$(".overlay-video").removeClass("o1");
    		setTimeout(function() {
    			$(".overlay-video").hide();
    		}, 600);

    });

    $('.open-video').click(function() {
        let videoUrl    = $(this).data('video-url');
        $(".overlay-video").find('iframe').attr('src', videoUrl);
        $(".overlay-video").show();
        setTimeout(function() {
            $(".overlay-video").addClass("o1");
        }, 100);
    });
    // $('.videoWrapper .close').click(function() {
    //     alert(1);
    // });

    $('.btn-remove').click(function() {
        if(confirm('¿Estás seguro que deseas modificar el registro?')) {
            let id = $(this).data('id');
            let data = {
                id,
                form: 'remove_user',
                submit: ''
            };

            $.ajax({
                url: '/user-event-linker/',
                method: 'POST',
                data: data,
                success: function (response, status, jqXHR) {
                    if(response.status) {
                        alert(response.message);

                        window.location.reload();
                    }
                }
            });
        }
    });

    $('.btn-finish').click(function() {
        if(confirm('¿Estás seguro que deseas modificar el registro?')) {
            let id = $(this).data('id');
            let data = {
                id,
                form: 'finish_event',
                submit: ''
            };

            $.ajax({
                url: '/user-event-linker/',
                method: 'POST',
                data: data,
                success: function (response, status, jqXHR) {
                    alert('Carrera terminada con éxito, el progreso ha sido añadido al perfil');
                    window.location.reload();
                }
            });
        }
    });

    $('.btn-edit-r').click(function() {
        let isPrivate = $(this).data('private') == '1';
        let id = $(this).data('id');
        let $tr = $(this).closest('tr');

        let $tdTracking = null;
        let $tdExtraField1 = null;
        let $tdExtraField2 = null;


        if(isPrivate) {
            $tdTracking = $tr.find('td:nth-child(9)');
            $tdExtraField1 = $tr.find('td:nth-child(10)');
            $tdExtraField2 = $tr.find('td:nth-child(11)');
        } else {
            $tdExtraField1 = $tr.find('td:nth-child(9)');
            $tdExtraField2 = $tr.find('td:nth-child(10)');
        }

        console.log(isPrivate);
        let tracking = null;
        if($tdTracking != null)
            tracking  = $tdTracking.html();
        let extraField1 = $tdExtraField1.html();
        let extraField2 = $tdExtraField2.html();

        if($tdTracking != null)
            tracking = tracking != '-' ? tracking : '';
        extraField1 = extraField1 != '-' ? extraField1 : '';
        extraField2 = extraField2 != '-' ? extraField2 : '';

        if($tdTracking != null)
            $tdTracking.html(`<input class="form-control" style="min-width: 250px; margin-top: 8px;" value="${ tracking }" type="text"/>`)
        $tdExtraField1.html(`<input class="form-control" style="min-width: 250px; margin-top: 8px;" value="${ extraField1 }" type="text"/>`)
        $tdExtraField2.html(`<input class="form-control" style="min-width: 250px; margin-top: 8px;" value="${ extraField2 }" type="text"/>`)

        if($tdTracking != null)
            $tr.find('td:last-child').append('<button class="btn btn-warning btn-send" data-id="' + id + '" data-private="1">Enviar <i class="fa fa-paper-plane"></i></button>');
        else
            $tr.find('td:last-child').append('<button class="btn btn-warning btn-send" data-id="' + id + '" data-private="0">Enviar <i class="fa fa-paper-plane"></i></button>');

        $('.btn-send').click(function() {
            let isPrivate = $(this).data('private') == '1';
            let id = $(this).data('id');
            // let trackingGuide = $(this).parent().parent().find('input').val();
            let $tr = $(this).closest('tr');


            let $tdTracking = null;
            let $tdExtraField1 = null;
            let $tdExtraField2 = null;

            let dataToSend = {};

            if(isPrivate) {
                $tdTracking = $tr.find('td:nth-child(9)');
                $tdExtraField1 = $tr.find('td:nth-child(10)');
                $tdExtraField2 = $tr.find('td:nth-child(11)');

                dataToSend = {
                    tracking_guide: $tdTracking.find('input').val(),
                    extra_field_1: $tdExtraField1.find('input').val(),
                    extra_field_2: $tdExtraField2.find('input').val()
                };
            } else {
                $tdExtraField1 = $tr.find('td:nth-child(9)');
                $tdExtraField2 = $tr.find('td:nth-child(10)');

                dataToSend = {
                    extra_field_1: $tdExtraField1.find('input').val(),
                    extra_field_2: $tdExtraField2.find('input').val()
                };
            }

            // if(trackingGuide == '') {
                // alert('La guía de rastreo no puede estar vacía');
                // return;
            // }

            if(confirm('¿Estás seguro que deseas agregar modificar el registro?')) {
                Swal.fire('Espera un momento', 'Se está procesando la información');
                Swal.showLoading();

                let data = {
                    id: id,
                    data: dataToSend,
                    form: 'tracking_submit',
                    submit: ''
                };

                $.ajax({
                    url: '/user-event-linker/',
                    method: 'POST',
                    data: data,
                    success: function (response, status, jqXHR) {
                        Swal.fire({
                            type: 'success',
                            title: '¡Éxito!',
                            text: response.message,
                        }).then(result => {
                            window.location.reload();
                        });
                    }
                })
            }
        });
        $('.btn-edit-r').remove();
        $('.btn-remove').remove();
        $('.btn-finish').remove();
    });

    $('.btn-invitation').click(function() {
        let extraLabel1 = $(this).data('lbl-1');
        let extraLabel2 = $(this).data('lbl-2');
        let id = $(this).data('id');

        $modalInputContainer.html('');

        let emailInput = `
            <div class="col-md-12">
               <div class="form-group">
                   <label>Correo Electrónico</label>
                   <input class="form-control" type="email" name="email" required>
               </div>
           </div>
        `;

        let extraField1 = `
            <div class="col-md-12">
                <div class="form-group">
                <label>${ extraLabel1 }</label>
                    <input class="form-control" type="text" name="extra_field_1" required>
                </div>
            </div>
        `;

        let extraField2 = `
            <div class="col-md-12">
                <div class="form-group">
                <label>${ extraLabel2 }</label>
                    <input class="form-control" type="text" name="extra_field_2" required>
                </div>
            </div>
        `;

        $modalInputContainer.append(`<input type="hidden" name="id" value="${ id }"/>`);
        $modalInputContainer.append(emailInput);
        $modalInputContainer.append(extraField1);
        $modalInputContainer.append(extraField2);

        $('#invitation-modal').modal('show');
    });

    $('#invitation-btn').click(function() {
        $('.invitation-form button').click();
    });

    $('.event-element').click(function(event) {
        let id          = $(this).data('id');
        let isLoggedIn  = $(this).data('logged') == '1';
        let eventExist  = $(this).data('already') == '1';

        $.get('/user-event-linker/?dispatch=get_event&id=' + id, function(response) {
            // console.log(response);
            let registerButton = '';

            if(!eventExist) {
                if(isLoggedIn) {
                    registerButton = '<div class="register-modal" style="display: none;">';
                    if(response.data.event.is_conekta) {
                        if(response.data.event.event_price1 != null && response.data.event.event_price1 > 0 && response.data.event.event_text_button != '') {
                            const conektaUrl1 = `https://v-run.mx/conekta-checkout/?user_id=${ response.data.event.user_id }&event_id=${ id }&price=${ response.data.event.event_price1 }&description=${  encodeURIComponent(response.data.event.event_text_button) }&event_carrier=${ response.data.event.event_carrier }&event_shipping1=${ response.data.event.event_shipping1a }&event_shipping2=${ response.data.event.event_shipping1b }`;
                            registerButton += `<button class="btn btn-primary btn-conekta" data-url="${ conektaUrl1 }">${ response.data.event.event_text_button }</button>`;
                        }

                        if(response.data.event.event_price2 != null && response.data.event.event_price2 > 0 && response.data.event.event_text_button2 != '') {
                            const conektaUrl2 = `https://v-run.mx/conekta-checkout/?user_id=${ response.data.event.user_id }&event_id=${ id }&price=${ response.data.event.event_price2 }&description=${  encodeURIComponent(response.data.event.event_text_button2) }&event_carrier=${ response.data.event.event_carrier2 }&event_shipping1=${ response.data.event.event_shipping2a }&event_shipping2=${ response.data.event.event_shipping2b }`;
                            registerButton += `<button class="btn btn-primary btn-conekta" data-url="${ conektaUrl2 }">${ response.data.event.event_text_button2 }</button>`;
                        }
                    } else {
                        if(response.data.event.id_boletify != null && response.data.event.id_boletify != 0)
                            registerButton += `<button class="btn btn-primary btn-boletify" data-url="${ response.data.event.boletify_url }" ${ response.data.event.boletify_url == '' ? 'disabled' : '' }>${ response.data.event.event_text_button }</button>`;

                        if(response.data.event.id_boletify2 != null && response.data.event.id_boletify2 != 0)
                            registerButton += `<button class="btn btn-primary btn-boletify" data-url="${ response.data.event.boletify_url2 }" ${ response.data.event.boletify_url2 == '' ? 'disabled' : '' }>${ response.data.event.event_text_button2 }</button>`;

                        if(response.data.event.id_boletify3 != null && response.data.event.id_boletify3 != 0)
                            registerButton += `<button class="btn btn-primary free-register" data-url="/user-event-linker/?dispatch=free_register&event_id=${ response.data.event.id_boletify3 }" data-request="${ response.data.event.event_request_text }" data-title="${ response.data.event.event_extra_label_1 }" style="width: 90%;">${ response.data.event.event_text_button3 }</button>`;
                    }
                    registerButton += '</div>';
                    registerButton += `<button type="button" class="btn btn-primary btn-register-modal" id="btn-event${ response.data.event.id }"><i class="icon-plus"></i> Inscribirte</button>`;
                } else {
                    registerButton = `
                        <form action="/login/" method="GET" style="display: inline;">
                            <input type="hidden" name="dispatch" value="LOGIN_FIRST"/>
                            <input type="hidden" name="event_id" value="${ response.data.event.id }"/>
                            <button class="btn btn-primary" name="submit" type="submit"><i class="icon-plus"></i> Inscribirte</button>
                        </form>
                    `;
                }
            }

            let carrousel = '';
            for(let image of response.data.event.images)
                carrousel += `
                    <div class="item">
                        <img src="${ image }"/>
                    </div>
                `;

            let kindOfEvent = '';

            if(response.data.event.type == 'Distancia' || response.data.event.type == 'Entrenamiento')
                kindOfEvent = 'DISTANCIA: ' + parseFloat(response.data.event.distance) + ' KM';
            else
                kindOfEvent = 'DURACIÓN: ' + parseFloat(response.data.event.duration) + ' minutos';

            let html = `
                <div class="event detail-content">
                    <div class="owl-carousel owl-theme">
                        ${ carrousel }
                    </div>
                    <p class="register">¡INSCRÍBETE!</p>
                    <p class="title">${ response.data.event.title} </br> <small style="color: #00b3ff;">${ kindOfEvent }</small></p>
                    <p class="title">DESCRIPCIÓN DEL EVENTO:</p>
                    <div class="description">${ response.data.event.description }</div>
                    ${ response.data.event.end_date != '' ? '<p class="end_date">' + response.data.event.end_date + ':</p>' : '' }
                    <p class="inscription">INSCRIPCIÓN:</p>
                    <div class="price-detail">${ response.data.event.price }</div>

                    <div class="row">
                        <div class="col-md-12">
                            <hr class="mt-30 mb-30"/>
                            <div class="d-flex flex-wrap justify-content-between mb-30">
                                <div class="sp-buttons mt-2 mb-2" style="margin: 0 auto;">
                                    ${ registerButton }
                                    <a href="/resultados/?event=${ response.data.event.name }" class="btn btn-primary" target="_blank"><i class="icon-align-justify"></i> Resultados</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            `;

            let $modal = $('#event-detail-modal');
            $modal.find('.modal-body').html('');
            $modal.find('.modal-body').html(html);

            $('.owl-carousel').owlCarousel({
                loop:true,
                margin:10,
                nav:false,
                items: 1
            })

            $('#event-detail-modal').modal('show');

            $('.btn-register-modal').click(function() {
                $('#event-detail-modal').modal('hide');
                let htmlContent = $(this).parent().children('.register-modal').html();
                $('#register-modal .modal-body').html(htmlContent);

                $('.free-register').click(function(event) {
                    event.preventDefault();

                    let requestText = $(this).data('request') == '1';
                    let url = $(this).data('url');
                    let title = $(this).data('title');

                    $('#register-modal').modal('hide');
                    if(requestText) {
                        Swal.fire({
                            title,
                            input: 'text',
                            showCancelButton: true,
                            cancelButtonText: 'Cancelar',
                            confirmButtonText: 'Enviar',
                            showLoaderOnConfirm: true,
                            reverseButtons: true
                        }).then(result => {
                            url += `&extra_field_1=${  encodeURIComponent(result.value) }`;
                            window.location.href = url;
                        })
                    } else {
                        window.location.href = url;
                    }
                });

                $('.btn-conekta').click(function() {
                    let url = $(this).data('url');

                    if(windowPrompt)
                        windowPrompt.close();

                    var w = 1013;
                    var h = window.innerHeight;

                    let code = `
                    <html>
                    <head>
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    </head>
                    <body><iframe src="${ url }" width="100%" height="100%" scrolling="auto"></iframe></body>
                    </html>`;

                    windowPrompt = window.open("", "", `width=${ w },height=${ h }`);
                    windowPrompt.document.open("text/html", "replace");
                    windowPrompt.document.write(code);

                    windowPrompt.onbeforeunload = function() {
                        window.location.reload();
                    }
                });

                $('.btn-boletify').click(function() {
                    let url = $(this).data('url');

                    if(windowPrompt)
                        windowPrompt.close();

                    var w = 1013;
                    var h = window.innerHeight;

                    let code = '<iframe src="' + url + '" width="100%" height="100%" scrolling="auto"></iframe>';

                    windowPrompt = window.open("", "", `width=${ w },height=${ h }`);
                    windowPrompt.document.open("text/html", "replace");
                    windowPrompt.document.write(code);

                    windowPrompt.onbeforeunload = function() {
                        window.location.reload();
                    }
                });

                $('#register-modal').modal('show');
            });

        });
    });
});

function readURL(input) {
    console.log(input);
  if (input.files && input.files[0]) {

    var reader = new FileReader();

    reader.onload = function(e) {
      $('.image-upload-wrap').hide();

      $('.file-upload-image').attr('src', e.target.result);
      $('.file-upload-content').show();

      // $('.image-title').html(input.files[0].name);
    };

    reader.readAsDataURL(input.files[0]);

  } else {
    removeUpload();
  }
}

function removeUpload() {
  $('.file-upload-input').replaceWith($('.file-upload-input').clone());
  $('.file-upload-content').hide();
  $('.image-upload-wrap').show();
}
$('.image-upload-wrap').bind('dragover', function () {
        $('.image-upload-wrap').addClass('image-dropping');
    });
    $('.image-upload-wrap').bind('dragleave', function () {
        $('.image-upload-wrap').removeClass('image-dropping');
});


function createButton(text, cb) {
    return $('<button>' + text + '</button>').on('click', cb);
}
